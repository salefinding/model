lazy val commonSettings = Seq(
  organization := "com.salefinding",
  version := "0.1.0-SNAPSHOT",
  publishMavenStyle := false,
  crossPaths := false
)

lazy val models = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    name := "models",
    libraryDependencies ++= Seq(
      "org.apache.commons" % "commons-lang3" % "3.9",
      "com.h2database" % "h2" % "1.4.199",
      "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.8",
      "ch.qos.logback" % "logback-classic" % "1.2.3",

      "junit" % "junit" % "4.12",
      "com.novocode" % "junit-interface" % "0.11" % Test,

      "io.ebean" % "ebean" % "11.38.1",
      "io.ebean" % "ebean-agent" % "11.38.1",

      "org.projectlombok" % "lombok" % "1.18.8"
    )
  )