-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: salefinding_api
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `salefinding_api`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `salefinding_api` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `salefinding_api`;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` VALUES (1,'Uniqlo','http://www.uniqlo.com','2017-07-09 21:26:57','2017-07-09 21:26:57'),(2,'Adidas','http://shop.adidas.com','2017-07-11 00:59:57','2017-07-12 22:52:22'),(3,'Pedro','http://www.pedroshoes.com','2017-07-12 22:52:22','2017-07-12 22:52:22'),(4,'Charles & Keith','http://www.charleskeith.com','2017-07-23 10:50:29','2017-07-23 10:50:29');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) NOT NULL,
  `cat1` varchar(200) DEFAULT NULL,
  `cat2` varchar(200) DEFAULT NULL,
  `cat3` varchar(200) DEFAULT NULL,
  `cat4` varchar(200) DEFAULT NULL,
  `cat5` varchar(200) DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`brand_id`,`cat1`,`cat2`,`cat3`,`cat4`,`cat5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `short_code` varchar(45) DEFAULT NULL,
  `currency_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Vietnam','VN',1,'2017-07-09 21:26:35','2017-07-14 23:51:51'),(2,'Singapore','SG',2,'2017-07-09 21:26:35','2017-07-14 23:51:19'),(3,'Malaysia','ML',3,'2017-07-09 21:26:35','2017-07-14 23:51:39');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `short_code` varchar(3) DEFAULT NULL,
  `vnd_exchange_rate` decimal(10,2) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (1,'Vietnam dong','VND',1.00,'2017-07-14 23:49:02','2017-07-14 23:49:02'),(2,'Singapore Dollar','SGD',17000.00,'2017-07-14 23:44:31','2017-08-15 16:33:09'),(3,'Malaysian Ringgit','MYR',5500.00,'2017-07-14 23:44:31','2017-08-15 16:33:09');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` text,
  `code` varchar(200) NOT NULL,
  `on_sale` tinyint(1) NOT NULL DEFAULT '0',
  `sale_rate` int(11) DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `brand` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_detail`
--

DROP TABLE IF EXISTS `item_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `brand_id` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `old_price` decimal(19,2) DEFAULT NULL,
  `item_price` decimal(19,2) NOT NULL,
  `special_price` decimal(19,2) DEFAULT NULL,
  `sale_rate` int(11) DEFAULT NULL,
  `on_sale` tinyint(1) NOT NULL DEFAULT '0',
  `url` text,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Uniq` (`item_id`,`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_detail`
--

LOCK TABLES `item_detail` WRITE;
/*!40000 ALTER TABLE `item_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_stock`
--

DROP TABLE IF EXISTS `item_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_stock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `color_code` varchar(100) DEFAULT NULL,
  `size_code` varchar(100) DEFAULT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`item_id`,`color_code`,`size_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_stock`
--

LOCK TABLES `item_stock` WRITE;
/*!40000 ALTER TABLE `item_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_address_id` bigint(20) NOT NULL,
  `order_detail` text,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `is_actived` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total_spent_money` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_address`
--

DROP TABLE IF EXISTS `user_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone` date DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_address`
--

LOCK TABLES `user_address` WRITE;
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_item_subcribe`
--

DROP TABLE IF EXISTS `user_item_subcribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_item_subcribe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `entity_type` varchar(50) NOT NULL,
  `entity_id` bigint(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_item_subcribe`
--

LOCK TABLES `user_item_subcribe` WRITE;
/*!40000 ALTER TABLE `user_item_subcribe` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_item_subcribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `crawler`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `crawler` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `crawler`;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` VALUES (1,'Uniqlo','http://www.uniqlo.com','2017-07-09 21:26:57','2017-07-09 21:26:57'),(2,'Adidas','http://shop.adidas.com','2017-07-11 00:59:57','2017-07-12 22:52:22'),(3,'Pedro','http://www.pedroshoes.com','2017-07-12 22:52:22','2017-07-12 22:52:22'),(4,'Charles & Keith','http://www.charleskeith.com','2017-07-23 10:50:29','2017-07-23 10:50:29');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brand_country`
--

DROP TABLE IF EXISTS `brand_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand_country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `crawler_info_id` bigint(20) NOT NULL,
  `crawler_class` text NOT NULL,
  `parser_class` text NOT NULL,
  `category_parser_class` text NOT NULL,
  `crawler_factory_class` text NOT NULL,
  `start_url` text NOT NULL,
  `check_stock_api_uri` text,
  `filter` text NOT NULL,
  `politeness_delay` int(11) NOT NULL DEFAULT '0',
  `number_of_thread` int(11) NOT NULL DEFAULT '1',
  `last_crawling_time` bigint(20) NOT NULL DEFAULT '0',
  `last_crawling_at` datetime DEFAULT NULL,
  `use_selenium` tinyint(1) NOT NULL DEFAULT '0',
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand_country`
--

LOCK TABLES `brand_country` WRITE;
/*!40000 ALTER TABLE `brand_country` DISABLE KEYS */;
INSERT INTO `brand_country` VALUES (1,1,2,0,'com.salefinding.crawler.actions.BaseCrawler','com.salefinding.crawler.parser.UniqloParser','com.salefinding.crawler.category.UniqloCategoryParser','com.salefinding.crawler.actions.NoPagingBrandCrawlerFactory','http://www.uniqlo.com/sg/','http://www.uniqlo.com/sg/store/Sp_FetchInventory.php','store/([a-z]|[0-9]|-)+(\\.(html))$',200,4,512,'2017-09-23 14:09:16',0,1,'2017-07-11 21:59:09','2017-09-23 22:32:03'),(2,1,3,0,'com.salefinding.crawler.actions.BaseCrawler','com.salefinding.crawler.parser.UniqloParser','com.salefinding.crawler.category.UniqloCategoryParser','com.salefinding.crawler.actions.NoPagingBrandCrawlerFactory','http://www.uniqlo.com/my/','http://www.uniqlo.com/my/store/Sp_FetchInventory.php','store/([a-z]|[0-9]|-)+(\\.(html))$',200,4,532,'2017-09-23 14:09:36',0,1,'2017-07-11 22:00:28','2017-09-23 22:32:03'),(3,2,2,0,'com.salefinding.crawler.actions.PagingBrandCrawler','com.salefinding.crawler.parser.AdidasParser','com.salefinding.crawler.category.AdidasCategoryParser','com.salefinding.crawler.actions.PagingBrandCrawlerFactory','http://shop.adidas.com.sg/','http://shop.adidas.com.sg/Sp_hash.php','([a-z]|[0-9]|-|/)+(\\.html)(\\?a=|\\?p=)(.*)$',400,4,3822,'2017-08-15 03:58:02',0,1,'2017-07-11 22:05:37','2017-09-23 22:32:03'),(4,2,3,0,'com.salefinding.crawler.actions.PagingBrandCrawler','com.salefinding.crawler.parser.AdidasParser','com.salefinding.crawler.category.AdidasCategoryParser','com.salefinding.crawler.actions.PagingBrandCrawlerFactory','http://shop.adidas.com.my/','http://shop.adidas.com.my/Sp_hash.php','([a-z]|[0-9]|-|/)+(\\.html)(\\?a=|\\?p=)(.*)$',400,4,3151,'2017-08-15 03:46:51',0,1,'2017-07-11 22:06:48','2017-09-23 22:32:03'),(5,3,2,0,'com.salefinding.crawler.actions.PagingBrandCrawler','com.salefinding.crawler.parser.PedroParser','com.salefinding.crawler.category.PedroCategoryParser','com.salefinding.crawler.actions.PagingBrandCrawlerFactory','http://www.pedroshoes.com/sg/',NULL,'([a-z]|[0-9]|-|\\/)+(\\/)([a-z]|[0-9]|-)+(\\.(html))(.*)$',300,2,1261,'2017-08-15 03:15:21',0,1,'2017-07-12 22:53:24','2017-09-23 22:32:19'),(6,3,3,0,'com.salefinding.crawler.actions.PagingBrandCrawler','com.salefinding.crawler.parser.PedroParser','com.salefinding.crawler.category.PedroCategoryParser','com.salefinding.crawler.actions.PagingBrandCrawlerFactory','http://www.pedroshoes.com/my/',NULL,'([a-z]|[0-9]|-|\\/)+(\\/)([a-z]|[0-9]|-)+(\\.(html))(.*)$',300,2,1200,'2017-08-15 03:25:42',0,1,'2017-07-12 23:25:59','2017-09-23 22:32:03'),(7,4,2,0,'com.salefinding.crawler.actions.PagingBrandCrawler','com.salefinding.crawler.parser.CharlesKeithParser','com.salefinding.crawler.category.CharlesKeithCategoryParser','com.salefinding.crawler.actions.PagingBrandCrawlerFactory','http://www.charleskeith.com/sg/',NULL,'([a-z]|[0-9]|-|\\/)+(\\/)([a-z]|[0-9]|-)+(\\.(html))(.*)$',300,2,5171,'2017-08-15 04:32:33',0,1,'2017-07-23 10:54:28','2017-09-23 22:32:03'),(8,4,3,0,'com.salefinding.crawler.actions.PagingBrandCrawler','com.salefinding.crawler.parser.CharlesKeithParser','com.salefinding.crawler.category.CharlesKeithCategoryParser','com.salefinding.crawler.actions.PagingBrandCrawlerFactory','http://www.charleskeith.com/my/',NULL,'([a-z]|[0-9]|-|\\/)+(\\/)([a-z]|[0-9]|-)+(\\.(html))(.*)$',300,2,5690,'2017-08-15 04:50:12',0,1,'2017-07-23 18:47:53','2017-09-23 22:32:03');
/*!40000 ALTER TABLE `brand_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) NOT NULL,
  `cat1` varchar(200) DEFAULT NULL,
  `cat2` varchar(200) DEFAULT NULL,
  `cat3` varchar(200) DEFAULT NULL,
  `cat4` varchar(200) DEFAULT NULL,
  `cat5` varchar(200) DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`brand_id`,`cat1`,`cat2`,`cat3`,`cat4`,`cat5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_crawler`
--

DROP TABLE IF EXISTS `category_crawler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_crawler` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `cat1` varchar(200) DEFAULT NULL,
  `cat2` varchar(200) DEFAULT NULL,
  `cat3` varchar(200) DEFAULT NULL,
  `cat4` varchar(200) DEFAULT NULL,
  `cat5` varchar(200) DEFAULT NULL,
  `url` text,
  `available` tinyint(1) DEFAULT '1',
  `crawl_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`brand_id`,`cat1`,`cat2`,`cat3`,`cat4`,`cat5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_crawler`
--

LOCK TABLES `category_crawler` WRITE;
/*!40000 ALTER TABLE `category_crawler` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_crawler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `short_code` varchar(45) DEFAULT NULL,
  `currency_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Vietnam','VN',1,'2017-07-09 21:26:35','2017-07-14 23:51:51'),(2,'Singapore','SG',2,'2017-07-09 21:26:35','2017-07-14 23:51:19'),(3,'Malaysia','ML',3,'2017-07-09 21:26:35','2017-07-14 23:51:39');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crawler_info`
--

DROP TABLE IF EXISTS `crawler_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crawler_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `crawler_class` text NOT NULL,
  `crawler_factory_class` text NOT NULL,
  `parser_class` text NOT NULL,
  `category_parser_class` text NOT NULL,
  `filter` varchar(400) NOT NULL,
  `politeness_delay` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crawler_info`
--

LOCK TABLES `crawler_info` WRITE;
/*!40000 ALTER TABLE `crawler_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `crawler_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` text,
  `code` varchar(200) NOT NULL,
  `on_sale` tinyint(1) NOT NULL DEFAULT '0',
  `sale_rate` int(11) DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `brand` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_detail`
--

DROP TABLE IF EXISTS `item_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `brand_id` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `old_price` decimal(19,2) DEFAULT NULL,
  `item_price` decimal(19,2) NOT NULL,
  `special_price` decimal(19,2) DEFAULT NULL,
  `sale_rate` int(11) DEFAULT NULL,
  `on_sale` tinyint(1) NOT NULL DEFAULT '0',
  `url` text,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `crawl_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Uniq` (`item_id`,`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_detail`
--

LOCK TABLES `item_detail` WRITE;
/*!40000 ALTER TABLE `item_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_detail` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `crawler`.`item_detail_AFTER_INSERT` AFTER INSERT ON `item_detail` FOR EACH ROW
BEGIN
	INSERT INTO `crawler`.`price_history` (item_detail_id, old_price_n, price_n, special_price_n) VALUES(NEW.id, NEW.old_price, NEW.item_price, NEW.special_price);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `crawler`.`item_detail_AFTER_UPDATE` AFTER UPDATE ON `item_detail` FOR EACH ROW
BEGIN
	INSERT INTO `crawler`.`price_history` (item_detail_id, old_price, price, special_price, old_price_n, price_n, special_price_n) VALUES(NEW.id, OLD.old_price, OLD.item_price, OLD.special_price, NEW.old_price, NEW.item_price, NEW.special_price);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `item_stock`
--

DROP TABLE IF EXISTS `item_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_stock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `color_code` varchar(100) DEFAULT NULL,
  `size_code` varchar(100) DEFAULT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`item_id`,`color_code`,`size_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_stock`
--

LOCK TABLES `item_stock` WRITE;
/*!40000 ALTER TABLE `item_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_history`
--

DROP TABLE IF EXISTS `price_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_detail_id` bigint(20) NOT NULL,
  `old_price` decimal(19,2) DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `special_price` decimal(19,2) DEFAULT NULL,
  `old_price_n` decimal(19,2) DEFAULT NULL,
  `price_n` decimal(19,2) DEFAULT NULL,
  `special_price_n` decimal(19,2) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_history`
--

LOCK TABLES `price_history` WRITE;
/*!40000 ALTER TABLE `price_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-23 22:32:50
