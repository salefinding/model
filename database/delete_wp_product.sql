-- Remove all attributes from WooCommerce
use wordpress;

DELETE FROM wp_terms WHERE term_id IN (SELECT term_id FROM wp_term_taxonomy WHERE taxonomy LIKE 'pa_%');
DELETE FROM wp_term_taxonomy WHERE taxonomy LIKE 'pa_%';
DELETE FROM wp_term_relationships WHERE term_taxonomy_id not IN (SELECT term_taxonomy_id FROM wp_term_taxonomy);
-- Delete all WooCommerce products
DELETE FROM wp_term_relationships WHERE object_id IN (SELECT ID FROM wp_posts WHERE post_type IN ('product','product_variation'));
DELETE FROM wp_postmeta WHERE post_id IN (SELECT ID FROM wp_posts WHERE post_type IN ('product','product_variation'));
DELETE FROM wp_posts WHERE post_type IN ('product','product_variation');
-- Delete orphaned postmeta
DELETE pm
FROM wp_postmeta pm
LEFT JOIN wp_posts wp ON wp.ID = pm.post_id
WHERE wp.ID IS NULL;

DELETE FROM `wp_postmeta` WHERE `meta_key` = "_thumbnail_id";
UPDATE `wp_postmeta` SET `meta_value` = NULL WHERE `meta_key` = "_product_image_gallery";

DELETE FROM `wp_posts` WHERE `post_type` = "attachment";
DELETE FROM `wp_postmeta` WHERE `meta_key` = "_wp_attached_file";
DELETE FROM `wp_postmeta` WHERE `meta_key` = "_wp_attachment_metadata";

use crawler;
truncate wp_item;
truncate wp_item_detail;
truncate wp_image;
truncate wp_item_image;

