CREATE DATABASE  IF NOT EXISTS `crawler` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `crawler`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 192.168.1.57    Database: crawler
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brand` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `short_code` varchar(45) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` VALUES (1,'Uniqlo','Uniqlo','http://www.uniqlo.com','5f3806f9510a11e886a23215678bade8','2017-07-09 21:26:57','2018-11-02 10:10:11'),(2,'Adidas','Adidas','http://shop.adidas.com','60598cd8510a11e886a23215678bade8','2017-07-11 00:59:57','2018-11-02 10:10:11'),(3,'Pedro','Pedro','http://www.pedroshoes.com','6122d513510a11e886a23215678bade8','2017-07-12 22:52:22','2018-11-02 10:10:11'),(4,'Charles & Keith','C&K','http://www.charleskeith.com','61c64f8b510a11e886a23215678bade8','2017-07-23 10:50:29','2018-11-02 10:10:11');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `brand_BEFORE_INSERT` BEFORE INSERT ON `brand` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `brand_country`
--

DROP TABLE IF EXISTS `brand_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brand_country` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_id` bigint NOT NULL,
  `sub_brand` int DEFAULT NULL,
  `country_id` bigint NOT NULL,
  `group_id` int NOT NULL DEFAULT '1',
  `crawler_info_id` bigint NOT NULL,
  `launcher_class` text,
  `fetcher_class` text,
  `crawler_class` text NOT NULL,
  `parser_class` text NOT NULL,
  `category_parser_class` text NOT NULL,
  `crawler_factory_class` text NOT NULL,
  `start_url` text NOT NULL,
  `wait_for_element` varchar(255) DEFAULT NULL,
  `check_stock_api_uri` text,
  `filter` text NOT NULL,
  `repeat` tinyint(1) NOT NULL DEFAULT '0',
  `politeness_delay` int NOT NULL DEFAULT '0',
  `number_of_thread` int NOT NULL DEFAULT '1',
  `last_crawling_item_count` int DEFAULT '0',
  `last_crawling_time` bigint NOT NULL DEFAULT '0',
  `last_crawling_at` datetime DEFAULT NULL,
  `order` int NOT NULL DEFAULT '0',
  `running` tinyint(1) NOT NULL DEFAULT '0',
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_brand_country_brand1_idx` (`brand_id`),
  KEY `fk_brand_country_country1_idx` (`country_id`),
  CONSTRAINT `fk_brand_country_brand1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`),
  CONSTRAINT `fk_brand_country_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand_country`
--

LOCK TABLES `brand_country` WRITE;
/*!40000 ALTER TABLE `brand_country` DISABLE KEYS */;
INSERT INTO `brand_country` VALUES (1,2,1,2,2,0,'com.salefinding.crawler.launchers.AdidasRestLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.NoPagingBrandCrawler','com.salefinding.crawler.parser.AdidasParser2','com.salefinding.crawler.category.AdidasCategoryParser3','com.salefinding.crawler.crawlers.NoPagingBrandCrawlerFactory','https://www.adidas.com.sg/','1000','https://www.adidas.com.sg/api/products','([A-Z]|[a-z]|[0-9]|-|\\/|_)+(\\.html|(\\?)(.*)(start=)(.*))$',0,500,24,2802,746,'2021-07-03 09:21:17',0,0,0,'72ed6a17510a11e886a23215678bade8','2017-07-11 22:05:37','2021-07-03 10:15:04'),(2,2,1,3,2,0,'com.salefinding.crawler.launchers.AdidasRestLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.NoPagingBrandCrawler','com.salefinding.crawler.parser.AdidasParser2','com.salefinding.crawler.category.AdidasCategoryParser3','com.salefinding.crawler.crawlers.NoPagingBrandCrawlerFactory','https://www.adidas.com.my/','1000','https://www.adidas.com.my/api/products','([A-Z]|[a-z]|[0-9]|-|\\/|_)+(\\.html|(\\?)(.*)(start=)(.*))$',0,500,24,3470,3511,'2021-07-03 10:19:46',0,0,0,'73938f92510a11e886a23215678bade8','2017-07-11 22:06:48','2021-07-03 11:18:59'),(3,3,1,2,3,0,'com.salefinding.crawler.launchers.PedroLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.PagingBrandCrawler','com.salefinding.crawler.parser.PedroParser','com.salefinding.crawler.category.PedroCategoryParser4','com.salefinding.crawler.crawlers.PagingBrandCrawlerFactory','https://www.pedroshoes.com/sg/','.footer_newsletter',NULL,'.*P.*-.*_.*\\.html$',0,1000,24,1656,541,'2021-07-07 13:13:35',5,1,1,'741e9252510a11e886a23215678bade8','2017-07-12 22:53:24','2021-07-07 14:35:06'),(4,3,1,3,3,0,'com.salefinding.crawler.launchers.PedroLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.PagingBrandCrawler','com.salefinding.crawler.parser.PedroParser','com.salefinding.crawler.category.PedroCategoryParser4','com.salefinding.crawler.crawlers.PagingBrandCrawlerFactory','https://www.pedroshoes.com/my/','.footer_newsletter',NULL,'.*P.*-.*_.*\\.html1$',0,1000,24,810,514,'2021-07-07 13:23:28',6,1,1,'74c71d7e510a11e886a23215678bade8','2017-07-12 23:25:59','2021-07-07 14:35:06'),(5,1,1,2,1,0,'com.salefinding.crawler.launchers.UniqloRestLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.NoPagingBrandCrawler','com.salefinding.crawler.parser.UniqloParser','com.salefinding.crawler.category.UniqloCategoryParser2','com.salefinding.crawler.crawlers.NoPagingBrandCrawlerFactory','https://www.uniqlo.com/sg/en/','1000','http://www.uniqlo.com/sg/store/Sp_FetchInventory.php','store/([A-Z]|[a-z]|[0-9]|-|\\/|_)+(\\.html)$',0,500,24,1414,446,'2021-07-03 09:34:59',1,0,0,'719001ca510a11e886a23215678bade8','2017-07-11 21:59:09','2021-07-03 10:15:04'),(6,1,1,3,1,0,'com.salefinding.crawler.launchers.UniqloRestLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.NoPagingBrandCrawler','com.salefinding.crawler.parser.UniqloParser','com.salefinding.crawler.category.UniqloCategoryParser2','com.salefinding.crawler.crawlers.NoPagingBrandCrawlerFactory','https://www.uniqlo.com/my/en/','1000','http://www.uniqlo.com/my/store/Sp_FetchInventory.php','store/([A-Z]|[a-z]|[0-9]|-|\\/|_)+(\\.html)$',0,500,24,1334,453,'2021-07-03 09:42:20',1,0,0,'7254fbc1510a11e886a23215678bade8','2017-07-11 22:00:28','2021-07-03 10:15:04'),(7,4,1,2,3,0,'com.salefinding.crawler.launchers.CrawlerLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.PagingBrandCrawler','com.salefinding.crawler.parser.CharlesKeithParser2','com.salefinding.crawler.category.CharlesKeithCategoryParser2','com.salefinding.crawler.crawlers.PagingBrandCrawlerFactory','https://www.charleskeith.com/sg/','[class*=page_footer]',NULL,'(.+)(\\.html.*|\\?start=[0-9]+&sz=[0-9]+)$',0,1000,10,0,0,'2019-11-11 17:06:11',1,2,0,'75831be6510a11e886a23215678bade8','2017-07-23 10:54:28','2021-06-15 16:06:04'),(8,4,1,3,3,0,'com.salefinding.crawler.launchers.CrawlerLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.PagingBrandCrawler','com.salefinding.crawler.parser.CharlesKeithParser2','com.salefinding.crawler.category.CharlesKeithCategoryParser2','com.salefinding.crawler.crawlers.PagingBrandCrawlerFactory','https://www.charleskeith.com/my/','[class*=page_footer]',NULL,'(.+)(\\.html.*|\\?start=[0-9]+&sz=[0-9]+)$',0,1000,10,0,0,'2019-10-28 09:54:05',2,2,0,'76937002510a11e886a23215678bade8','2017-07-23 18:47:53','2021-06-15 16:06:04'),(9,3,2,2,3,0,'com.salefinding.crawler.launchers.CrawlerLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.PagingBrandCrawler','com.salefinding.crawler.parser.PedroParser','com.salefinding.crawler.category.PedroCategoryParser2','com.salefinding.crawler.crawlers.PagingBrandCrawlerFactory','https://www.pedroshoes.com/sg/men?shipto=sg&gender=men','.footer-container',NULL,'([a-z]|[0-9]|-|\\/)+(\\/)([a-z]|[0-9]|-)+(\\.html|\\?p=[0-9]+)$',0,1000,10,0,44,'2019-10-11 18:19:59',7,0,0,'741e9252510a11e886a23215678bade8','2017-07-12 22:53:24','2021-06-15 16:06:04'),(10,3,2,3,3,0,'com.salefinding.crawler.launchers.CrawlerLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.PagingBrandCrawler','com.salefinding.crawler.parser.PedroParser','com.salefinding.crawler.category.PedroCategoryParser2','com.salefinding.crawler.crawlers.PagingBrandCrawlerFactory','https://www.pedroshoes.com/my/men?shipto=my&gender=men','.footer-container',NULL,'([a-z]|[0-9]|-|\\/)+(\\/)([a-z]|[0-9]|-)+(\\.html|\\?p=[0-9]+)$',0,1000,10,0,44,'2019-10-11 18:20:44',8,0,0,'741e9252510a11e886a23215678bade8','2017-07-12 22:53:24','2021-06-15 16:06:04'),(11,1,1,1,1,0,'com.salefinding.crawler.launchers.UniqloRestLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.NoPagingBrandCrawler','com.salefinding.crawler.parser.UniqloParser','com.salefinding.crawler.category.UniqloCategoryParser2','com.salefinding.crawler.crawlers.NoPagingBrandCrawlerFactory','https://www.uniqlo.com/vn/','1000','','store/([A-Z]|[a-z]|[0-9]|-|\\/|_)+(\\.html)$',0,1000,1,0,0,'2021-06-21 09:18:03',0,2,0,'2c44b24db9124c6fa5b4c4039bc730f9','2017-07-11 21:59:09','2021-06-22 09:38:15'),(12,2,1,1,2,0,'com.salefinding.crawler.launchers.AdidasRestLauncher','com.salefinding.crawler.fetchers.LocalPuppeteerFetcher','com.salefinding.crawler.crawlers.NoPagingBrandCrawler','com.salefinding.crawler.parser.AdidasParser2','com.salefinding.crawler.category.AdidasCategoryParser3','com.salefinding.crawler.crawlers.NoPagingBrandCrawlerFactory','https://www.adidas.com.vn/','1000','','([A-Z]|[a-z]|[0-9]|-|\\/|_)+(\\.html|(\\?)(.*)(start=)(.*))$',0,500,24,3603,958,'2021-07-03 09:49:55',1,0,0,'1703d3c59e71437395752896b2584a5f','2017-07-11 22:06:48','2021-07-03 10:15:04');
/*!40000 ALTER TABLE `brand_country` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `brand_country_BEFORE_INSERT` BEFORE INSERT ON `brand_country` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_id` bigint NOT NULL,
  `sub_brand` int DEFAULT NULL,
  `cat1` varchar(200) DEFAULT NULL,
  `cat2` varchar(200) DEFAULT NULL,
  `cat3` varchar(200) DEFAULT NULL,
  `cat4` varchar(200) DEFAULT NULL,
  `cat5` varchar(200) DEFAULT NULL,
  `shipping_fee` text,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `crawled` tinyint(1) NOT NULL DEFAULT '1',
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`brand_id`,`cat1`,`cat2`,`cat3`,`cat4`,`cat5`),
  CONSTRAINT `fk_category_brand1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `category_BEFORE_INSERT` BEFORE INSERT ON `category` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `category_crawler`
--

DROP TABLE IF EXISTS `category_crawler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_crawler` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_country_id` bigint NOT NULL,
  `brand_id` bigint NOT NULL,
  `sub_brand` int DEFAULT NULL,
  `country_id` bigint NOT NULL,
  `category_id` bigint DEFAULT NULL,
  `cat1` varchar(200) DEFAULT NULL,
  `cat2` varchar(200) DEFAULT NULL,
  `cat3` varchar(200) DEFAULT NULL,
  `cat4` varchar(200) DEFAULT NULL,
  `cat5` varchar(200) DEFAULT NULL,
  `url` text,
  `available` tinyint(1) DEFAULT '1',
  `crawled` tinyint(1) NOT NULL DEFAULT '1',
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `category_crawlercol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category_crawler_category_idx` (`category_id`),
  KEY `fk_category_crawler_country1_idx` (`country_id`),
  KEY `fk_category_crawler_brand1` (`brand_id`),
  CONSTRAINT `fk_category_crawler_brand1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`),
  CONSTRAINT `fk_category_crawler_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `fk_category_crawler_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_crawler`
--

LOCK TABLES `category_crawler` WRITE;
/*!40000 ALTER TABLE `category_crawler` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_crawler` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `category_crawler_BEFORE_INSERT` BEFORE INSERT ON `category_crawler` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `country` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `short_code` varchar(45) DEFAULT NULL,
  `currency_id` bigint NOT NULL,
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Vietnam','VN',1,'85d37909510a11e886a23215678bade8','2017-07-09 21:26:35','2018-05-06 16:50:33'),(2,'Singapore','SG',2,'8653e7e1510a11e886a23215678bade8','2017-07-09 21:26:35','2018-05-06 16:50:34'),(3,'Malaysia','MY',3,'86cb9399510a11e886a23215678bade8','2017-07-09 21:26:35','2021-06-19 18:55:54');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `country_BEFORE_INSERT` BEFORE INSERT ON `country` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `crawler_info`
--

DROP TABLE IF EXISTS `crawler_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crawler_info` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `crawler_class` text NOT NULL,
  `crawler_factory_class` text NOT NULL,
  `parser_class` text NOT NULL,
  `category_parser_class` text NOT NULL,
  `filter` varchar(400) NOT NULL,
  `politeness_delay` int NOT NULL DEFAULT '0',
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crawler_info`
--

LOCK TABLES `crawler_info` WRITE;
/*!40000 ALTER TABLE `crawler_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `crawler_info` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `crawler_info_BEFORE_INSERT` BEFORE INSERT ON `crawler_info` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `crawling_data`
--

DROP TABLE IF EXISTS `crawling_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crawling_data` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_country_id` bigint NOT NULL,
  `run_uid` varchar(45) NOT NULL,
  `url` varchar(500) NOT NULL,
  `should_visit` tinyint(1) NOT NULL DEFAULT '0',
  `visited` tinyint(1) NOT NULL DEFAULT '0',
  `saved_item` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq` (`brand_country_id`,`run_uid`,`url`) /*!80000 INVISIBLE */,
  KEY `crawling_data_brand_country_idx` (`brand_country_id`) /*!80000 INVISIBLE */,
  CONSTRAINT `fk_crawling_data_brand_country` FOREIGN KEY (`brand_country_id`) REFERENCES `brand_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crawling_data`
--

LOCK TABLES `crawling_data` WRITE;
/*!40000 ALTER TABLE `crawling_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `crawling_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crawling_statistics`
--

DROP TABLE IF EXISTS `crawling_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crawling_statistics` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_country_id` bigint NOT NULL,
  `politeness_delay` int DEFAULT '0',
  `number_of_thread` int DEFAULT '1',
  `crawling_item_count` int DEFAULT '0',
  `crawling_time` bigint DEFAULT '0',
  `crawling_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `brand_country_id_idx` (`brand_country_id`) /*!80000 INVISIBLE */,
  CONSTRAINT `brand_country_id` FOREIGN KEY (`brand_country_id`) REFERENCES `brand_country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1144 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crawling_statistics`
--

LOCK TABLES `crawling_statistics` WRITE;
/*!40000 ALTER TABLE `crawling_statistics` DISABLE KEYS */;
INSERT INTO `crawling_statistics` VALUES (1,4,1000,8,485,2110,'2019-04-29 18:01:47','2019-04-29 18:36:58','2019-04-29 18:36:58'),(2,3,1000,8,711,6925,'2019-04-29 18:01:47','2019-04-29 19:57:13','2019-04-29 19:57:13'),(3,7,1000,8,3167,9709,'2019-04-29 18:36:58','2019-04-29 21:18:47','2019-04-29 21:18:47'),(4,9,1000,8,498,1493,'2019-04-29 21:18:47','2019-04-29 21:43:40','2019-04-29 21:43:40'),(5,10,1000,8,376,1763,'2019-04-29 21:43:40','2019-04-29 22:13:04','2019-04-29 22:13:04'),(6,8,1000,8,4000,38607,'2019-04-29 19:57:13','2019-04-30 06:40:40','2019-04-30 06:40:40'),(7,7,1000,8,3135,8324,'2019-05-03 03:34:56','2019-05-03 05:53:41','2019-05-03 05:53:41'),(8,3,1000,8,683,1847,'2019-05-03 05:53:41','2019-05-03 06:24:29','2019-05-03 06:24:29'),(9,4,1000,8,493,1662,'2019-05-03 06:24:29','2019-05-03 06:52:11','2019-05-03 06:52:11'),(10,10,1000,8,318,1923,'2019-05-03 06:52:11','2019-05-03 07:24:14','2019-05-03 07:24:14'),(11,9,1000,8,449,1885,'2019-05-03 07:24:14','2019-05-03 07:55:40','2019-05-03 07:55:40'),(12,1,1000,8,2662,6474,'2019-05-03 14:09:56','2019-05-03 15:57:50','2019-05-03 15:57:50'),(13,2,1000,8,3608,20669,'2019-05-03 14:09:56','2019-05-03 19:54:25','2019-05-03 19:54:25'),(14,3,1000,8,683,1315,'2019-05-04 02:10:51','2019-05-04 02:32:47','2019-05-04 02:32:47'),(15,8,1000,8,3985,12128,'2019-05-04 02:35:16','2019-05-04 05:57:24','2019-05-04 05:57:24'),(16,4,1000,8,494,1110,'2019-05-04 06:01:56','2019-05-04 06:20:27','2019-05-04 06:20:27'),(17,3,1000,8,683,1463,'2019-05-04 06:01:56','2019-05-04 06:26:20','2019-05-04 06:26:20'),(18,7,1000,8,3197,5845,'2019-05-04 11:24:25','2019-05-04 13:01:50','2019-05-04 13:01:50'),(19,8,1000,8,3989,6617,'2019-05-04 11:24:25','2019-05-04 13:14:43','2019-05-04 13:14:43'),(20,3,1000,8,683,1307,'2019-05-05 04:01:52','2019-05-05 04:23:39','2019-05-05 04:23:39'),(21,4,1000,8,494,1416,'2019-05-05 04:01:52','2019-05-05 04:25:28','2019-05-05 04:25:28'),(22,9,1000,8,449,1105,'2019-05-05 04:23:39','2019-05-05 04:42:04','2019-05-05 04:42:04'),(23,10,1000,8,333,1110,'2019-05-05 04:25:28','2019-05-05 04:43:59','2019-05-05 04:43:59'),(24,7,1000,8,3197,4886,'2019-05-05 05:09:05','2019-05-05 06:30:32','2019-05-05 06:30:32'),(25,8,1000,8,3987,6499,'2019-05-05 05:09:05','2019-05-05 06:57:24','2019-05-05 06:57:24'),(26,1,1000,8,2653,4134,'2019-05-05 07:01:03','2019-05-05 08:09:57','2019-05-05 08:09:57'),(27,4,1000,8,494,985,'2019-05-05 08:09:57','2019-05-05 08:26:23','2019-05-05 08:26:23'),(28,3,1000,8,683,1126,'2019-05-05 08:26:23','2019-05-05 08:45:10','2019-05-05 08:45:10'),(29,2,1000,8,3614,8120,'2019-05-05 06:30:32','2019-05-05 08:45:53','2019-05-05 08:45:53'),(30,10,1000,8,333,914,'2019-05-05 08:45:10','2019-05-05 09:00:24','2019-05-05 09:00:24'),(31,9,1000,8,449,1070,'2019-05-05 08:47:47','2019-05-05 09:05:37','2019-05-05 09:05:37'),(32,8,1000,8,3989,6032,'2019-05-05 09:13:10','2019-05-05 10:53:43','2019-05-05 10:53:43'),(33,2,1000,8,3611,7246,'2019-05-05 09:13:10','2019-05-05 11:13:57','2019-05-05 11:13:57'),(34,1,1000,8,2638,3979,'2019-05-05 11:13:57','2019-05-05 12:20:16','2019-05-05 12:20:16'),(35,7,1000,8,3197,5348,'2019-05-05 10:53:43','2019-05-05 12:22:52','2019-05-05 12:22:52'),(36,3,1000,8,683,1161,'2019-05-05 12:20:41','2019-05-05 12:40:02','2019-05-05 12:40:02'),(37,9,1000,8,449,1199,'2019-05-05 12:24:08','2019-05-05 12:44:07','2019-05-05 12:44:07'),(38,4,1000,8,494,967,'2019-05-05 12:40:02','2019-05-05 12:56:10','2019-05-05 12:56:10'),(39,10,1000,8,333,1068,'2019-05-05 12:44:07','2019-05-05 13:01:56','2019-05-05 13:01:56'),(40,8,1000,8,4195,5921,'2019-05-07 14:36:52','2019-05-07 16:15:33','2019-05-07 16:15:33'),(41,2,1000,8,3613,7344,'2019-05-07 14:36:52','2019-05-07 16:39:16','2019-05-07 16:39:16'),(42,7,1000,8,3456,4905,'2019-05-07 16:15:33','2019-05-07 17:37:18','2019-05-07 17:37:18'),(43,1,1000,8,2652,4197,'2019-05-07 16:39:16','2019-05-07 17:49:14','2019-05-07 17:49:14'),(44,4,1000,8,509,1087,'2019-05-07 17:39:15','2019-05-07 17:57:23','2019-05-07 17:57:23'),(45,3,1000,8,654,1576,'2019-05-07 17:50:26','2019-05-07 18:16:43','2019-05-07 18:16:43'),(46,10,1000,8,315,1165,'2019-05-07 17:57:23','2019-05-07 18:16:49','2019-05-07 18:16:49'),(47,9,1000,8,0,1322,'2019-05-07 18:16:43','2019-05-07 18:38:45','2019-05-07 18:38:45'),(48,9,1000,8,459,1174,'2019-05-08 13:34:22','2019-05-08 13:53:56','2019-05-08 13:53:56'),(49,3,1000,8,701,1334,'2019-05-08 13:34:22','2019-05-08 13:56:36','2019-05-08 13:56:36'),(50,9,1000,8,460,1191,'2019-05-08 14:06:08','2019-05-08 14:26:00','2019-05-08 14:26:00'),(51,3,1000,8,701,1276,'2019-05-08 14:06:08','2019-05-08 14:27:25','2019-05-08 14:27:25'),(52,2,1000,6,3619,7605,'2019-05-09 03:46:00','2019-05-09 05:52:45','2019-05-09 05:52:45'),(53,8,1000,6,4188,10589,'2019-05-09 03:46:00','2019-05-09 06:42:30','2019-05-09 06:42:30'),(54,7,1000,6,3458,5797,'2019-05-09 05:52:45','2019-05-09 07:29:22','2019-05-09 07:29:22'),(55,4,1000,6,503,1399,'2019-05-09 07:31:07','2019-05-09 07:54:27','2019-05-09 07:54:27'),(56,1,1000,6,2659,4538,'2019-05-09 06:42:30','2019-05-09 07:58:08','2019-05-09 07:58:08'),(57,10,1000,6,343,1127,'2019-05-09 07:59:17','2019-05-09 08:18:05','2019-05-09 08:18:05'),(58,3,1000,6,701,1457,'2019-05-09 07:54:27','2019-05-09 08:18:44','2019-05-09 08:18:44'),(59,9,1000,6,460,1124,'2019-05-09 08:18:05','2019-05-09 08:36:50','2019-05-09 08:36:50'),(60,6,1000,8,848,1340,'2019-05-10 13:49:12','2019-05-10 14:11:33','2019-05-10 14:11:33'),(61,5,1000,8,910,1522,'2019-05-10 13:49:12','2019-05-10 14:14:35','2019-05-10 14:14:35'),(62,2,1000,8,3565,7076,'2019-05-10 18:37:31','2019-05-10 20:35:28','2019-05-10 20:35:28'),(63,8,1000,8,4082,8904,'2019-05-10 18:37:31','2019-05-10 21:05:56','2019-05-10 21:05:56'),(64,7,1000,8,3318,5772,'2019-05-10 20:35:28','2019-05-10 22:11:40','2019-05-10 22:11:40'),(65,1,1000,8,2621,4127,'2019-05-10 21:05:56','2019-05-10 22:14:44','2019-05-10 22:14:44'),(66,4,1000,8,506,1435,'2019-05-10 22:16:07','2019-05-10 22:40:02','2019-05-10 22:40:02'),(67,3,1000,8,700,1600,'2019-05-10 22:14:36','2019-05-10 22:41:17','2019-05-10 22:41:17'),(68,10,1000,8,346,1193,'2019-05-10 22:40:02','2019-05-10 22:59:56','2019-05-10 22:59:56'),(69,9,1000,8,461,1302,'2019-05-10 22:41:17','2019-05-10 23:02:59','2019-05-10 23:02:59'),(70,5,1000,8,910,1380,'2019-05-10 23:03:36','2019-05-10 23:26:37','2019-05-10 23:26:37'),(71,6,1000,8,848,1506,'2019-05-10 23:03:36','2019-05-10 23:28:43','2019-05-10 23:28:43'),(72,1,1000,8,1868,4018,'2019-05-11 15:31:37','2019-05-11 16:38:35','2019-05-11 16:38:35'),(73,7,1000,8,3319,4934,'2019-05-11 15:31:37','2019-05-11 16:53:52','2019-05-11 16:53:52'),(74,4,1000,8,505,1049,'2019-05-11 16:38:35','2019-05-11 16:56:05','2019-05-11 16:56:05'),(75,10,1000,8,345,884,'2019-05-11 16:56:05','2019-05-11 17:10:49','2019-05-11 17:10:49'),(76,3,1000,8,700,1270,'2019-05-11 16:53:52','2019-05-11 17:15:03','2019-05-11 17:15:03'),(77,9,1000,8,461,1057,'2019-05-11 17:10:50','2019-05-11 17:28:27','2019-05-11 17:28:27'),(78,8,1000,8,4082,5841,'2019-05-12 08:51:29','2019-05-12 10:28:51','2019-05-12 10:28:51'),(79,7,1000,8,3319,4652,'2019-05-12 10:28:51','2019-05-12 11:46:23','2019-05-12 11:46:23'),(80,8,1000,8,4082,5200,'2019-05-12 12:37:02','2019-05-12 14:03:42','2019-05-12 14:03:42'),(81,8,1000,8,4082,5547,'2019-05-12 14:58:30','2019-05-12 16:30:57','2019-05-12 16:30:57'),(82,7,1000,8,3319,4196,'2019-05-12 16:30:57','2019-05-12 17:40:54','2019-05-12 17:40:54'),(83,2,1500,8,1340,3432,'2019-05-15 04:31:28','2019-05-15 05:28:41','2019-05-15 05:28:41'),(84,8,1500,8,4127,7802,'2019-05-15 04:31:28','2019-05-15 06:41:31','2019-05-15 06:41:31'),(85,7,1500,8,3378,5575,'2019-05-15 05:28:41','2019-05-15 07:01:37','2019-05-15 07:01:37'),(86,1,1500,8,1133,3071,'2019-05-15 17:13:35','2019-05-15 18:04:46','2019-05-15 18:04:46'),(87,8,1500,8,4127,8315,'2019-05-15 17:13:35','2019-05-15 19:32:11','2019-05-15 19:32:11'),(88,7,1500,8,3378,6132,'2019-05-15 18:04:46','2019-05-15 19:46:58','2019-05-15 19:46:58'),(89,8,1500,8,4127,7859,'2019-05-16 18:43:37','2019-05-16 20:54:37','2019-05-16 20:54:37'),(90,2,1500,8,3461,6225,'2019-05-17 06:02:52','2019-05-17 07:46:37','2019-05-17 07:46:37'),(91,8,1500,8,4145,8234,'2019-05-17 06:02:52','2019-05-17 08:20:06','2019-05-17 08:20:06'),(92,7,1500,8,3396,5692,'2019-05-17 07:46:37','2019-05-17 09:21:30','2019-05-17 09:21:30'),(93,1,1500,8,1806,3884,'2019-05-17 08:20:06','2019-05-17 09:24:51','2019-05-17 09:24:51'),(94,2,1500,8,3453,7996,'2019-05-17 17:03:30','2019-05-17 19:16:47','2019-05-17 19:16:47'),(95,8,1500,8,4145,8148,'2019-05-17 17:03:30','2019-05-17 19:19:19','2019-05-17 19:19:19'),(96,1,1500,8,2441,6015,'2019-05-17 19:19:19','2019-05-17 20:59:35','2019-05-17 20:59:35'),(97,7,1500,8,3396,6589,'2019-05-17 19:16:47','2019-05-17 21:06:36','2019-05-17 21:06:36'),(98,8,1500,8,4157,7560,'2019-05-18 17:38:20','2019-05-18 19:44:20','2019-05-18 19:44:20'),(99,2,1500,8,3,422,'2019-05-18 19:44:20','2019-05-18 19:51:22','2019-05-18 19:51:22'),(100,7,1500,8,3398,5491,'2019-05-18 19:51:22','2019-05-18 21:22:53','2019-05-18 21:22:53'),(101,1,1500,8,2,455,'2019-05-18 21:26:14','2019-05-18 21:33:50','2019-05-18 21:33:50'),(102,4,1500,8,770,1581,'2019-05-18 21:33:51','2019-05-18 22:00:12','2019-05-18 22:00:12'),(103,4,1500,8,770,1498,'2019-05-19 07:28:32','2019-05-19 07:53:31','2019-05-19 07:53:31'),(104,2,1500,8,4,576,'2019-05-19 07:53:31','2019-05-19 08:03:07','2019-05-19 08:03:07'),(105,4,1500,8,770,1491,'2019-05-19 08:12:11','2019-05-19 08:37:03','2019-05-19 08:37:03'),(106,7,1500,8,3398,5467,'2019-05-19 08:37:03','2019-05-19 10:08:10','2019-05-19 10:08:10'),(107,8,1500,8,4157,6787,'2019-05-19 10:08:11','2019-05-19 12:01:18','2019-05-19 12:01:18'),(108,2,1500,8,7,600,'2019-05-19 12:05:08','2019-05-19 12:15:09','2019-05-19 12:15:09'),(109,1,1500,8,31,1313,'2019-05-19 12:15:09','2019-05-19 12:37:03','2019-05-19 12:37:03'),(110,4,1500,8,770,1510,'2019-05-20 10:47:06','2019-05-20 11:12:16','2019-05-20 11:12:16'),(111,4,1500,8,770,1486,'2019-05-20 11:15:36','2019-05-20 11:40:23','2019-05-20 11:40:23'),(112,2,1500,8,12,592,'2019-05-20 11:47:23','2019-05-20 11:57:15','2019-05-20 11:57:15'),(113,3,1500,8,793,1507,'2019-05-20 11:47:23','2019-05-20 12:12:31','2019-05-20 12:12:31'),(114,1,1500,8,21,835,'2019-05-20 12:12:31','2019-05-20 12:26:26','2019-05-20 12:26:26'),(115,4,1500,8,770,1476,'2019-05-20 12:26:28','2019-05-20 12:51:05','2019-05-20 12:51:05'),(116,4,1500,8,770,1470,'2019-05-20 14:42:55','2019-05-20 15:07:25','2019-05-20 15:07:25'),(117,3,1500,8,793,1530,'2019-05-20 14:42:55','2019-05-20 15:08:25','2019-05-20 15:08:25'),(118,7,1500,8,3404,5487,'2019-05-20 15:08:25','2019-05-20 16:39:52','2019-05-20 16:39:52'),(119,9,1500,8,560,1185,'2019-05-20 17:07:54','2019-05-20 17:27:40','2019-05-20 17:27:40'),(120,10,1500,8,587,1360,'2019-05-20 17:27:40','2019-05-20 17:50:21','2019-05-20 17:50:21'),(121,8,1500,8,4157,7323,'2019-05-20 17:07:54','2019-05-20 19:09:58','2019-05-20 19:09:58'),(122,6,1500,5,852,2438,'2019-05-20 19:14:04','2019-05-20 19:54:43','2019-05-20 19:54:43'),(123,5,1500,5,890,2539,'2019-05-20 19:14:04','2019-05-20 19:56:23','2019-05-20 19:56:23'),(124,1,1500,8,2128,5760,'2019-05-21 11:44:40','2019-05-21 13:20:41','2019-05-21 13:20:41'),(125,2,1500,8,3494,6779,'2019-05-21 11:44:40','2019-05-21 13:37:40','2019-05-21 13:37:40'),(126,4,1500,10,795,1591,'2019-05-21 15:38:22','2019-05-21 16:04:53','2019-05-21 16:04:53'),(127,3,1500,10,815,1624,'2019-05-21 15:38:22','2019-05-21 16:05:26','2019-05-21 16:05:26'),(128,3,1000,10,815,1275,'2019-05-21 18:07:47','2019-05-21 18:29:02','2019-05-21 18:29:02'),(129,4,1000,10,795,1297,'2019-05-21 18:07:47','2019-05-21 18:29:24','2019-05-21 18:29:24'),(130,7,1000,10,4397,5779,'2019-05-21 18:29:24','2019-05-21 20:05:44','2019-05-21 20:05:44'),(131,8,1000,10,4200,6650,'2019-05-21 18:29:02','2019-05-21 20:19:52','2019-05-21 20:19:52'),(132,1,1000,10,2385,4683,'2019-05-21 20:05:44','2019-05-21 21:23:48','2019-05-21 21:23:48'),(133,1,1000,10,2381,4154,'2019-05-22 10:36:54','2019-05-22 11:46:08','2019-05-22 11:46:08'),(134,9,1000,10,576,1148,'2019-05-22 13:29:48','2019-05-22 13:48:57','2019-05-22 13:48:57'),(135,10,1000,10,585,1058,'2019-05-22 14:33:47','2019-05-22 14:51:25','2019-05-22 14:51:25'),(136,2,1000,10,3468,6673,'2019-05-22 14:33:47','2019-05-22 16:25:00','2019-05-22 16:25:00'),(137,5,1000,5,897,2080,'2019-05-22 16:26:18','2019-05-22 17:00:58','2019-05-22 17:00:58'),(138,6,1000,5,844,2788,'2019-05-22 16:26:18','2019-05-22 17:12:46','2019-05-22 17:12:46'),(139,3,1000,10,830,1244,'2019-05-22 17:24:35','2019-05-22 17:45:20','2019-05-22 17:45:20'),(140,4,1000,10,738,1279,'2019-05-22 17:24:35','2019-05-22 17:45:55','2019-05-22 17:45:55'),(141,7,1000,10,4392,6328,'2019-05-22 17:45:55','2019-05-22 19:31:24','2019-05-22 19:31:24'),(142,8,1000,10,4202,8163,'2019-05-22 17:45:20','2019-05-22 20:01:24','2019-05-22 20:01:24'),(143,1,1000,10,2376,5770,'2019-05-22 19:31:24','2019-05-22 21:07:34','2019-05-22 21:07:34'),(144,3,1000,10,830,1181,'2019-05-23 03:59:53','2019-05-23 04:19:35','2019-05-23 04:19:35'),(145,4,1000,10,738,1220,'2019-05-23 03:59:53','2019-05-23 04:20:14','2019-05-23 04:20:14'),(146,7,1000,10,4402,5032,'2019-05-23 04:20:14','2019-05-23 05:44:07','2019-05-23 05:44:07'),(147,8,1000,10,4162,6261,'2019-05-23 04:19:35','2019-05-23 06:03:56','2019-05-23 06:03:56'),(148,4,1000,10,738,1088,'2019-05-23 07:53:16','2019-05-23 08:11:25','2019-05-23 08:11:25'),(149,3,1000,10,829,1136,'2019-05-23 07:53:16','2019-05-23 08:12:12','2019-05-23 08:12:12'),(150,7,1000,10,4402,4952,'2019-05-23 08:12:12','2019-05-23 09:34:45','2019-05-23 09:34:45'),(151,8,1000,10,4162,5028,'2019-05-23 08:11:25','2019-05-23 09:35:13','2019-05-23 09:35:13'),(152,4,1000,10,680,1035,'2019-05-23 12:01:03','2019-05-23 12:18:19','2019-05-23 12:18:19'),(153,3,1000,10,744,1075,'2019-05-23 12:01:03','2019-05-23 12:18:59','2019-05-23 12:18:59'),(154,7,1000,10,4402,4974,'2019-05-23 12:18:59','2019-05-23 13:41:54','2019-05-23 13:41:54'),(155,8,1000,10,4162,6638,'2019-05-23 12:18:19','2019-05-23 14:08:57','2019-05-23 14:08:57'),(156,1,1000,10,2365,3967,'2019-05-23 13:41:54','2019-05-23 14:48:01','2019-05-23 14:48:01'),(157,9,1000,10,454,734,'2019-05-23 14:48:01','2019-05-23 15:00:16','2019-05-23 15:00:16'),(158,10,1000,10,495,846,'2019-05-23 15:00:16','2019-05-23 15:14:22','2019-05-23 15:14:22'),(159,2,1000,10,3440,8546,'2019-05-23 14:10:51','2019-05-23 16:33:17','2019-05-23 16:33:17'),(160,5,1000,5,894,2287,'2019-05-23 16:34:43','2019-05-23 17:12:50','2019-05-23 17:12:50'),(161,6,1000,5,837,2739,'2019-05-23 16:34:43','2019-05-23 17:20:22','2019-05-23 17:20:22'),(162,4,1000,10,688,1142,'2019-05-27 06:08:51','2019-05-27 06:27:53','2019-05-27 06:27:53'),(163,3,1000,10,753,1237,'2019-05-27 06:08:51','2019-05-27 06:29:28','2019-05-27 06:29:28'),(164,4,1000,10,693,1207,'2019-05-28 06:24:54','2019-05-28 06:45:01','2019-05-28 06:45:01'),(165,3,1000,10,755,1222,'2019-05-28 06:24:54','2019-05-28 06:45:17','2019-05-28 06:45:17'),(166,7,1000,10,4254,4834,'2019-05-28 06:45:17','2019-05-28 08:05:51','2019-05-28 08:05:51'),(167,8,1000,10,4064,5764,'2019-05-28 06:45:01','2019-05-28 08:21:06','2019-05-28 08:21:06'),(168,1,1000,10,2238,4491,'2019-05-28 08:05:51','2019-05-28 09:20:42','2019-05-28 09:20:42'),(169,9,1000,10,469,844,'2019-05-28 09:20:43','2019-05-28 09:34:47','2019-05-28 09:34:47'),(170,10,1000,10,505,916,'2019-05-28 09:34:47','2019-05-28 09:50:03','2019-05-28 09:50:03'),(171,2,1000,10,3422,5586,'2019-05-28 08:24:55','2019-05-28 09:58:01','2019-05-28 09:58:01'),(172,5,1000,5,926,2417,'2019-05-28 09:59:57','2019-05-28 10:40:15','2019-05-28 10:40:15'),(173,6,1000,5,844,2681,'2019-05-28 09:59:57','2019-05-28 10:44:39','2019-05-28 10:44:39'),(174,4,1000,10,715,1359,'2019-05-30 18:26:44','2019-05-30 18:49:24','2019-05-30 18:49:24'),(175,3,1000,10,757,1369,'2019-05-30 18:26:44','2019-05-30 18:49:34','2019-05-30 18:49:34'),(176,7,1000,10,3261,4150,'2019-05-30 18:49:34','2019-05-30 19:58:45','2019-05-30 19:58:45'),(177,8,1000,10,4011,6360,'2019-05-30 18:49:24','2019-05-30 20:35:25','2019-05-30 20:35:25'),(178,1,1000,10,2264,6304,'2019-05-30 19:58:45','2019-05-30 21:43:50','2019-05-30 21:43:50'),(179,9,1000,10,474,816,'2019-05-30 21:43:50','2019-05-30 21:57:27','2019-05-30 21:57:27'),(180,10,1000,10,510,950,'2019-05-30 21:57:27','2019-05-30 22:13:17','2019-05-30 22:13:17'),(181,2,1000,10,3446,6925,'2019-05-30 20:39:12','2019-05-30 22:34:37','2019-05-30 22:34:37'),(182,6,1000,5,838,2378,'2019-05-30 22:36:35','2019-05-30 23:16:14','2019-05-30 23:16:14'),(183,5,1000,5,933,2468,'2019-05-30 22:36:35','2019-05-30 23:17:43','2019-05-30 23:17:43'),(184,3,1000,10,763,1208,'2019-06-02 17:35:43','2019-06-02 17:55:51','2019-06-02 17:55:51'),(185,4,1000,10,727,1230,'2019-06-02 17:35:43','2019-06-02 17:56:14','2019-06-02 17:56:14'),(186,3,1000,10,763,1192,'2019-06-03 02:59:55','2019-06-03 03:19:48','2019-06-03 03:19:48'),(187,4,1000,10,727,1311,'2019-06-03 02:59:55','2019-06-03 03:21:47','2019-06-03 03:21:47'),(188,7,1000,10,3216,3937,'2019-06-03 03:21:47','2019-06-03 04:27:25','2019-06-03 04:27:25'),(189,8,1000,10,3990,6055,'2019-06-03 03:19:48','2019-06-03 05:00:44','2019-06-03 05:00:44'),(190,1,1000,10,2574,5556,'2019-06-03 04:27:25','2019-06-03 06:00:01','2019-06-03 06:00:01'),(191,9,1000,10,483,918,'2019-06-03 06:00:01','2019-06-03 06:15:19','2019-06-03 06:15:19'),(192,10,1000,10,515,1039,'2019-06-03 06:15:19','2019-06-03 06:32:38','2019-06-03 06:32:38'),(193,2,1000,10,3888,8203,'2019-06-03 05:04:36','2019-06-03 07:21:19','2019-06-03 07:21:19'),(194,5,1000,5,913,8182,'2019-06-03 07:23:38','2019-06-03 09:40:00','2019-06-03 09:40:00'),(195,6,1000,5,852,10737,'2019-06-03 07:23:38','2019-06-03 10:22:35','2019-06-03 10:22:35'),(196,3,1000,10,763,1460,'2019-06-04 15:09:29','2019-06-04 15:33:50','2019-06-04 15:33:50'),(197,4,1000,10,727,1605,'2019-06-04 15:09:29','2019-06-04 15:36:14','2019-06-04 15:36:14'),(198,7,1000,10,3336,5647,'2019-06-04 15:36:14','2019-06-04 17:10:22','2019-06-04 17:10:22'),(199,8,1000,10,4109,6883,'2019-06-04 15:33:50','2019-06-04 17:28:33','2019-06-04 17:28:33'),(200,3,1000,10,763,1250,'2019-06-04 20:19:33','2019-06-04 20:40:23','2019-06-04 20:40:23'),(201,4,1000,10,727,1298,'2019-06-04 20:19:33','2019-06-04 20:41:12','2019-06-04 20:41:12'),(202,7,1000,10,3338,4104,'2019-06-04 20:41:12','2019-06-04 21:49:37','2019-06-04 21:49:37'),(203,8,1000,10,4109,6262,'2019-06-04 20:40:23','2019-06-04 22:24:46','2019-06-04 22:24:46'),(204,1,1000,10,2607,5122,'2019-06-04 21:49:37','2019-06-04 23:14:59','2019-06-04 23:14:59'),(205,9,1000,10,483,962,'2019-06-04 23:14:59','2019-06-04 23:31:02','2019-06-04 23:31:02'),(206,10,1000,10,515,1092,'2019-06-04 23:31:02','2019-06-04 23:49:14','2019-06-04 23:49:14'),(207,2,1000,10,3865,9152,'2019-06-04 22:29:13','2019-06-05 01:01:45','2019-06-05 01:01:45'),(208,5,1000,5,902,2775,'2019-06-05 01:04:10','2019-06-05 01:50:26','2019-06-05 01:50:26'),(209,6,1000,5,846,2829,'2019-06-05 01:04:10','2019-06-05 01:51:19','2019-06-05 01:51:19'),(210,4,1000,10,0,175,'2019-06-06 05:17:58','2019-06-06 05:20:53','2019-06-06 05:20:53'),(211,3,1000,10,0,187,'2019-06-06 05:17:58','2019-06-06 05:21:05','2019-06-06 05:21:05'),(212,7,1000,10,0,273,'2019-06-06 05:21:05','2019-06-06 05:25:38','2019-06-06 05:25:38'),(213,8,1000,10,0,299,'2019-06-06 05:20:53','2019-06-06 05:25:53','2019-06-06 05:25:53'),(214,2,1000,10,0,485,'2019-06-06 05:25:53','2019-06-06 05:33:59','2019-06-06 05:33:59'),(215,1,1000,10,0,524,'2019-06-06 05:25:38','2019-06-06 05:34:23','2019-06-06 05:34:23'),(216,9,1000,10,0,184,'2019-06-06 05:33:59','2019-06-06 05:37:04','2019-06-06 05:37:04'),(217,10,1000,10,0,180,'2019-06-06 05:34:23','2019-06-06 05:37:24','2019-06-06 05:37:24'),(218,5,1000,5,0,373,'2019-06-06 05:37:24','2019-06-06 05:43:38','2019-06-06 05:43:38'),(219,6,1000,5,0,404,'2019-06-06 05:37:24','2019-06-06 05:44:08','2019-06-06 05:44:08'),(220,4,1000,10,729,1234,'2019-06-06 06:06:32','2019-06-06 06:27:06','2019-06-06 06:27:06'),(221,3,1000,10,765,1268,'2019-06-06 06:06:32','2019-06-06 06:27:41','2019-06-06 06:27:41'),(222,7,1000,10,3264,3955,'2019-06-06 06:27:41','2019-06-06 07:33:37','2019-06-06 07:33:37'),(223,8,1000,10,4032,8291,'2019-06-06 06:27:06','2019-06-06 08:45:18','2019-06-06 08:45:18'),(224,1,1000,10,2633,5172,'2019-06-06 07:33:37','2019-06-06 08:59:49','2019-06-06 08:59:49'),(225,9,1000,10,483,902,'2019-06-06 09:59:54','2019-06-06 10:14:56','2019-06-06 10:14:56'),(226,10,1000,10,515,1132,'2019-06-06 10:14:56','2019-06-06 10:33:49','2019-06-06 10:33:49'),(227,2,1000,10,3858,7991,'2019-06-06 09:59:54','2019-06-06 12:13:05','2019-06-06 12:13:05'),(228,5,1000,5,887,2144,'2019-06-06 12:15:20','2019-06-06 12:51:05','2019-06-06 12:51:05'),(229,6,1000,5,849,2470,'2019-06-06 12:15:20','2019-06-06 12:56:31','2019-06-06 12:56:31'),(230,9,1000,10,483,866,'2019-06-07 16:33:09','2019-06-07 16:47:35','2019-06-07 16:47:35'),(231,10,1000,10,515,954,'2019-06-07 16:47:35','2019-06-07 17:03:30','2019-06-07 17:03:30'),(232,2,1000,10,3837,10758,'2019-06-07 16:33:09','2019-06-07 19:32:27','2019-06-07 19:32:27'),(233,5,1000,5,891,2101,'2019-06-07 19:34:43','2019-06-07 20:09:45','2019-06-07 20:09:45'),(234,6,1000,5,848,2380,'2019-06-07 19:34:43','2019-06-07 20:14:24','2019-06-07 20:14:24'),(235,9,1000,10,483,1050,'2019-06-09 03:09:01','2019-06-09 03:26:32','2019-06-09 03:26:32'),(236,9,1000,10,493,883,'2019-06-10 10:19:16','2019-06-10 10:34:00','2019-06-10 10:34:00'),(237,10,1000,10,528,1052,'2019-06-10 10:34:00','2019-06-10 10:51:32','2019-06-10 10:51:32'),(238,2,1000,10,3825,9327,'2019-06-10 10:19:16','2019-06-10 12:54:44','2019-06-10 12:54:44'),(239,5,1000,5,864,2117,'2019-06-10 12:56:58','2019-06-10 13:32:16','2019-06-10 13:32:16'),(240,6,1000,5,848,2654,'2019-06-10 12:56:58','2019-06-10 13:41:13','2019-06-10 13:41:13'),(241,9,1000,10,493,901,'2019-06-12 14:42:20','2019-06-12 14:57:21','2019-06-12 14:57:21'),(242,10,1000,10,528,944,'2019-06-12 14:57:21','2019-06-12 15:13:06','2019-06-12 15:13:06'),(243,2,1000,10,3842,11279,'2019-06-12 14:42:20','2019-06-12 17:50:19','2019-06-12 17:50:19'),(244,5,1000,5,854,2428,'2019-06-12 17:52:35','2019-06-12 18:33:04','2019-06-12 18:33:04'),(245,6,1000,5,862,2625,'2019-06-12 17:52:35','2019-06-12 18:36:20','2019-06-12 18:36:20'),(246,3,1000,10,767,1321,'2019-06-13 05:14:12','2019-06-13 05:36:13','2019-06-13 05:36:13'),(247,4,1000,10,731,1452,'2019-06-13 05:14:12','2019-06-13 05:38:24','2019-06-13 05:38:24'),(248,7,1000,10,3253,3975,'2019-06-13 05:38:24','2019-06-13 06:44:40','2019-06-13 06:44:40'),(249,8,1000,10,4017,8301,'2019-06-13 05:36:13','2019-06-13 07:54:34','2019-06-13 07:54:34'),(250,1,1000,10,2674,6950,'2019-06-13 06:44:40','2019-06-13 08:40:30','2019-06-13 08:40:30'),(251,9,1000,10,493,915,'2019-06-13 08:40:30','2019-06-13 08:55:46','2019-06-13 08:55:46'),(252,10,1000,10,528,1237,'2019-06-13 08:55:46','2019-06-13 09:16:24','2019-06-13 09:16:24'),(253,2,1000,10,3818,8520,'2019-06-13 07:59:18','2019-06-13 10:21:19','2019-06-13 10:21:19'),(254,4,1000,10,731,1184,'2019-06-13 14:50:44','2019-06-13 15:10:28','2019-06-13 15:10:28'),(255,3,1000,10,768,1197,'2019-06-13 14:50:44','2019-06-13 15:10:42','2019-06-13 15:10:42'),(256,7,1000,10,3253,4747,'2019-06-13 15:10:42','2019-06-13 16:29:49','2019-06-13 16:29:49'),(257,8,1000,10,4017,6196,'2019-06-13 15:10:29','2019-06-13 16:53:45','2019-06-13 16:53:45'),(258,4,1000,10,520,2265,'2019-06-17 02:59:48','2019-06-17 03:37:34','2019-06-17 03:37:34'),(259,3,1000,10,395,2333,'2019-06-17 02:59:48','2019-06-17 03:38:41','2019-06-17 03:38:41'),(260,7,1000,10,3253,4526,'2019-06-17 03:38:41','2019-06-17 04:54:07','2019-06-17 04:54:07'),(261,8,1000,10,4017,6416,'2019-06-17 03:37:34','2019-06-17 05:24:30','2019-06-17 05:24:30'),(262,1,1000,10,2690,6970,'2019-06-17 04:54:07','2019-06-17 06:50:18','2019-06-17 06:50:18'),(263,9,1000,10,300,765,'2019-06-17 06:50:18','2019-06-17 07:03:03','2019-06-17 07:03:03'),(264,10,1000,10,477,947,'2019-06-17 07:03:03','2019-06-17 07:18:51','2019-06-17 07:18:51'),(265,2,1000,10,3817,9239,'2019-06-17 05:29:28','2019-06-17 08:03:27','2019-06-17 08:03:27'),(266,6,1000,5,821,2576,'2019-06-17 08:06:06','2019-06-17 08:49:02','2019-06-17 08:49:02'),(267,5,1000,5,838,2992,'2019-06-17 08:06:06','2019-06-17 08:55:58','2019-06-17 08:55:58'),(268,4,1000,10,673,1184,'2019-06-20 13:38:52','2019-06-20 13:58:37','2019-06-20 13:58:37'),(269,3,1000,10,854,1250,'2019-06-20 13:38:52','2019-06-20 13:59:42','2019-06-20 13:59:42'),(270,7,1000,10,4216,6558,'2019-06-20 13:59:42','2019-06-20 15:49:01','2019-06-20 15:49:01'),(271,8,1000,10,4067,8010,'2019-06-20 13:58:37','2019-06-20 16:12:07','2019-06-20 16:12:07'),(272,1,1000,10,2778,7865,'2019-06-20 15:49:01','2019-06-20 18:00:07','2019-06-20 18:00:07'),(273,9,1000,10,537,1164,'2019-06-20 18:00:07','2019-06-20 18:19:31','2019-06-20 18:19:31'),(274,10,1000,10,439,995,'2019-06-20 18:19:31','2019-06-20 18:36:07','2019-06-20 18:36:07'),(275,2,1000,10,3863,10949,'2019-06-20 16:17:35','2019-06-20 19:20:05','2019-06-20 19:20:05'),(276,5,1000,5,137,1163,'2019-06-20 19:22:55','2019-06-20 19:42:19','2019-06-20 19:42:19'),(277,6,1000,5,459,1649,'2019-06-20 19:22:55','2019-06-20 19:50:24','2019-06-20 19:50:24'),(278,4,1000,10,645,1239,'2019-06-24 17:56:33','2019-06-24 18:17:12','2019-06-24 18:17:12'),(279,3,1000,10,822,1330,'2019-06-24 17:56:33','2019-06-24 18:18:43','2019-06-24 18:18:43'),(280,7,1000,10,4216,6079,'2019-06-24 18:18:43','2019-06-24 20:00:02','2019-06-24 20:00:02'),(281,8,1000,10,4060,7241,'2019-06-24 18:17:12','2019-06-24 20:17:54','2019-06-24 20:17:54'),(282,1,1000,10,2775,9961,'2019-06-24 20:00:02','2019-06-24 22:46:04','2019-06-24 22:46:04'),(283,9,1000,10,506,1041,'2019-06-24 22:46:04','2019-06-24 23:03:25','2019-06-24 23:03:25'),(284,10,1000,10,407,916,'2019-06-24 23:03:25','2019-06-24 23:18:42','2019-06-24 23:18:42'),(285,2,1000,10,3846,11998,'2019-06-24 20:23:07','2019-06-24 23:43:06','2019-06-24 23:43:06'),(286,5,1000,5,884,2088,'2019-06-24 23:45:58','2019-06-25 00:20:47','2019-06-25 00:20:47'),(287,6,1000,5,777,2096,'2019-06-24 23:45:58','2019-06-25 00:20:54','2019-06-25 00:20:54'),(288,4,1000,10,641,1204,'2019-06-26 08:39:57','2019-06-26 09:00:01','2019-06-26 09:00:01'),(289,3,1000,10,815,1220,'2019-06-26 08:39:57','2019-06-26 09:00:18','2019-06-26 09:00:18'),(290,4,1000,10,641,1056,'2019-06-26 09:48:09','2019-06-26 10:05:46','2019-06-26 10:05:46'),(291,3,1000,10,814,1113,'2019-06-26 09:48:09','2019-06-26 10:06:43','2019-06-26 10:06:43'),(292,7,1000,10,4145,5609,'2019-06-26 10:06:43','2019-06-26 11:40:13','2019-06-26 11:40:13'),(293,8,1000,10,3955,6208,'2019-06-26 10:05:46','2019-06-26 11:49:14','2019-06-26 11:49:14'),(294,4,1000,10,641,1192,'2019-06-26 14:17:54','2019-06-26 14:37:46','2019-06-26 14:37:46'),(295,3,1000,10,814,1246,'2019-06-26 14:17:54','2019-06-26 14:38:40','2019-06-26 14:38:40'),(296,4,1000,10,641,1037,'2019-06-26 15:59:23','2019-06-26 16:16:41','2019-06-26 16:16:41'),(297,3,1000,10,814,1132,'2019-06-26 15:59:23','2019-06-26 16:18:16','2019-06-26 16:18:16'),(298,7,1000,10,4140,5176,'2019-06-26 16:18:16','2019-06-26 17:44:32','2019-06-26 17:44:32'),(299,8,1000,10,3958,5302,'2019-06-26 16:16:41','2019-06-26 17:45:04','2019-06-26 17:45:04'),(300,1,1000,10,2756,10037,'2019-06-26 17:44:32','2019-06-26 20:31:50','2019-06-26 20:31:50'),(301,9,1000,10,505,1019,'2019-06-26 20:31:50','2019-06-26 20:48:50','2019-06-26 20:48:50'),(302,10,1000,10,406,965,'2019-06-26 20:48:50','2019-06-26 21:04:55','2019-06-26 21:04:55'),(303,2,1000,10,3854,12966,'2019-06-26 17:50:00','2019-06-26 21:26:07','2019-06-26 21:26:07'),(304,5,1000,5,870,2442,'2019-06-26 21:29:14','2019-06-26 22:09:57','2019-06-26 22:09:57'),(305,6,1000,5,800,4456,'2019-06-26 21:29:14','2019-06-26 22:43:30','2019-06-26 22:43:30'),(306,4,1000,10,638,1195,'2019-06-28 03:31:46','2019-06-28 03:51:42','2019-06-28 03:51:42'),(307,3,1000,10,812,1215,'2019-06-28 03:31:46','2019-06-28 03:52:02','2019-06-28 03:52:02'),(308,7,1000,10,4151,5422,'2019-06-28 03:52:02','2019-06-28 05:22:25','2019-06-28 05:22:25'),(309,8,1000,10,3962,6892,'2019-06-28 03:51:42','2019-06-28 05:46:35','2019-06-28 05:46:35'),(310,1,1000,10,2747,6345,'2019-06-28 05:22:25','2019-06-28 07:08:11','2019-06-28 07:08:11'),(311,4,1000,10,650,1048,'2019-06-28 08:56:43','2019-06-28 09:14:11','2019-06-28 09:14:11'),(312,3,1000,10,812,1170,'2019-06-28 08:56:43','2019-06-28 09:16:13','2019-06-28 09:16:13'),(313,7,1000,10,4151,6129,'2019-06-28 09:16:13','2019-06-28 10:58:23','2019-06-28 10:58:23'),(314,8,1000,10,3962,6969,'2019-06-28 09:14:11','2019-06-28 11:10:21','2019-06-28 11:10:21'),(315,1,1000,10,2748,6677,'2019-06-28 10:58:23','2019-06-28 12:49:41','2019-06-28 12:49:41'),(316,9,1000,10,503,847,'2019-06-28 12:49:41','2019-06-28 13:03:49','2019-06-28 13:03:49'),(317,10,1000,10,404,918,'2019-06-28 13:03:49','2019-06-28 13:19:07','2019-06-28 13:19:07'),(318,2,1000,10,3533,8687,'2019-06-28 11:16:22','2019-06-28 13:41:10','2019-06-28 13:41:10'),(319,6,1000,5,764,2098,'2019-06-28 13:44:04','2019-06-28 14:19:03','2019-06-28 14:19:03'),(320,5,1000,5,867,2115,'2019-06-28 13:44:04','2019-06-28 14:19:20','2019-06-28 14:19:20'),(321,4,1000,10,650,1242,'2019-06-28 18:34:42','2019-06-28 18:55:25','2019-06-28 18:55:25'),(322,3,1000,10,812,1323,'2019-06-28 18:34:42','2019-06-28 18:56:46','2019-06-28 18:56:46'),(323,8,1000,10,3962,7168,'2019-06-28 18:55:25','2019-06-28 20:54:53','2019-06-28 20:54:53'),(324,7,1000,10,4151,7282,'2019-06-28 18:56:46','2019-06-28 20:58:09','2019-06-28 20:58:09'),(325,1,1000,10,2754,8290,'2019-06-28 20:54:53','2019-06-28 23:13:04','2019-06-28 23:13:04'),(326,9,1000,10,503,1013,'2019-06-28 23:13:04','2019-06-28 23:29:58','2019-06-28 23:29:58'),(327,2,1000,10,3815,9930,'2019-06-28 20:58:09','2019-06-28 23:43:40','2019-06-28 23:43:40'),(328,10,1000,10,404,898,'2019-06-28 23:29:58','2019-06-28 23:44:57','2019-06-28 23:44:57'),(329,6,1000,5,764,1977,'2019-06-28 23:44:57','2019-06-29 00:17:54','2019-06-29 00:17:54'),(330,5,1000,5,857,2145,'2019-06-28 23:44:57','2019-06-29 00:20:42','2019-06-29 00:20:42'),(331,4,1000,10,650,1114,'2019-06-29 08:13:09','2019-06-29 08:31:44','2019-06-29 08:31:44'),(332,3,1000,10,812,1120,'2019-06-29 08:13:09','2019-06-29 08:31:50','2019-06-29 08:31:50'),(333,7,1000,10,4151,5730,'2019-06-29 08:31:50','2019-06-29 10:07:21','2019-06-29 10:07:21'),(334,8,1000,10,3962,6291,'2019-06-29 08:31:44','2019-06-29 10:16:35','2019-06-29 10:16:35'),(335,1,1000,10,2752,7033,'2019-06-29 10:07:21','2019-06-29 12:04:35','2019-06-29 12:04:35'),(336,9,1000,10,503,824,'2019-06-29 12:04:35','2019-06-29 12:18:19','2019-06-29 12:18:19'),(337,10,1000,10,404,773,'2019-06-29 12:18:19','2019-06-29 12:31:12','2019-06-29 12:31:12'),(338,2,1000,10,3862,9823,'2019-06-29 10:16:35','2019-06-29 13:00:19','2019-06-29 13:00:19'),(339,6,1000,5,755,1809,'2019-06-29 13:00:19','2019-06-29 13:30:29','2019-06-29 13:30:29'),(340,5,1000,5,857,2348,'2019-06-29 13:00:19','2019-06-29 13:39:28','2019-06-29 13:39:28'),(341,3,1000,10,924,1430,'2019-07-08 17:52:13','2019-07-08 18:16:03','2019-07-08 18:16:03'),(342,7,1000,10,4101,5749,'2019-07-08 17:52:13','2019-07-08 19:28:02','2019-07-08 19:28:02'),(343,9,1000,10,571,1065,'2019-07-08 19:28:02','2019-07-08 19:45:48','2019-07-08 19:45:48'),(344,1,1000,10,3181,12895,'2019-07-08 18:16:03','2019-07-08 21:50:59','2019-07-08 21:50:59'),(345,3,1000,10,891,1290,'2019-07-09 10:23:13','2019-07-09 10:44:43','2019-07-09 10:44:43'),(346,7,1000,10,4060,5584,'2019-07-09 10:23:13','2019-07-09 11:56:18','2019-07-09 11:56:18'),(347,9,1000,10,548,947,'2019-07-09 11:56:18','2019-07-09 12:12:06','2019-07-09 12:12:06'),(348,1,1000,10,3179,10058,'2019-07-09 10:44:44','2019-07-09 13:32:22','2019-07-09 13:32:22'),(349,3,1000,10,1046,1523,'2019-07-12 04:27:45','2019-07-12 04:53:09','2019-07-12 04:53:09'),(350,3,1000,10,1043,1469,'2019-07-12 06:40:45','2019-07-12 07:05:14','2019-07-12 07:05:14'),(351,7,1000,10,4108,5961,'2019-07-12 06:40:45','2019-07-12 08:20:07','2019-07-12 08:20:07'),(352,9,1000,10,783,1329,'2019-07-12 08:20:07','2019-07-12 08:42:16','2019-07-12 08:42:16'),(353,1,1000,10,3091,7548,'2019-07-12 07:05:15','2019-07-12 09:11:03','2019-07-12 09:11:03'),(354,3,1000,10,816,1148,'2019-07-12 15:08:19','2019-07-12 15:27:28','2019-07-12 15:27:28'),(355,7,1000,10,4108,5884,'2019-07-12 15:08:19','2019-07-12 16:46:24','2019-07-12 16:46:24'),(356,9,1000,10,422,799,'2019-07-12 16:46:24','2019-07-12 16:59:43','2019-07-12 16:59:43'),(357,1,1000,10,3093,11390,'2019-07-12 15:27:28','2019-07-12 18:37:19','2019-07-12 18:37:19'),(358,3,1000,10,828,1274,'2019-07-17 10:32:09','2019-07-17 10:53:24','2019-07-17 10:53:24'),(359,7,1000,10,4052,5622,'2019-07-17 10:32:09','2019-07-17 12:05:51','2019-07-17 12:05:51'),(360,9,1000,10,427,887,'2019-07-17 12:05:51','2019-07-17 12:20:39','2019-07-17 12:20:39'),(361,1,1000,10,3144,18027,'2019-07-17 10:53:24','2019-07-17 15:53:51','2019-07-17 15:53:51'),(362,3,1000,10,828,1253,'2019-07-18 07:02:09','2019-07-18 07:23:02','2019-07-18 07:23:02'),(363,7,1000,10,4067,5499,'2019-07-18 07:02:09','2019-07-18 08:33:48','2019-07-18 08:33:48'),(364,9,1000,10,427,944,'2019-07-18 08:33:48','2019-07-18 08:49:33','2019-07-18 08:49:33'),(365,1,1000,10,3129,9876,'2019-07-18 07:23:02','2019-07-18 10:07:39','2019-07-18 10:07:39'),(366,5,1000,5,934,2138,'2019-07-18 10:07:39','2019-07-18 10:43:17','2019-07-18 10:43:17'),(367,3,1000,10,847,1341,'2019-07-23 10:05:09','2019-07-23 10:27:30','2019-07-23 10:27:30'),(368,7,1000,10,3265,4302,'2019-07-23 10:05:09','2019-07-23 11:16:52','2019-07-23 11:16:52'),(369,9,1000,10,452,907,'2019-07-23 11:16:52','2019-07-23 11:32:00','2019-07-23 11:32:00'),(370,1,1000,10,3113,6672,'2019-07-23 10:27:30','2019-07-23 12:18:43','2019-07-23 12:18:43'),(371,5,1000,5,982,2456,'2019-07-23 12:18:43','2019-07-23 12:59:39','2019-07-23 12:59:39'),(372,3,1000,10,824,1265,'2019-07-26 02:07:58','2019-07-26 02:29:04','2019-07-26 02:29:04'),(373,7,1000,10,3206,4170,'2019-07-26 02:07:58','2019-07-26 03:17:29','2019-07-26 03:17:29'),(374,9,1000,10,435,912,'2019-07-26 03:17:29','2019-07-26 03:32:42','2019-07-26 03:32:42'),(375,1,1000,10,3121,5830,'2019-07-26 02:29:04','2019-07-26 04:06:15','2019-07-26 04:06:15'),(376,5,1000,5,940,2130,'2019-07-26 04:06:15','2019-07-26 04:41:45','2019-07-26 04:41:45'),(377,3,1000,10,825,1270,'2019-07-28 03:39:01','2019-07-28 04:00:12','2019-07-28 04:00:12'),(378,7,1000,10,3206,4602,'2019-07-28 03:39:01','2019-07-28 04:55:44','2019-07-28 04:55:44'),(379,9,1000,10,435,1128,'2019-07-28 04:55:44','2019-07-28 05:14:32','2019-07-28 05:14:32'),(380,1,1000,10,3118,6238,'2019-07-28 04:00:12','2019-07-28 05:44:11','2019-07-28 05:44:11'),(381,5,1000,5,932,2151,'2019-07-28 05:44:11','2019-07-28 06:20:03','2019-07-28 06:20:03'),(382,3,1000,10,536,875,'2019-07-30 06:51:49','2019-07-30 07:06:24','2019-07-30 07:06:24'),(383,3,1000,10,536,892,'2019-07-30 13:47:55','2019-07-30 14:02:47','2019-07-30 14:02:47'),(384,7,1000,10,3382,4044,'2019-07-30 13:47:55','2019-07-30 14:55:19','2019-07-30 14:55:19'),(385,9,1000,10,350,744,'2019-07-30 14:55:19','2019-07-30 15:07:44','2019-07-30 15:07:44'),(386,1,1000,10,3082,14567,'2019-07-30 14:02:47','2019-07-30 18:05:34','2019-07-30 18:05:34'),(387,5,1000,5,976,2443,'2019-07-30 18:05:34','2019-07-30 18:46:18','2019-07-30 18:46:18'),(388,3,1000,10,536,889,'2019-07-31 14:16:02','2019-07-31 14:30:52','2019-07-31 14:30:52'),(389,3,1000,10,536,856,'2019-07-31 15:16:53','2019-07-31 15:31:10','2019-07-31 15:31:10'),(390,7,1000,10,3389,4252,'2019-07-31 15:16:53','2019-07-31 16:27:45','2019-07-31 16:27:45'),(391,9,1000,10,350,779,'2019-07-31 16:27:45','2019-07-31 16:40:44','2019-07-31 16:40:44'),(392,1,1000,10,3213,11848,'2019-07-31 15:31:10','2019-07-31 18:48:38','2019-07-31 18:48:38'),(393,5,1000,5,960,2518,'2019-07-31 18:48:38','2019-07-31 19:30:37','2019-07-31 19:30:37'),(394,3,1000,10,538,861,'2019-08-02 05:26:21','2019-08-02 05:40:43','2019-08-02 05:40:43'),(395,7,1000,10,3268,4507,'2019-08-02 05:26:21','2019-08-02 06:41:28','2019-08-02 06:41:28'),(396,9,1000,10,351,818,'2019-08-02 06:41:28','2019-08-02 06:55:07','2019-08-02 06:55:07'),(397,1,1000,10,3224,6213,'2019-08-02 05:40:43','2019-08-02 07:24:16','2019-08-02 07:24:16'),(398,5,1000,5,967,2367,'2019-08-02 07:24:16','2019-08-02 08:03:43','2019-08-02 08:03:43'),(399,3,1000,10,538,1147,'2019-08-02 10:42:51','2019-08-02 11:01:59','2019-08-02 11:01:59'),(400,7,1000,10,3268,6057,'2019-08-02 10:42:51','2019-08-02 12:23:48','2019-08-02 12:23:48'),(401,9,1000,10,351,1000,'2019-08-02 12:23:48','2019-08-02 12:40:29','2019-08-02 12:40:29'),(402,1,1000,10,3218,19225,'2019-08-02 11:01:59','2019-08-02 16:22:24','2019-08-02 16:22:24'),(403,5,1000,5,968,2569,'2019-08-02 16:22:25','2019-08-02 17:05:14','2019-08-02 17:05:14'),(404,3,1000,10,538,1026,'2019-08-03 18:02:49','2019-08-03 18:19:56','2019-08-03 18:19:56'),(405,7,1000,10,3268,5486,'2019-08-03 18:02:49','2019-08-03 19:34:15','2019-08-03 19:34:15'),(406,9,1000,10,351,809,'2019-08-03 19:34:16','2019-08-03 19:47:45','2019-08-03 19:47:45'),(407,1,1000,10,3195,15644,'2019-08-03 18:19:56','2019-08-03 22:40:40','2019-08-03 22:40:40'),(408,5,1000,5,962,2457,'2019-08-03 22:40:40','2019-08-03 23:21:38','2019-08-03 23:21:38'),(409,3,1000,10,538,1044,'2019-08-04 18:02:49','2019-08-04 18:20:14','2019-08-04 18:20:14'),(410,7,1000,10,3268,4600,'2019-08-04 18:02:49','2019-08-04 19:19:29','2019-08-04 19:19:29'),(411,9,1000,10,351,953,'2019-08-04 19:19:29','2019-08-04 19:35:23','2019-08-04 19:35:23'),(412,1,1000,10,3176,16126,'2019-08-04 18:20:14','2019-08-04 22:49:01','2019-08-04 22:49:01'),(413,5,1000,5,966,2522,'2019-08-04 22:49:01','2019-08-04 23:31:04','2019-08-04 23:31:04'),(414,3,1000,10,570,914,'2019-08-05 17:02:51','2019-08-05 17:18:06','2019-08-05 17:18:06'),(415,7,1000,10,3243,4283,'2019-08-05 17:02:51','2019-08-05 18:14:15','2019-08-05 18:14:15'),(416,9,1000,10,363,959,'2019-08-05 18:14:15','2019-08-05 18:30:15','2019-08-05 18:30:15'),(417,1,1000,10,3108,10371,'2019-08-05 17:18:06','2019-08-05 20:10:57','2019-08-05 20:10:57'),(418,5,1000,5,985,2539,'2019-08-05 20:10:57','2019-08-05 20:53:17','2019-08-05 20:53:17'),(419,3,1000,10,570,954,'2019-08-06 17:02:50','2019-08-06 17:18:44','2019-08-06 17:18:44'),(420,7,1000,10,3353,4957,'2019-08-06 17:02:50','2019-08-06 18:25:27','2019-08-06 18:25:27'),(421,9,1000,10,362,909,'2019-08-06 18:25:27','2019-08-06 18:40:37','2019-08-06 18:40:37'),(422,1,1000,10,3144,12134,'2019-08-06 17:18:44','2019-08-06 20:40:58','2019-08-06 20:40:58'),(423,5,1000,5,989,2664,'2019-08-06 20:40:58','2019-08-06 21:25:23','2019-08-06 21:25:23'),(424,3,1000,10,576,914,'2019-08-07 17:02:49','2019-08-07 17:18:03','2019-08-07 17:18:03'),(425,7,1000,10,3308,4668,'2019-08-07 17:02:49','2019-08-07 18:20:37','2019-08-07 18:20:37'),(426,9,1000,10,363,891,'2019-08-07 18:20:37','2019-08-07 18:35:29','2019-08-07 18:35:29'),(427,1,1000,10,3131,7758,'2019-08-07 17:18:03','2019-08-07 19:27:21','2019-08-07 19:27:21'),(428,5,1000,5,984,2859,'2019-08-07 19:27:21','2019-08-07 20:15:00','2019-08-07 20:15:00'),(429,3,1000,10,576,934,'2019-08-08 17:02:46','2019-08-08 17:18:21','2019-08-08 17:18:21'),(430,7,1000,10,3330,4609,'2019-08-08 17:02:46','2019-08-08 18:19:36','2019-08-08 18:19:36'),(431,9,1000,10,362,942,'2019-08-08 18:19:36','2019-08-08 18:35:19','2019-08-08 18:35:19'),(432,1,1000,10,3124,12171,'2019-08-08 17:18:21','2019-08-08 20:41:13','2019-08-08 20:41:13'),(433,5,1000,5,968,2681,'2019-08-08 20:41:13','2019-08-08 21:25:55','2019-08-08 21:25:55'),(434,3,1000,10,576,1006,'2019-08-09 17:02:49','2019-08-09 17:19:36','2019-08-09 17:19:36'),(435,7,1000,10,3330,5123,'2019-08-09 17:02:49','2019-08-09 18:28:13','2019-08-09 18:28:13'),(436,3,1000,10,607,978,'2019-08-15 13:54:48','2019-08-15 14:11:07','2019-08-15 14:11:07'),(437,7,1000,10,3454,4736,'2019-08-15 13:54:48','2019-08-15 15:13:45','2019-08-15 15:13:45'),(438,9,1000,10,375,893,'2019-08-15 15:13:45','2019-08-15 15:28:38','2019-08-15 15:28:38'),(439,1,1000,10,3094,8417,'2019-08-15 14:11:07','2019-08-15 16:31:25','2019-08-15 16:31:25'),(440,3,1000,10,607,949,'2019-08-15 17:03:19','2019-08-15 17:19:09','2019-08-15 17:19:09'),(441,7,1000,10,3454,4819,'2019-08-15 17:03:19','2019-08-15 18:23:39','2019-08-15 18:23:39'),(442,9,1000,10,375,889,'2019-08-15 18:23:39','2019-08-15 18:38:29','2019-08-15 18:38:29'),(443,1,1000,10,3096,8746,'2019-08-15 17:19:09','2019-08-15 19:44:55','2019-08-15 19:44:55'),(444,5,1000,5,943,2352,'2019-08-15 19:44:55','2019-08-15 20:24:07','2019-08-15 20:24:07'),(445,3,1000,10,607,934,'2019-08-16 02:22:47','2019-08-16 02:38:21','2019-08-16 02:38:21'),(446,3,1000,10,607,927,'2019-08-16 06:22:50','2019-08-16 06:38:18','2019-08-16 06:38:18'),(447,7,1000,10,3346,4598,'2019-08-16 06:22:50','2019-08-16 07:39:28','2019-08-16 07:39:28'),(448,9,1000,10,375,921,'2019-08-16 07:39:28','2019-08-16 07:54:50','2019-08-16 07:54:50'),(449,1,1000,10,3100,10488,'2019-08-16 06:38:18','2019-08-16 09:33:06','2019-08-16 09:33:06'),(450,3,1000,10,607,1154,'2019-08-17 19:08:46','2019-08-17 19:28:01','2019-08-17 19:28:01'),(451,3,1000,10,607,916,'2019-08-17 20:00:37','2019-08-17 20:15:54','2019-08-17 20:15:54'),(452,7,1000,10,3346,4545,'2019-08-17 20:00:37','2019-08-17 21:16:23','2019-08-17 21:16:23'),(453,9,1000,10,375,936,'2019-08-17 21:16:23','2019-08-17 21:32:00','2019-08-17 21:32:00'),(454,1,1000,10,3097,9071,'2019-08-17 20:15:54','2019-08-17 22:47:06','2019-08-17 22:47:06'),(455,5,1000,5,955,2510,'2019-08-17 22:47:06','2019-08-17 23:28:56','2019-08-17 23:28:56'),(456,3,1000,10,607,1014,'2019-08-18 19:02:53','2019-08-18 19:19:47','2019-08-18 19:19:47'),(457,7,1000,10,3346,4546,'2019-08-18 19:02:53','2019-08-18 20:18:39','2019-08-18 20:18:39'),(458,9,1000,10,375,898,'2019-08-18 20:18:39','2019-08-18 20:33:38','2019-08-18 20:33:38'),(459,1,1000,10,3095,9865,'2019-08-18 19:19:47','2019-08-18 22:04:13','2019-08-18 22:04:13'),(460,5,1000,5,973,2504,'2019-08-18 22:04:13','2019-08-18 22:45:57','2019-08-18 22:45:57'),(461,3,1000,10,619,1042,'2019-08-19 19:02:52','2019-08-19 19:20:15','2019-08-19 19:20:15'),(462,7,1000,10,3346,4877,'2019-08-19 19:02:52','2019-08-19 20:24:10','2019-08-19 20:24:10'),(463,9,1000,10,386,863,'2019-08-19 20:24:10','2019-08-19 20:38:33','2019-08-19 20:38:33'),(464,1,1000,10,3086,9832,'2019-08-19 19:20:15','2019-08-19 22:04:07','2019-08-19 22:04:07'),(465,5,1000,5,1013,2555,'2019-08-19 22:04:07','2019-08-19 22:46:42','2019-08-19 22:46:42'),(466,3,1000,10,619,1045,'2019-08-20 19:02:53','2019-08-20 19:20:19','2019-08-20 19:20:19'),(467,7,1000,10,3449,4298,'2019-08-20 19:02:53','2019-08-20 20:14:32','2019-08-20 20:14:32'),(468,9,1000,10,386,821,'2019-08-20 20:14:32','2019-08-20 20:28:13','2019-08-20 20:28:13'),(469,1,1000,10,3134,9480,'2019-08-20 19:20:19','2019-08-20 21:58:20','2019-08-20 21:58:20'),(470,5,1000,5,987,2495,'2019-08-20 21:58:20','2019-08-20 22:39:55','2019-08-20 22:39:55'),(471,3,1000,10,608,1030,'2019-08-21 19:02:50','2019-08-21 19:20:01','2019-08-21 19:20:01'),(472,7,1000,10,3456,4503,'2019-08-21 19:02:50','2019-08-21 20:17:54','2019-08-21 20:17:54'),(473,9,1000,10,371,822,'2019-08-21 20:17:54','2019-08-21 20:31:36','2019-08-21 20:31:36'),(474,1,1000,10,2798,7915,'2019-08-21 19:20:01','2019-08-21 21:31:57','2019-08-21 21:31:57'),(475,5,1000,5,979,2529,'2019-08-21 21:31:57','2019-08-21 22:14:07','2019-08-21 22:14:07'),(476,3,1000,10,608,981,'2019-08-22 19:02:51','2019-08-22 19:19:13','2019-08-22 19:19:13'),(477,3,1000,10,608,1150,'2019-08-23 02:41:11','2019-08-23 03:00:22','2019-08-23 03:00:22'),(478,7,1000,10,3466,4400,'2019-08-23 02:41:11','2019-08-23 03:54:32','2019-08-23 03:54:32'),(479,9,1000,10,371,791,'2019-08-23 03:54:32','2019-08-23 04:07:43','2019-08-23 04:07:43'),(480,1,1000,10,2841,8571,'2019-08-23 03:00:22','2019-08-23 05:23:14','2019-08-23 05:23:14'),(481,5,1000,5,985,2427,'2019-08-23 05:23:14','2019-08-23 06:03:41','2019-08-23 06:03:41'),(482,3,1000,10,608,976,'2019-08-23 06:29:40','2019-08-23 06:45:56','2019-08-23 06:45:56'),(483,7,1000,10,3466,4127,'2019-08-23 06:29:40','2019-08-23 07:38:27','2019-08-23 07:38:27'),(484,9,1000,10,371,785,'2019-08-23 07:38:27','2019-08-23 07:51:32','2019-08-23 07:51:32'),(485,1,1000,10,2842,9626,'2019-08-23 06:45:56','2019-08-23 09:26:22','2019-08-23 09:26:22'),(486,5,1000,5,992,2354,'2019-08-23 09:26:23','2019-08-23 10:05:37','2019-08-23 10:05:37'),(487,3,1000,10,608,958,'2019-08-23 19:02:53','2019-08-23 19:18:52','2019-08-23 19:18:52'),(488,7,1000,10,3466,4931,'2019-08-23 19:02:53','2019-08-23 20:25:05','2019-08-23 20:25:05'),(489,9,1000,10,371,862,'2019-08-23 20:25:05','2019-08-23 20:39:28','2019-08-23 20:39:28'),(490,1,1000,10,2853,9845,'2019-08-23 19:18:52','2019-08-23 22:02:58','2019-08-23 22:02:58'),(491,5,1000,5,992,2560,'2019-08-23 22:02:58','2019-08-23 22:45:38','2019-08-23 22:45:38'),(492,3,1000,10,608,999,'2019-08-24 19:02:53','2019-08-24 19:19:33','2019-08-24 19:19:33'),(493,7,1000,10,3466,4567,'2019-08-24 19:02:53','2019-08-24 20:19:00','2019-08-24 20:19:00'),(494,9,1000,10,371,919,'2019-08-24 20:19:00','2019-08-24 20:34:20','2019-08-24 20:34:20'),(495,1,1000,10,2844,10946,'2019-08-24 19:19:33','2019-08-24 22:21:59','2019-08-24 22:21:59'),(496,5,1000,5,986,2468,'2019-08-24 22:21:59','2019-08-24 23:03:08','2019-08-24 23:03:08'),(497,3,1000,10,608,1142,'2019-08-25 19:02:55','2019-08-25 19:21:57','2019-08-25 19:21:57'),(498,7,1000,10,3466,4594,'2019-08-25 19:02:55','2019-08-25 20:19:29','2019-08-25 20:19:29'),(499,9,1000,10,371,853,'2019-08-25 20:19:29','2019-08-25 20:33:42','2019-08-25 20:33:42'),(500,1,1000,10,2834,9992,'2019-08-25 19:21:57','2019-08-25 22:08:30','2019-08-25 22:08:30'),(501,5,1000,5,993,2513,'2019-08-25 22:08:30','2019-08-25 22:50:23','2019-08-25 22:50:23'),(502,3,1000,10,620,1023,'2019-08-26 19:02:51','2019-08-26 19:19:55','2019-08-26 19:19:55'),(503,7,1000,10,3466,4842,'2019-08-26 19:02:51','2019-08-26 20:23:34','2019-08-26 20:23:34'),(504,9,1000,10,381,884,'2019-08-26 20:23:34','2019-08-26 20:38:18','2019-08-26 20:38:18'),(505,1,1000,10,2842,8428,'2019-08-26 19:19:55','2019-08-26 21:40:23','2019-08-26 21:40:23'),(506,5,1000,5,1000,2470,'2019-08-26 21:40:23','2019-08-26 22:21:34','2019-08-26 22:21:34'),(507,3,1000,12,628,1002,'2019-08-27 19:03:26','2019-08-27 19:20:08','2019-08-27 19:20:08'),(508,7,1000,12,3587,4306,'2019-08-27 19:03:26','2019-08-27 20:15:12','2019-08-27 20:15:12'),(509,9,1000,12,381,959,'2019-08-27 20:15:12','2019-08-27 20:31:12','2019-08-27 20:31:12'),(510,1,1000,12,2854,8316,'2019-08-27 19:20:08','2019-08-27 21:38:45','2019-08-27 21:38:45'),(511,5,1000,5,1002,2516,'2019-08-27 21:38:45','2019-08-27 22:20:42','2019-08-27 22:20:42'),(512,3,1000,10,628,1099,'2019-08-28 03:33:14','2019-08-28 03:51:33','2019-08-28 03:51:33'),(513,7,1000,10,3495,4449,'2019-08-28 03:33:14','2019-08-28 04:47:24','2019-08-28 04:47:24'),(514,9,1000,10,381,873,'2019-08-28 04:47:24','2019-08-28 05:01:57','2019-08-28 05:01:57'),(515,1,1000,10,2849,5703,'2019-08-28 03:51:33','2019-08-28 05:26:37','2019-08-28 05:26:37'),(516,5,1000,5,1000,2469,'2019-08-28 05:26:37','2019-08-28 06:07:46','2019-08-28 06:07:46'),(517,3,1000,10,628,1257,'2019-08-28 09:58:44','2019-08-28 10:19:41','2019-08-28 10:19:41'),(518,7,1000,10,3495,5617,'2019-08-28 09:58:44','2019-08-28 11:32:21','2019-08-28 11:32:21'),(519,9,1000,10,381,920,'2019-08-28 11:32:21','2019-08-28 11:47:42','2019-08-28 11:47:42'),(520,1,1000,10,2851,9676,'2019-08-28 10:19:41','2019-08-28 13:00:57','2019-08-28 13:00:57'),(521,5,1000,5,1007,2723,'2019-08-28 13:00:58','2019-08-28 13:46:21','2019-08-28 13:46:21'),(522,3,1000,10,627,1125,'2019-08-28 19:03:04','2019-08-28 19:21:50','2019-08-28 19:21:50'),(523,7,1000,10,3495,4754,'2019-08-28 19:03:04','2019-08-28 20:22:19','2019-08-28 20:22:19'),(524,9,1000,10,381,845,'2019-08-28 20:22:19','2019-08-28 20:36:25','2019-08-28 20:36:25'),(525,1,1000,10,2852,9735,'2019-08-28 19:21:50','2019-08-28 22:04:06','2019-08-28 22:04:06'),(526,5,1000,5,978,2529,'2019-08-28 22:04:06','2019-08-28 22:46:16','2019-08-28 22:46:16'),(527,3,1000,10,627,1125,'2019-08-29 19:03:03','2019-08-29 19:21:49','2019-08-29 19:21:49'),(528,7,1000,10,3541,4594,'2019-08-29 19:03:03','2019-08-29 20:19:37','2019-08-29 20:19:37'),(529,9,1000,10,381,871,'2019-08-29 20:19:37','2019-08-29 20:34:09','2019-08-29 20:34:09'),(530,1,1000,10,2855,8870,'2019-08-29 19:21:49','2019-08-29 21:49:39','2019-08-29 21:49:39'),(531,5,1000,5,979,2428,'2019-08-29 21:49:39','2019-08-29 22:30:07','2019-08-29 22:30:07'),(532,3,1000,10,627,1073,'2019-08-30 19:03:05','2019-08-30 19:20:58','2019-08-30 19:20:58'),(533,7,1000,10,3522,5223,'2019-08-30 19:03:05','2019-08-30 20:30:09','2019-08-30 20:30:09'),(534,9,1000,10,383,1074,'2019-08-30 20:30:09','2019-08-30 20:48:04','2019-08-30 20:48:04'),(535,1,1000,10,2906,9341,'2019-08-30 19:20:58','2019-08-30 21:56:40','2019-08-30 21:56:40'),(536,5,1000,5,982,2523,'2019-08-30 21:56:40','2019-08-30 22:38:43','2019-08-30 22:38:43'),(537,3,1000,10,627,1143,'2019-09-01 07:07:45','2019-09-01 07:26:48','2019-09-01 07:26:48'),(538,7,1000,10,3522,4470,'2019-09-01 07:07:45','2019-09-01 08:22:15','2019-09-01 08:22:15'),(539,9,1000,10,383,893,'2019-09-01 08:22:15','2019-09-01 08:37:09','2019-09-01 08:37:09'),(540,1,1000,10,3103,8023,'2019-09-01 07:26:48','2019-09-01 09:40:31','2019-09-01 09:40:31'),(541,4,1000,10,539,938,'2019-09-01 10:03:05','2019-09-01 10:18:44','2019-09-01 10:18:44'),(542,4,1000,10,542,867,'2019-09-01 10:20:47','2019-09-01 10:35:14','2019-09-01 10:35:14'),(543,3,1000,10,627,989,'2019-09-01 10:20:47','2019-09-01 10:37:16','2019-09-01 10:37:16'),(544,4,1000,10,542,1040,'2019-09-01 19:03:06','2019-09-01 19:20:27','2019-09-01 19:20:27'),(545,3,1000,10,627,1054,'2019-09-01 19:03:06','2019-09-01 19:20:41','2019-09-01 19:20:41'),(546,7,1000,10,3513,4962,'2019-09-01 19:20:41','2019-09-01 20:43:24','2019-09-01 20:43:24'),(547,8,1000,10,4365,6326,'2019-09-01 19:20:27','2019-09-01 21:05:54','2019-09-01 21:05:54'),(548,1,1000,10,3107,8613,'2019-09-01 20:43:24','2019-09-01 23:06:57','2019-09-01 23:06:57'),(549,9,1000,10,383,1001,'2019-09-01 23:06:57','2019-09-01 23:23:39','2019-09-01 23:23:39'),(550,10,1000,10,339,921,'2019-09-01 23:23:39','2019-09-01 23:39:01','2019-09-01 23:39:01'),(551,2,1000,10,3831,10841,'2019-09-01 21:05:54','2019-09-02 00:06:35','2019-09-02 00:06:35'),(552,6,1000,5,892,2311,'2019-09-02 00:06:35','2019-09-02 00:45:07','2019-09-02 00:45:07'),(553,5,1000,5,987,2664,'2019-09-02 00:06:35','2019-09-02 00:51:00','2019-09-02 00:51:00'),(554,4,1000,10,552,1070,'2019-09-02 16:35:08','2019-09-02 16:52:58','2019-09-02 16:52:58'),(555,3,1000,10,640,1089,'2019-09-02 16:35:08','2019-09-02 16:53:18','2019-09-02 16:53:18'),(556,7,1000,10,3486,4769,'2019-09-02 16:53:18','2019-09-02 18:12:47','2019-09-02 18:12:47'),(557,8,1000,10,4353,6710,'2019-09-02 16:52:58','2019-09-02 18:44:49','2019-09-02 18:44:49'),(558,4,1000,10,552,1061,'2019-09-03 02:23:11','2019-09-03 02:40:52','2019-09-03 02:40:52'),(559,3,1000,10,640,1068,'2019-09-03 02:23:11','2019-09-03 02:40:59','2019-09-03 02:40:59'),(560,4,1000,10,552,870,'2019-09-03 14:13:08','2019-09-03 14:27:38','2019-09-03 14:27:38'),(561,3,1000,10,640,996,'2019-09-03 14:13:08','2019-09-03 14:29:44','2019-09-03 14:29:44'),(562,7,1000,10,3566,5356,'2019-09-03 14:29:44','2019-09-03 15:59:01','2019-09-03 15:59:01'),(563,8,1000,10,4433,6769,'2019-09-03 14:27:38','2019-09-03 16:20:27','2019-09-03 16:20:27'),(564,1,1000,10,3107,7743,'2019-09-03 15:59:01','2019-09-03 18:08:04','2019-09-03 18:08:04'),(565,9,1000,10,399,881,'2019-09-03 18:08:05','2019-09-03 18:22:46','2019-09-03 18:22:46'),(566,10,1000,10,335,851,'2019-09-03 18:22:46','2019-09-03 18:36:58','2019-09-03 18:36:58'),(567,2,1000,10,3760,10914,'2019-09-03 16:20:27','2019-09-03 19:22:22','2019-09-03 19:22:22'),(568,6,1000,5,920,2447,'2019-09-03 19:22:22','2019-09-03 20:03:09','2019-09-03 20:03:09'),(569,5,1000,5,967,2773,'2019-09-03 19:22:22','2019-09-03 20:08:36','2019-09-03 20:08:36'),(570,4,1000,10,552,970,'2019-09-04 16:03:06','2019-09-04 16:19:16','2019-09-04 16:19:16'),(571,3,1000,10,640,1076,'2019-09-04 16:03:06','2019-09-04 16:21:03','2019-09-04 16:21:03'),(572,8,1000,10,4830,6707,'2019-09-04 16:19:16','2019-09-04 18:11:04','2019-09-04 18:11:04'),(573,7,1000,10,4885,6955,'2019-09-04 16:21:03','2019-09-04 18:16:58','2019-09-04 18:16:58'),(574,1,1000,10,2987,8798,'2019-09-04 18:11:04','2019-09-04 20:37:42','2019-09-04 20:37:42'),(575,9,1000,10,399,818,'2019-09-04 20:37:42','2019-09-04 20:51:20','2019-09-04 20:51:20'),(576,10,1000,10,335,764,'2019-09-04 20:51:20','2019-09-04 21:04:05','2019-09-04 21:04:05'),(577,2,1000,10,3774,12724,'2019-09-04 18:16:58','2019-09-04 21:49:03','2019-09-04 21:49:03'),(578,5,1000,5,899,2246,'2019-09-04 21:49:03','2019-09-04 22:26:29','2019-09-04 22:26:29'),(579,6,1000,5,928,2332,'2019-09-04 21:49:03','2019-09-04 22:27:55','2019-09-04 22:27:55'),(580,3,1000,10,640,1085,'2019-09-05 16:03:09','2019-09-05 16:21:14','2019-09-05 16:21:14'),(581,4,1000,10,552,1109,'2019-09-05 16:03:09','2019-09-05 16:21:39','2019-09-05 16:21:39'),(582,3,1000,10,640,1186,'2019-09-05 18:27:07','2019-09-05 18:46:54','2019-09-05 18:46:54'),(583,4,1000,10,552,1259,'2019-09-05 18:27:07','2019-09-05 18:48:07','2019-09-05 18:48:07'),(584,7,1000,10,4820,6470,'2019-09-05 18:48:07','2019-09-05 20:35:58','2019-09-05 20:35:58'),(585,8,1000,10,4777,7184,'2019-09-05 18:46:54','2019-09-05 20:46:39','2019-09-05 20:46:39'),(586,1,1000,10,2992,8324,'2019-09-05 20:35:58','2019-09-05 22:54:42','2019-09-05 22:54:42'),(587,9,1000,10,400,1033,'2019-09-05 22:54:42','2019-09-05 23:11:56','2019-09-05 23:11:56'),(588,10,1000,10,336,912,'2019-09-05 23:11:56','2019-09-05 23:27:08','2019-09-05 23:27:08'),(589,2,1000,10,3766,10669,'2019-09-05 20:46:39','2019-09-05 23:44:28','2019-09-05 23:44:28'),(590,6,1000,5,911,2156,'2019-09-05 23:44:28','2019-09-06 00:20:24','2019-09-06 00:20:24'),(591,5,1000,5,951,2453,'2019-09-05 23:44:28','2019-09-06 00:25:22','2019-09-06 00:25:22'),(592,3,1000,10,640,1072,'2019-09-06 17:03:08','2019-09-06 17:21:00','2019-09-06 17:21:00'),(593,4,1000,10,552,1086,'2019-09-06 17:03:08','2019-09-06 17:21:14','2019-09-06 17:21:14'),(594,7,1000,10,4745,6744,'2019-09-06 17:21:14','2019-09-06 19:13:38','2019-09-06 19:13:38'),(595,8,1000,10,4734,7304,'2019-09-06 17:21:00','2019-09-06 19:22:44','2019-09-06 19:22:44'),(596,1,1000,10,2978,8713,'2019-09-06 19:13:38','2019-09-06 21:38:52','2019-09-06 21:38:52'),(597,9,1000,10,400,983,'2019-09-06 21:38:52','2019-09-06 21:55:15','2019-09-06 21:55:15'),(598,10,1000,10,336,1045,'2019-09-06 21:55:15','2019-09-06 22:12:40','2019-09-06 22:12:40'),(599,2,1000,10,3761,10326,'2019-09-06 19:22:44','2019-09-06 22:14:51','2019-09-06 22:14:51'),(600,6,1000,5,938,2416,'2019-09-06 22:14:51','2019-09-06 22:55:07','2019-09-06 22:55:07'),(601,5,1000,5,974,2661,'2019-09-06 22:14:51','2019-09-06 22:59:13','2019-09-06 22:59:13'),(602,3,1000,10,640,1082,'2019-09-07 05:43:06','2019-09-07 06:01:09','2019-09-07 06:01:09'),(603,4,1000,10,552,1092,'2019-09-07 05:43:06','2019-09-07 06:01:18','2019-09-07 06:01:18'),(604,7,1000,10,3012,3746,'2019-09-07 06:01:18','2019-09-07 07:03:45','2019-09-07 07:03:45'),(605,8,1000,10,4081,5995,'2019-09-07 06:01:09','2019-09-07 07:41:05','2019-09-07 07:41:05'),(606,1,1000,10,2971,5666,'2019-09-07 07:03:45','2019-09-07 08:38:11','2019-09-07 08:38:11'),(607,9,1000,10,400,983,'2019-09-07 08:38:11','2019-09-07 08:54:34','2019-09-07 08:54:34'),(608,10,1000,10,336,813,'2019-09-07 08:54:34','2019-09-07 09:08:08','2019-09-07 09:08:08'),(609,2,1000,10,3756,7247,'2019-09-07 07:41:05','2019-09-07 09:41:53','2019-09-07 09:41:53'),(610,6,1000,5,943,2328,'2019-09-07 09:41:53','2019-09-07 10:20:41','2019-09-07 10:20:41'),(611,5,1000,5,959,2401,'2019-09-07 09:41:53','2019-09-07 10:21:55','2019-09-07 10:21:55'),(612,3,1000,10,640,1115,'2019-09-08 05:43:07','2019-09-08 06:01:43','2019-09-08 06:01:43'),(613,4,1000,10,552,1125,'2019-09-08 05:43:07','2019-09-08 06:01:52','2019-09-08 06:01:52'),(614,4,1000,10,552,1036,'2019-09-08 06:57:04','2019-09-08 07:14:21','2019-09-08 07:14:21'),(615,3,1000,10,640,1051,'2019-09-08 06:57:04','2019-09-08 07:14:36','2019-09-08 07:14:36'),(616,7,1000,10,4657,6221,'2019-09-08 07:14:36','2019-09-08 08:58:17','2019-09-08 08:58:17'),(617,8,1000,10,4656,6754,'2019-09-08 07:14:21','2019-09-08 09:06:56','2019-09-08 09:06:56'),(618,1,1000,10,2922,12057,'2019-09-08 08:58:17','2019-09-08 12:19:15','2019-09-08 12:19:15'),(619,9,1000,10,400,939,'2019-09-08 12:19:15','2019-09-08 12:34:55','2019-09-08 12:34:55'),(620,10,1000,10,336,872,'2019-09-08 12:34:55','2019-09-08 12:49:28','2019-09-08 12:49:28'),(621,2,1000,10,3746,16751,'2019-09-08 09:06:56','2019-09-08 13:46:07','2019-09-08 13:46:07'),(622,6,1000,5,933,2481,'2019-09-08 13:46:07','2019-09-08 14:27:29','2019-09-08 14:27:29'),(623,5,1000,5,953,2612,'2019-09-08 13:46:07','2019-09-08 14:29:40','2019-09-08 14:29:40'),(624,3,1000,10,649,1141,'2019-09-09 16:03:09','2019-09-09 16:22:10','2019-09-09 16:22:10'),(625,4,1000,10,561,1215,'2019-09-09 16:03:09','2019-09-09 16:23:24','2019-09-09 16:23:24'),(626,7,1000,10,4657,6194,'2019-09-09 16:23:24','2019-09-09 18:06:39','2019-09-09 18:06:39'),(627,8,1000,10,4656,6669,'2019-09-09 16:22:10','2019-09-09 18:13:20','2019-09-09 18:13:20'),(628,1,1000,10,2876,17186,'2019-09-09 18:06:39','2019-09-09 22:53:05','2019-09-09 22:53:05'),(629,9,1000,10,404,1139,'2019-09-09 22:53:05','2019-09-09 23:12:05','2019-09-09 23:12:05'),(630,10,1000,10,334,1180,'2019-09-09 23:12:05','2019-09-09 23:31:45','2019-09-09 23:31:45'),(631,2,1000,10,3362,21367,'2019-09-09 18:13:20','2019-09-10 00:09:27','2019-09-10 00:09:27'),(632,6,1000,5,936,2320,'2019-09-10 00:09:27','2019-09-10 00:48:07','2019-09-10 00:48:07'),(633,5,1000,5,1021,2464,'2019-09-10 00:09:27','2019-09-10 00:50:32','2019-09-10 00:50:32'),(634,3,1000,10,652,1119,'2019-09-11 16:03:08','2019-09-11 16:21:47','2019-09-11 16:21:47'),(635,4,1000,10,564,1128,'2019-09-11 16:03:08','2019-09-11 16:21:56','2019-09-11 16:21:56'),(636,8,1000,10,4691,6419,'2019-09-11 16:21:47','2019-09-11 18:08:47','2019-09-11 18:08:47'),(637,7,1000,10,4723,6456,'2019-09-11 16:21:56','2019-09-11 18:09:33','2019-09-11 18:09:33'),(638,1,1000,10,2886,14066,'2019-09-11 18:08:47','2019-09-11 22:03:13','2019-09-11 22:03:13'),(639,2,1000,10,3285,14314,'2019-09-11 18:09:33','2019-09-11 22:08:07','2019-09-11 22:08:07'),(640,9,1000,10,407,1091,'2019-09-11 22:03:13','2019-09-11 22:21:25','2019-09-11 22:21:25'),(641,10,1000,10,336,968,'2019-09-11 22:08:07','2019-09-11 22:24:15','2019-09-11 22:24:15'),(642,6,1000,5,938,2403,'2019-09-11 22:24:16','2019-09-11 23:04:19','2019-09-11 23:04:19'),(643,5,1000,5,1032,2667,'2019-09-11 22:24:16','2019-09-11 23:08:43','2019-09-11 23:08:43'),(644,3,1000,10,652,1124,'2019-09-12 16:03:09','2019-09-12 16:21:54','2019-09-12 16:21:54'),(645,4,1000,10,564,1135,'2019-09-12 16:03:09','2019-09-12 16:22:05','2019-09-12 16:22:05'),(646,7,1000,10,3729,4808,'2019-09-12 16:22:05','2019-09-12 17:42:13','2019-09-12 17:42:13'),(647,8,1000,10,4405,5448,'2019-09-12 16:21:54','2019-09-12 17:52:43','2019-09-12 17:52:43'),(648,1,1000,10,2799,14207,'2019-09-12 17:42:13','2019-09-12 21:39:01','2019-09-12 21:39:01'),(649,9,1000,10,406,929,'2019-09-12 21:39:01','2019-09-12 21:54:31','2019-09-12 21:54:31'),(650,10,1000,10,349,1016,'2019-09-12 21:54:31','2019-09-12 22:11:27','2019-09-12 22:11:27'),(651,2,1000,10,3286,17528,'2019-09-12 17:52:43','2019-09-12 22:44:51','2019-09-12 22:44:51'),(652,6,1000,5,952,2334,'2019-09-12 22:44:51','2019-09-12 23:23:46','2019-09-12 23:23:46'),(653,5,1000,5,1006,2512,'2019-09-12 22:44:51','2019-09-12 23:26:44','2019-09-12 23:26:44'),(654,3,1000,10,652,1187,'2019-09-13 16:03:11','2019-09-13 16:22:59','2019-09-13 16:22:59'),(655,4,1000,10,564,1199,'2019-09-13 16:03:11','2019-09-13 16:23:11','2019-09-13 16:23:11'),(656,7,1000,10,3607,5120,'2019-09-13 16:23:11','2019-09-13 17:48:32','2019-09-13 17:48:32'),(657,8,1000,10,4303,5755,'2019-09-13 16:22:59','2019-09-13 17:58:54','2019-09-13 17:58:54'),(658,1,1000,10,2791,16610,'2019-09-13 17:48:32','2019-09-13 22:25:22','2019-09-13 22:25:22'),(659,9,1000,10,406,1177,'2019-09-13 22:25:22','2019-09-13 22:44:59','2019-09-13 22:44:59'),(660,10,1000,10,337,1140,'2019-09-13 22:44:59','2019-09-13 23:04:00','2019-09-13 23:04:00'),(661,2,1000,10,3274,20563,'2019-09-13 17:58:54','2019-09-13 23:41:37','2019-09-13 23:41:37'),(662,6,1000,5,933,2383,'2019-09-13 23:41:37','2019-09-14 00:21:21','2019-09-14 00:21:21'),(663,5,1000,5,1014,2697,'2019-09-13 23:41:37','2019-09-14 00:26:34','2019-09-14 00:26:34'),(664,3,1000,10,652,1359,'2019-09-14 16:03:10','2019-09-14 16:25:50','2019-09-14 16:25:50'),(665,4,1000,10,564,1428,'2019-09-14 16:03:10','2019-09-14 16:26:59','2019-09-14 16:26:59'),(666,7,1000,10,3607,7627,'2019-09-14 16:26:59','2019-09-14 18:34:07','2019-09-14 18:34:07'),(667,8,1000,10,4303,8142,'2019-09-14 16:25:50','2019-09-14 18:41:32','2019-09-14 18:41:32'),(668,1,1000,10,2797,16088,'2019-09-14 18:34:07','2019-09-14 23:02:15','2019-09-14 23:02:15'),(669,9,1000,10,405,1177,'2019-09-14 23:02:15','2019-09-14 23:21:53','2019-09-14 23:21:53'),(670,10,1000,10,348,1021,'2019-09-14 23:21:53','2019-09-14 23:38:55','2019-09-14 23:38:55'),(671,2,1000,10,3311,19861,'2019-09-14 18:41:32','2019-09-15 00:12:34','2019-09-15 00:12:34'),(672,6,1000,5,927,2296,'2019-09-15 00:12:34','2019-09-15 00:50:50','2019-09-15 00:50:50'),(673,5,1000,5,1024,2532,'2019-09-15 00:12:34','2019-09-15 00:54:46','2019-09-15 00:54:46'),(674,3,1000,10,652,1143,'2019-09-15 16:03:10','2019-09-15 16:22:13','2019-09-15 16:22:13'),(675,4,1000,10,564,1157,'2019-09-15 16:03:10','2019-09-15 16:22:27','2019-09-15 16:22:27'),(676,7,1000,10,3607,5778,'2019-09-15 16:22:27','2019-09-15 17:58:45','2019-09-15 17:58:45'),(677,8,1000,10,4303,6099,'2019-09-15 16:22:13','2019-09-15 18:03:52','2019-09-15 18:03:52'),(678,1,1000,10,2789,16409,'2019-09-15 17:58:45','2019-09-15 22:32:15','2019-09-15 22:32:15'),(679,9,1000,10,405,1223,'2019-09-15 22:32:15','2019-09-15 22:52:39','2019-09-15 22:52:39'),(680,10,1000,10,348,1093,'2019-09-15 22:52:39','2019-09-15 23:10:52','2019-09-15 23:10:52'),(681,2,1000,10,3282,19976,'2019-09-15 18:03:52','2019-09-15 23:36:49','2019-09-15 23:36:49'),(682,6,1000,5,920,2315,'2019-09-15 23:36:49','2019-09-16 00:15:24','2019-09-16 00:15:24'),(683,5,1000,5,1032,2556,'2019-09-15 23:36:49','2019-09-16 00:19:26','2019-09-16 00:19:26'),(684,3,1000,10,656,1151,'2019-09-16 16:49:13','2019-09-16 17:08:24','2019-09-16 17:08:24'),(685,4,1000,10,568,1211,'2019-09-16 16:49:13','2019-09-16 17:09:24','2019-09-16 17:09:24'),(686,7,1000,10,3602,5847,'2019-09-16 17:09:24','2019-09-16 18:46:52','2019-09-16 18:46:52'),(687,8,1000,10,4302,6810,'2019-09-16 17:08:24','2019-09-16 19:01:55','2019-09-16 19:01:55'),(688,1,1000,10,2808,16092,'2019-09-16 18:46:52','2019-09-16 23:15:05','2019-09-16 23:15:05'),(689,9,1000,10,412,1122,'2019-09-16 23:15:05','2019-09-16 23:33:48','2019-09-16 23:33:48'),(690,10,1000,10,361,1327,'2019-09-16 23:33:48','2019-09-16 23:55:55','2019-09-16 23:55:55'),(691,2,1000,10,3276,20314,'2019-09-16 19:01:55','2019-09-17 00:40:29','2019-09-17 00:40:29'),(692,6,1000,5,939,2413,'2019-09-17 00:40:29','2019-09-17 01:20:42','2019-09-17 01:20:42'),(693,5,1000,5,1047,2635,'2019-09-17 00:40:29','2019-09-17 01:24:25','2019-09-17 01:24:25'),(694,3,1000,10,656,1190,'2019-09-17 16:49:10','2019-09-17 17:09:00','2019-09-17 17:09:00'),(695,4,1000,10,568,1221,'2019-09-17 16:49:10','2019-09-17 17:09:31','2019-09-17 17:09:31'),(696,7,1000,10,3639,6125,'2019-09-17 17:09:31','2019-09-17 18:51:37','2019-09-17 18:51:37'),(697,8,1000,10,4337,7092,'2019-09-17 17:09:00','2019-09-17 19:07:13','2019-09-17 19:07:13'),(698,1,1000,10,0,1321,'2019-09-17 18:51:37','2019-09-17 19:13:39','2019-09-17 19:13:39'),(699,2,1000,10,0,1280,'2019-09-17 19:07:13','2019-09-17 19:28:34','2019-09-17 19:28:34'),(700,9,1000,10,411,965,'2019-09-17 19:13:39','2019-09-17 19:29:44','2019-09-17 19:29:44'),(701,10,1000,10,357,1014,'2019-09-17 19:28:34','2019-09-17 19:45:29','2019-09-17 19:45:29'),(702,6,1000,5,939,2496,'2019-09-17 19:45:29','2019-09-17 20:27:05','2019-09-17 20:27:05'),(703,5,1000,5,1036,2864,'2019-09-17 19:45:29','2019-09-17 20:33:13','2019-09-17 20:33:13'),(704,3,1000,10,656,1276,'2019-09-18 16:49:10','2019-09-18 17:10:26','2019-09-18 17:10:26'),(705,4,1000,10,568,1314,'2019-09-18 16:49:10','2019-09-18 17:11:05','2019-09-18 17:11:05'),(706,7,1000,10,3699,6199,'2019-09-18 17:11:05','2019-09-18 18:54:24','2019-09-18 18:54:24'),(707,8,1000,10,4384,7245,'2019-09-18 17:10:26','2019-09-18 19:11:11','2019-09-18 19:11:11'),(708,1,1000,10,0,1289,'2019-09-18 18:54:24','2019-09-18 19:15:54','2019-09-18 19:15:54'),(709,9,1000,10,409,913,'2019-09-18 19:15:54','2019-09-18 19:31:08','2019-09-18 19:31:08'),(710,2,1000,10,0,1285,'2019-09-18 19:11:12','2019-09-18 19:32:37','2019-09-18 19:32:37'),(711,10,1000,10,356,967,'2019-09-18 19:31:08','2019-09-18 19:47:15','2019-09-18 19:47:15'),(712,5,1000,5,999,2660,'2019-09-18 19:47:15','2019-09-18 20:31:35','2019-09-18 20:31:35'),(713,6,1000,5,941,2673,'2019-09-18 19:47:15','2019-09-18 20:31:49','2019-09-18 20:31:49'),(714,3,1000,10,656,1245,'2019-09-19 16:49:12','2019-09-19 17:09:58','2019-09-19 17:09:58'),(715,4,1000,10,568,1301,'2019-09-19 16:49:12','2019-09-19 17:10:53','2019-09-19 17:10:53'),(716,7,1000,10,3706,6505,'2019-09-19 17:10:53','2019-09-19 18:59:19','2019-09-19 18:59:19'),(717,8,1000,10,4391,7459,'2019-09-19 17:09:58','2019-09-19 19:14:17','2019-09-19 19:14:17'),(718,1,1000,10,0,1307,'2019-09-19 18:59:19','2019-09-19 19:21:06','2019-09-19 19:21:06'),(719,2,1000,10,0,1280,'2019-09-19 19:14:17','2019-09-19 19:35:38','2019-09-19 19:35:38'),(720,9,1000,10,409,899,'2019-09-19 19:21:06','2019-09-19 19:36:05','2019-09-19 19:36:05'),(721,10,1000,10,356,832,'2019-09-19 19:35:38','2019-09-19 19:49:30','2019-09-19 19:49:30'),(722,6,1000,5,939,2528,'2019-09-19 19:49:30','2019-09-19 20:31:39','2019-09-19 20:31:39'),(723,5,1000,5,996,2598,'2019-09-19 19:49:30','2019-09-19 20:32:48','2019-09-19 20:32:48'),(724,3,1000,10,653,1323,'2019-09-20 16:49:12','2019-09-20 17:11:15','2019-09-20 17:11:15'),(725,4,1000,10,567,1351,'2019-09-20 16:49:12','2019-09-20 17:11:43','2019-09-20 17:11:43'),(726,7,1000,10,3511,6910,'2019-09-20 17:11:43','2019-09-20 19:06:53','2019-09-20 19:06:53'),(727,8,1000,10,4139,7778,'2019-09-20 17:11:15','2019-09-20 19:20:53','2019-09-20 19:20:53'),(728,1,1000,10,0,1271,'2019-09-20 19:06:53','2019-09-20 19:28:05','2019-09-20 19:28:05'),(729,2,1000,10,0,1324,'2019-09-20 19:20:53','2019-09-20 19:42:58','2019-09-20 19:42:58'),(730,9,1000,10,409,944,'2019-09-20 19:28:05','2019-09-20 19:43:50','2019-09-20 19:43:50'),(731,10,1000,10,356,843,'2019-09-20 19:42:58','2019-09-20 19:57:01','2019-09-20 19:57:01'),(732,6,1000,5,936,2408,'2019-09-20 19:57:01','2019-09-20 20:37:09','2019-09-20 20:37:09'),(733,5,1000,5,1015,2709,'2019-09-20 19:57:01','2019-09-20 20:42:10','2019-09-20 20:42:10'),(734,3,1000,10,653,1264,'2019-09-21 16:49:19','2019-09-21 17:10:24','2019-09-21 17:10:24'),(735,4,1000,10,567,1284,'2019-09-21 16:49:19','2019-09-21 17:10:44','2019-09-21 17:10:44'),(736,7,1000,10,3511,6293,'2019-09-21 17:10:44','2019-09-21 18:55:38','2019-09-21 18:55:38'),(737,8,1000,10,4139,7261,'2019-09-21 17:10:24','2019-09-21 19:11:26','2019-09-21 19:11:26'),(738,1,1000,10,0,1327,'2019-09-21 18:55:38','2019-09-21 19:17:45','2019-09-21 19:17:45'),(739,9,1000,10,409,914,'2019-09-21 19:17:45','2019-09-21 19:32:59','2019-09-21 19:32:59'),(740,2,1000,10,0,1294,'2019-09-21 19:11:26','2019-09-21 19:33:00','2019-09-21 19:33:00'),(741,10,1000,10,356,915,'2019-09-21 19:32:59','2019-09-21 19:48:15','2019-09-21 19:48:15'),(742,6,1000,5,927,2313,'2019-09-21 19:48:15','2019-09-21 20:26:49','2019-09-21 20:26:49'),(743,5,1000,5,1009,2693,'2019-09-21 19:48:15','2019-09-21 20:33:09','2019-09-21 20:33:09'),(744,3,1000,10,653,1260,'2019-09-22 16:49:11','2019-09-22 17:10:12','2019-09-22 17:10:12'),(745,4,1000,10,567,1284,'2019-09-22 16:49:11','2019-09-22 17:10:35','2019-09-22 17:10:35'),(746,7,1000,10,3511,6407,'2019-09-22 17:10:35','2019-09-22 18:57:23','2019-09-22 18:57:23'),(747,8,1000,10,4139,6984,'2019-09-22 17:10:12','2019-09-22 19:06:36','2019-09-22 19:06:36'),(748,1,1000,10,0,1355,'2019-09-22 18:57:23','2019-09-22 19:19:59','2019-09-22 19:19:59'),(749,2,1000,10,0,1335,'2019-09-22 19:06:36','2019-09-22 19:28:52','2019-09-22 19:28:52'),(750,9,1000,10,409,979,'2019-09-22 19:19:59','2019-09-22 19:36:18','2019-09-22 19:36:18'),(751,10,1000,10,356,908,'2019-09-22 19:28:52','2019-09-22 19:44:00','2019-09-22 19:44:00'),(752,6,1000,5,923,2468,'2019-09-22 19:44:00','2019-09-22 20:25:08','2019-09-22 20:25:08'),(753,5,1000,5,1001,2908,'2019-09-22 19:44:00','2019-09-22 20:32:28','2019-09-22 20:32:28'),(754,3,1000,10,672,1310,'2019-09-23 16:49:12','2019-09-23 17:11:02','2019-09-23 17:11:02'),(755,4,1000,10,586,1351,'2019-09-23 16:49:12','2019-09-23 17:11:44','2019-09-23 17:11:44'),(756,7,1000,10,3511,6930,'2019-09-23 17:11:44','2019-09-23 19:07:15','2019-09-23 19:07:15'),(757,8,1000,10,4139,7470,'2019-09-23 17:11:02','2019-09-23 19:15:33','2019-09-23 19:15:33'),(758,1,1000,10,0,1348,'2019-09-23 19:07:15','2019-09-23 19:29:44','2019-09-23 19:29:44'),(759,2,1000,10,0,1355,'2019-09-23 19:15:33','2019-09-23 19:38:08','2019-09-23 19:38:08'),(760,9,1000,10,412,934,'2019-09-23 19:29:44','2019-09-23 19:45:18','2019-09-23 19:45:18'),(761,10,1000,10,359,956,'2019-09-23 19:38:08','2019-09-23 19:54:04','2019-09-23 19:54:04'),(762,6,1000,5,931,2506,'2019-09-23 19:54:04','2019-09-23 20:35:51','2019-09-23 20:35:51'),(763,5,1000,5,996,2862,'2019-09-23 19:54:04','2019-09-23 20:41:47','2019-09-23 20:41:47'),(764,3,1000,10,745,1407,'2019-09-24 16:13:35','2019-09-24 16:37:03','2019-09-24 16:37:03'),(765,4,1000,10,656,1496,'2019-09-24 16:13:35','2019-09-24 16:38:32','2019-09-24 16:38:32'),(766,7,1000,10,3543,6544,'2019-09-24 16:38:32','2019-09-24 18:27:37','2019-09-24 18:27:37'),(767,8,1000,10,4171,7485,'2019-09-24 16:37:03','2019-09-24 18:41:49','2019-09-24 18:41:49'),(768,2,1000,10,0,70,'2019-09-24 18:41:49','2019-09-24 18:42:59','2019-09-24 18:42:59'),(769,9,1000,10,412,1182,'2019-09-24 18:42:59','2019-09-24 19:02:42','2019-09-24 19:02:42'),(770,10,1000,10,363,1137,'2019-09-24 19:02:42','2019-09-24 19:21:39','2019-09-24 19:21:39'),(771,1,1000,10,2649,14153,'2019-09-24 18:27:37','2019-09-24 22:23:30','2019-09-24 22:23:30'),(772,6,1000,5,938,2437,'2019-09-24 22:23:30','2019-09-24 23:04:08','2019-09-24 23:04:08'),(773,5,1000,5,997,2603,'2019-09-24 22:23:30','2019-09-24 23:06:54','2019-09-24 23:06:54'),(774,3,1000,10,746,1447,'2019-09-25 16:13:12','2019-09-25 16:37:20','2019-09-25 16:37:20'),(775,4,1000,10,657,1536,'2019-09-25 16:13:12','2019-09-25 16:38:48','2019-09-25 16:38:48'),(776,7,1000,10,3458,7534,'2019-09-25 16:38:48','2019-09-25 18:44:23','2019-09-25 18:44:23'),(777,1,1000,10,0,70,'2019-09-25 18:44:23','2019-09-25 18:45:34','2019-09-25 18:45:34'),(778,8,1000,10,4095,8432,'2019-09-25 16:37:20','2019-09-25 18:57:52','2019-09-25 18:57:52'),(779,2,1000,10,0,1313,'2019-09-25 18:45:34','2019-09-25 19:07:27','2019-09-25 19:07:27'),(780,9,1000,10,413,1009,'2019-09-25 18:57:52','2019-09-25 19:14:42','2019-09-25 19:14:42'),(781,10,1000,10,350,1080,'2019-09-25 19:07:27','2019-09-25 19:25:28','2019-09-25 19:25:28'),(782,6,1000,5,953,2471,'2019-09-25 19:25:28','2019-09-25 20:06:39','2019-09-25 20:06:39'),(783,5,1000,5,993,2536,'2019-09-25 19:25:28','2019-09-25 20:07:45','2019-09-25 20:07:45'),(784,2,1000,10,0,1258,'2019-09-26 07:41:34','2019-09-26 08:02:32','2019-09-26 08:02:32'),(785,1,1000,10,0,80,'2019-09-26 08:29:12','2019-09-26 08:30:33','2019-09-26 08:30:33'),(786,2,1000,10,0,80,'2019-09-26 08:29:12','2019-09-26 08:30:33','2019-09-26 08:30:33'),(787,2,1000,10,0,1813,'2019-09-26 08:58:50','2019-09-26 09:29:03','2019-09-26 09:29:03'),(788,1,1000,10,2658,11830,'2019-09-26 08:58:50','2019-09-26 12:16:01','2019-09-26 12:16:01'),(789,3,1000,10,673,1384,'2019-09-26 16:03:13','2019-09-26 16:26:17','2019-09-26 16:26:17'),(790,4,1000,10,587,1431,'2019-09-26 16:03:13','2019-09-26 16:27:05','2019-09-26 16:27:05'),(791,7,1000,10,3485,6224,'2019-09-26 16:27:05','2019-09-26 18:10:49','2019-09-26 18:10:49'),(792,8,1000,10,4122,7292,'2019-09-26 16:26:17','2019-09-26 18:27:49','2019-09-26 18:27:49'),(793,2,1000,10,0,70,'2019-09-26 18:27:49','2019-09-26 18:29:00','2019-09-26 18:29:00'),(794,9,1000,10,412,1205,'2019-09-26 18:29:00','2019-09-26 18:49:05','2019-09-26 18:49:05'),(795,10,1000,10,362,1291,'2019-09-26 18:49:05','2019-09-26 19:10:37','2019-09-26 19:10:37'),(796,1,1000,10,2649,15901,'2019-09-26 18:10:49','2019-09-26 22:35:51','2019-09-26 22:35:51'),(797,5,1000,5,987,2777,'2019-09-26 22:35:51','2019-09-26 23:22:08','2019-09-26 23:22:08'),(798,6,1000,5,955,2793,'2019-09-26 22:35:51','2019-09-26 23:22:24','2019-09-26 23:22:24'),(799,3,1000,10,673,1381,'2019-09-27 16:03:14','2019-09-27 16:26:16','2019-09-27 16:26:16'),(800,4,1000,10,590,1503,'2019-09-27 16:03:14','2019-09-27 16:28:17','2019-09-27 16:28:17'),(801,7,1000,10,3587,7121,'2019-09-27 16:28:17','2019-09-27 18:26:58','2019-09-27 18:26:58'),(802,8,1000,10,4227,7720,'2019-09-27 16:26:16','2019-09-27 18:34:57','2019-09-27 18:34:57'),(803,1,1000,10,2671,14164,'2019-09-27 18:26:59','2019-09-27 22:23:03','2019-09-27 22:23:03'),(804,9,1000,10,411,1159,'2019-09-27 22:23:03','2019-09-27 22:42:23','2019-09-27 22:42:23'),(805,10,1000,10,375,1047,'2019-09-27 22:42:23','2019-09-27 22:59:50','2019-09-27 22:59:50'),(806,2,1000,10,3113,17663,'2019-09-27 18:34:57','2019-09-27 23:29:20','2019-09-27 23:29:20'),(807,6,1000,5,943,2593,'2019-09-27 23:29:20','2019-09-28 00:12:33','2019-09-28 00:12:33'),(808,5,1000,5,1015,2723,'2019-09-27 23:29:20','2019-09-28 00:14:43','2019-09-28 00:14:43'),(809,3,1000,10,673,1312,'2019-09-28 16:03:14','2019-09-28 16:25:06','2019-09-28 16:25:06'),(810,4,1000,10,590,1452,'2019-09-28 16:03:14','2019-09-28 16:27:27','2019-09-28 16:27:27'),(811,7,1000,10,3587,6281,'2019-09-28 16:27:27','2019-09-28 18:12:08','2019-09-28 18:12:08'),(812,8,1000,10,4227,6758,'2019-09-28 16:25:06','2019-09-28 18:17:45','2019-09-28 18:17:45'),(813,2,1000,10,0,70,'2019-09-28 18:17:45','2019-09-28 18:18:55','2019-09-28 18:18:55'),(814,9,1000,10,411,1041,'2019-09-28 18:18:55','2019-09-28 18:36:17','2019-09-28 18:36:17'),(815,10,1000,10,0,223,'2019-09-28 18:36:17','2019-09-28 18:40:01','2019-09-28 18:40:01'),(816,1,1000,10,2660,14382,'2019-09-28 18:12:08','2019-09-28 22:11:50','2019-09-28 22:11:50'),(817,6,1000,5,945,2509,'2019-09-28 22:11:50','2019-09-28 22:53:40','2019-09-28 22:53:40'),(818,5,1000,5,1014,2683,'2019-09-28 22:11:50','2019-09-28 22:56:34','2019-09-28 22:56:34'),(819,3,1000,10,673,1328,'2019-09-29 16:03:14','2019-09-29 16:25:22','2019-09-29 16:25:22'),(820,4,1000,10,590,1391,'2019-09-29 16:03:14','2019-09-29 16:26:25','2019-09-29 16:26:25'),(821,7,1000,10,3581,6354,'2019-09-29 16:26:25','2019-09-29 18:12:20','2019-09-29 18:12:20'),(822,1,1000,10,0,70,'2019-09-29 18:12:20','2019-09-29 18:13:30','2019-09-29 18:13:30'),(823,8,1000,10,4220,7232,'2019-09-29 16:25:22','2019-09-29 18:25:55','2019-09-29 18:25:55'),(824,2,1000,10,0,1060,'2019-09-29 18:13:30','2019-09-29 18:31:11','2019-09-29 18:31:11'),(825,10,1000,10,0,343,'2019-09-29 18:31:11','2019-09-29 18:36:55','2019-09-29 18:36:55'),(826,9,1000,10,412,1161,'2019-09-29 18:25:55','2019-09-29 18:45:17','2019-09-29 18:45:17'),(827,6,1000,5,927,2342,'2019-09-29 18:45:17','2019-09-29 19:24:19','2019-09-29 19:24:19'),(828,5,1000,5,1009,2582,'2019-09-29 18:45:17','2019-09-29 19:28:19','2019-09-29 19:28:19'),(829,3,1000,10,0,50,'2019-09-30 16:03:13','2019-09-30 16:04:03','2019-09-30 16:04:03'),(830,4,1000,10,0,50,'2019-09-30 16:03:13','2019-09-30 16:04:03','2019-09-30 16:04:03'),(831,7,1000,10,3581,6784,'2019-09-30 16:04:03','2019-09-30 17:57:08','2019-09-30 17:57:08'),(832,8,1000,10,4220,7615,'2019-09-30 16:04:03','2019-09-30 18:10:59','2019-09-30 18:10:59'),(833,2,1000,10,0,70,'2019-09-30 18:10:59','2019-09-30 18:12:09','2019-09-30 18:12:09'),(834,9,1000,10,0,45,'2019-09-30 18:12:09','2019-09-30 18:12:55','2019-09-30 18:12:55'),(835,1,1000,10,0,981,'2019-09-30 17:57:08','2019-09-30 18:13:29','2019-09-30 18:13:29'),(836,10,1000,10,0,44,'2019-09-30 18:12:55','2019-09-30 18:13:39','2019-09-30 18:13:39'),(837,6,1000,5,936,2618,'2019-09-30 18:13:39','2019-09-30 18:57:18','2019-09-30 18:57:18'),(838,5,1000,5,1028,2845,'2019-09-30 18:13:39','2019-09-30 19:01:04','2019-09-30 19:01:04'),(839,3,1000,10,0,53,'2019-10-01 16:03:18','2019-10-01 16:04:11','2019-10-01 16:04:11'),(840,4,1000,10,0,53,'2019-10-01 16:03:18','2019-10-01 16:04:12','2019-10-01 16:04:12'),(841,7,1000,10,3623,6391,'2019-10-01 16:04:12','2019-10-01 17:50:43','2019-10-01 17:50:43'),(842,1,1000,10,0,70,'2019-10-01 17:50:43','2019-10-01 17:51:54','2019-10-01 17:51:54'),(843,2,1000,10,0,70,'2019-10-01 17:51:54','2019-10-01 17:53:04','2019-10-01 17:53:04'),(844,9,1000,10,0,45,'2019-10-01 17:53:04','2019-10-01 17:53:50','2019-10-01 17:53:50'),(845,10,1000,10,0,45,'2019-10-01 17:53:50','2019-10-01 17:54:36','2019-10-01 17:54:36'),(846,8,1000,10,4276,6948,'2019-10-01 16:04:11','2019-10-01 18:00:00','2019-10-01 18:00:00'),(847,5,1000,5,0,427,'2019-10-01 18:00:00','2019-10-01 18:07:07','2019-10-01 18:07:07'),(848,6,1000,5,0,442,'2019-10-01 18:00:00','2019-10-01 18:07:23','2019-10-01 18:07:23'),(849,3,1000,10,0,56,'2019-10-02 16:03:14','2019-10-02 16:04:10','2019-10-02 16:04:10'),(850,4,1000,10,0,56,'2019-10-02 16:03:14','2019-10-02 16:04:11','2019-10-02 16:04:11'),(851,7,1000,10,3528,9760,'2019-10-02 16:04:11','2019-10-02 18:46:51','2019-10-02 18:46:51'),(852,8,1000,10,4188,10463,'2019-10-02 16:04:11','2019-10-02 18:58:34','2019-10-02 18:58:34'),(853,1,1000,10,0,1187,'2019-10-02 18:46:51','2019-10-02 19:06:39','2019-10-02 19:06:39'),(854,9,1000,10,0,44,'2019-10-02 19:06:39','2019-10-02 19:07:24','2019-10-02 19:07:24'),(855,10,1000,10,0,45,'2019-10-02 19:07:24','2019-10-02 19:08:09','2019-10-02 19:08:09'),(856,2,1000,10,0,1151,'2019-10-02 18:58:34','2019-10-02 19:17:46','2019-10-02 19:17:46'),(857,5,1000,5,0,419,'2019-10-02 19:17:46','2019-10-02 19:24:46','2019-10-02 19:24:46'),(858,6,1000,5,0,429,'2019-10-02 19:17:46','2019-10-02 19:24:55','2019-10-02 19:24:55'),(859,3,1000,10,0,52,'2019-10-03 16:03:18','2019-10-03 16:04:10','2019-10-03 16:04:10'),(860,4,1000,10,0,53,'2019-10-03 16:03:18','2019-10-03 16:04:11','2019-10-03 16:04:11'),(861,7,1000,10,0,237,'2019-10-03 16:04:11','2019-10-03 16:08:09','2019-10-03 16:08:09'),(862,8,1000,10,0,272,'2019-10-03 16:04:10','2019-10-03 16:08:42','2019-10-03 16:08:42'),(863,1,1000,10,0,879,'2019-10-03 16:08:09','2019-10-03 16:22:49','2019-10-03 16:22:49'),(864,9,1000,10,0,48,'2019-10-03 16:22:49','2019-10-03 16:23:37','2019-10-03 16:23:37'),(865,10,1000,10,0,47,'2019-10-03 16:23:37','2019-10-03 16:24:25','2019-10-03 16:24:25'),(866,2,1000,10,0,1031,'2019-10-03 16:08:42','2019-10-03 16:25:53','2019-10-03 16:25:53'),(867,5,1000,5,0,442,'2019-10-03 16:25:53','2019-10-03 16:33:16','2019-10-03 16:33:16'),(868,6,1000,5,0,454,'2019-10-03 16:25:53','2019-10-03 16:33:28','2019-10-03 16:33:28'),(869,3,1000,10,0,57,'2019-10-04 16:03:16','2019-10-04 16:04:13','2019-10-04 16:04:13'),(870,4,1000,10,0,57,'2019-10-04 16:03:16','2019-10-04 16:04:13','2019-10-04 16:04:13'),(871,7,1000,10,0,249,'2019-10-04 16:04:13','2019-10-04 16:08:23','2019-10-04 16:08:23'),(872,8,1000,10,0,277,'2019-10-04 16:04:13','2019-10-04 16:08:51','2019-10-04 16:08:51'),(873,1,1000,10,0,70,'2019-10-04 16:08:23','2019-10-04 16:09:33','2019-10-04 16:09:33'),(874,2,1000,10,0,70,'2019-10-04 16:08:51','2019-10-04 16:10:01','2019-10-04 16:10:01'),(875,9,1000,10,0,44,'2019-10-04 16:09:33','2019-10-04 16:10:18','2019-10-04 16:10:18'),(876,10,1000,10,0,46,'2019-10-04 16:10:01','2019-10-04 16:10:47','2019-10-04 16:10:47'),(877,6,1000,5,0,419,'2019-10-04 16:10:47','2019-10-04 16:17:46','2019-10-04 16:17:46'),(878,5,1000,5,0,436,'2019-10-04 16:10:47','2019-10-04 16:18:04','2019-10-04 16:18:04'),(879,4,1000,10,0,51,'2019-10-05 16:03:17','2019-10-05 16:04:08','2019-10-05 16:04:08'),(880,3,1000,10,0,51,'2019-10-05 16:03:17','2019-10-05 16:04:08','2019-10-05 16:04:08'),(881,7,1000,10,0,237,'2019-10-05 16:04:08','2019-10-05 16:08:05','2019-10-05 16:08:05'),(882,8,1000,10,0,256,'2019-10-05 16:04:08','2019-10-05 16:08:24','2019-10-05 16:08:24'),(883,1,1000,10,0,1061,'2019-10-05 16:08:05','2019-10-05 16:25:47','2019-10-05 16:25:47'),(884,2,1000,10,0,1054,'2019-10-05 16:08:24','2019-10-05 16:25:59','2019-10-05 16:25:59'),(885,9,1000,10,0,44,'2019-10-05 16:25:48','2019-10-05 16:26:32','2019-10-05 16:26:32'),(886,10,1000,10,0,46,'2019-10-05 16:25:59','2019-10-05 16:26:45','2019-10-05 16:26:45'),(887,5,1000,5,0,424,'2019-10-05 16:26:45','2019-10-05 16:33:49','2019-10-05 16:33:49'),(888,6,1000,5,0,440,'2019-10-05 16:26:45','2019-10-05 16:34:05','2019-10-05 16:34:05'),(889,3,1000,10,0,54,'2019-10-06 16:03:16','2019-10-06 16:04:10','2019-10-06 16:04:10'),(890,4,1000,10,0,55,'2019-10-06 16:03:16','2019-10-06 16:04:11','2019-10-06 16:04:11'),(891,7,1000,10,0,255,'2019-10-06 16:04:11','2019-10-06 16:08:26','2019-10-06 16:08:26'),(892,8,1000,10,0,271,'2019-10-06 16:04:10','2019-10-06 16:08:42','2019-10-06 16:08:42'),(893,1,1000,10,0,70,'2019-10-06 16:08:26','2019-10-06 16:09:37','2019-10-06 16:09:37'),(894,2,1000,10,0,70,'2019-10-06 16:08:42','2019-10-06 16:09:52','2019-10-06 16:09:52'),(895,9,1000,10,0,44,'2019-10-06 16:09:37','2019-10-06 16:10:22','2019-10-06 16:10:22'),(896,10,1000,10,0,44,'2019-10-06 16:09:52','2019-10-06 16:10:37','2019-10-06 16:10:37'),(897,5,1000,5,0,413,'2019-10-06 16:10:37','2019-10-06 16:17:30','2019-10-06 16:17:30'),(898,6,1000,5,0,417,'2019-10-06 16:10:37','2019-10-06 16:17:34','2019-10-06 16:17:34'),(899,4,1000,10,0,54,'2019-10-07 16:03:17','2019-10-07 16:04:11','2019-10-07 16:04:11'),(900,3,1000,10,0,75,'2019-10-07 16:03:17','2019-10-07 16:04:32','2019-10-07 16:04:32'),(901,7,1000,10,0,258,'2019-10-07 16:04:32','2019-10-07 16:08:50','2019-10-07 16:08:50'),(902,8,1000,10,0,285,'2019-10-07 16:04:11','2019-10-07 16:08:57','2019-10-07 16:08:57'),(903,2,1000,10,0,70,'2019-10-07 16:08:57','2019-10-07 16:10:07','2019-10-07 16:10:07'),(904,9,1000,10,0,50,'2019-10-07 16:10:07','2019-10-07 16:10:58','2019-10-07 16:10:58'),(905,10,1000,10,0,51,'2019-10-07 16:10:58','2019-10-07 16:11:49','2019-10-07 16:11:49'),(906,1,1000,10,0,987,'2019-10-07 16:08:50','2019-10-07 16:25:18','2019-10-07 16:25:18'),(907,5,1000,5,0,443,'2019-10-07 16:25:18','2019-10-07 16:32:41','2019-10-07 16:32:41'),(908,6,1000,5,0,448,'2019-10-07 16:25:18','2019-10-07 16:32:47','2019-10-07 16:32:47'),(909,3,1000,10,0,50,'2019-10-08 16:03:17','2019-10-08 16:04:07','2019-10-08 16:04:07'),(910,4,1000,10,0,50,'2019-10-08 16:03:17','2019-10-08 16:04:08','2019-10-08 16:04:08'),(911,7,1000,10,0,269,'2019-10-08 16:04:08','2019-10-08 16:08:37','2019-10-08 16:08:37'),(912,8,1000,10,0,281,'2019-10-08 16:04:07','2019-10-08 16:08:48','2019-10-08 16:08:48'),(913,2,1000,10,0,70,'2019-10-08 16:08:48','2019-10-08 16:09:59','2019-10-08 16:09:59'),(914,9,1000,10,0,44,'2019-10-08 16:09:59','2019-10-08 16:10:43','2019-10-08 16:10:43'),(915,10,1000,10,0,44,'2019-10-08 16:10:43','2019-10-08 16:11:28','2019-10-08 16:11:28'),(916,1,1000,10,0,929,'2019-10-08 16:08:37','2019-10-08 16:24:06','2019-10-08 16:24:06'),(917,5,1000,5,0,411,'2019-10-08 16:24:06','2019-10-08 16:30:58','2019-10-08 16:30:58'),(918,6,1000,5,0,450,'2019-10-08 16:24:06','2019-10-08 16:31:37','2019-10-08 16:31:37'),(919,3,1000,10,0,50,'2019-10-09 16:03:19','2019-10-09 16:04:09','2019-10-09 16:04:09'),(920,4,1000,10,0,51,'2019-10-09 16:03:19','2019-10-09 16:04:10','2019-10-09 16:04:10'),(921,7,1000,10,0,267,'2019-10-09 16:04:10','2019-10-09 16:08:38','2019-10-09 16:08:38'),(922,8,1000,10,0,284,'2019-10-09 16:04:09','2019-10-09 16:08:53','2019-10-09 16:08:53'),(923,1,1000,10,0,618,'2019-10-09 16:08:38','2019-10-09 16:18:56','2019-10-09 16:18:56'),(924,2,1000,10,0,626,'2019-10-09 16:08:53','2019-10-09 16:19:20','2019-10-09 16:19:20'),(925,9,1000,10,0,45,'2019-10-09 16:18:56','2019-10-09 16:19:41','2019-10-09 16:19:41'),(926,10,1000,10,0,51,'2019-10-09 16:19:20','2019-10-09 16:20:12','2019-10-09 16:20:12'),(927,5,1000,5,0,448,'2019-10-09 16:20:12','2019-10-09 16:27:40','2019-10-09 16:27:40'),(928,6,1000,5,0,493,'2019-10-09 16:20:12','2019-10-09 16:28:26','2019-10-09 16:28:26'),(929,4,1000,10,0,51,'2019-10-11 08:50:39','2019-10-11 08:51:30','2019-10-11 08:51:30'),(930,3,1000,10,0,52,'2019-10-11 08:50:39','2019-10-11 08:51:31','2019-10-11 08:51:31'),(931,7,1000,10,4684,7811,'2019-10-11 08:51:31','2019-10-11 11:01:43','2019-10-11 11:01:43'),(932,8,1000,10,4630,7826,'2019-10-11 08:51:30','2019-10-11 11:01:57','2019-10-11 11:01:57'),(933,1,1000,10,2767,9597,'2019-10-11 11:01:43','2019-10-11 13:41:40','2019-10-11 13:41:40'),(934,9,1000,10,0,48,'2019-10-11 13:41:40','2019-10-11 13:42:29','2019-10-11 13:42:29'),(935,10,1000,10,0,45,'2019-10-11 13:42:29','2019-10-11 13:43:15','2019-10-11 13:43:15'),(936,2,1000,10,3310,10824,'2019-10-11 11:01:57','2019-10-11 14:02:21','2019-10-11 14:02:21'),(937,6,1000,5,916,2432,'2019-10-11 14:02:21','2019-10-11 14:42:54','2019-10-11 14:42:54'),(938,5,1000,5,1043,2904,'2019-10-11 14:02:21','2019-10-11 14:50:46','2019-10-11 14:50:46'),(939,3,1000,10,0,46,'2019-10-11 16:03:14','2019-10-11 16:04:00','2019-10-11 16:04:00'),(940,4,1000,10,0,46,'2019-10-11 16:03:14','2019-10-11 16:04:00','2019-10-11 16:04:00'),(941,8,1000,10,4630,8018,'2019-10-11 16:04:00','2019-10-11 18:17:39','2019-10-11 18:17:39'),(942,1,1000,10,0,70,'2019-10-11 18:17:39','2019-10-11 18:18:49','2019-10-11 18:18:49'),(943,2,1000,10,0,70,'2019-10-11 18:18:49','2019-10-11 18:19:59','2019-10-11 18:19:59'),(944,9,1000,10,0,44,'2019-10-11 18:19:59','2019-10-11 18:20:44','2019-10-11 18:20:44'),(945,10,1000,10,0,44,'2019-10-11 18:20:44','2019-10-11 18:21:29','2019-10-11 18:21:29'),(946,7,1000,10,4684,8444,'2019-10-11 16:04:01','2019-10-11 18:24:45','2019-10-11 18:24:45'),(947,6,1000,5,914,2547,'2019-10-11 18:24:45','2019-10-11 19:07:12','2019-10-11 19:07:12'),(948,5,1000,5,1043,2773,'2019-10-11 18:24:45','2019-10-11 19:10:58','2019-10-11 19:10:58'),(949,8,1000,10,4630,7657,'2019-10-13 17:54:59','2019-10-13 20:02:36','2019-10-13 20:02:36'),(950,1,1000,10,0,70,'2019-10-13 20:02:36','2019-10-13 20:03:47','2019-10-13 20:03:47'),(951,2,1000,10,0,70,'2019-10-13 20:03:47','2019-10-13 20:04:57','2019-10-13 20:04:57'),(952,7,1000,10,4684,8230,'2019-10-13 18:00:08','2019-10-13 20:17:19','2019-10-13 20:17:19'),(953,6,1000,5,903,2689,'2019-10-13 20:17:19','2019-10-13 21:02:08','2019-10-13 21:02:08'),(954,5,1000,5,1026,3147,'2019-10-13 20:17:19','2019-10-13 21:09:47','2019-10-13 21:09:47'),(955,3,1000,10,1111,2014,'2019-10-14 04:49:30','2019-10-14 05:23:05','2019-10-14 05:23:05'),(956,4,1000,10,1036,2076,'2019-10-14 04:49:30','2019-10-14 05:24:07','2019-10-14 05:24:07'),(957,7,1000,10,3538,6199,'2019-10-14 05:24:07','2019-10-14 07:07:27','2019-10-14 07:07:27'),(958,8,1000,10,4348,7486,'2019-10-14 05:23:05','2019-10-14 07:27:52','2019-10-14 07:27:52'),(959,1,1000,10,2756,8291,'2019-10-14 07:07:27','2019-10-14 09:25:39','2019-10-14 09:25:39'),(960,3,1000,10,1111,2090,'2019-10-14 15:28:19','2019-10-14 16:03:10','2019-10-14 16:03:10'),(961,4,1000,10,1036,2215,'2019-10-14 15:28:19','2019-10-14 16:05:15','2019-10-14 16:05:15'),(962,7,1000,10,3538,7501,'2019-10-14 16:05:15','2019-10-14 18:10:17','2019-10-14 18:10:17'),(963,8,1000,10,4348,8322,'2019-10-14 16:03:10','2019-10-14 18:21:52','2019-10-14 18:21:52'),(964,1,1000,10,2768,19203,'2019-10-14 18:10:17','2019-10-14 23:30:20','2019-10-14 23:30:20'),(965,2,1000,10,3283,23903,'2019-10-14 18:21:52','2019-10-15 01:00:16','2019-10-15 01:00:16'),(966,6,1000,5,889,2302,'2019-10-15 01:00:16','2019-10-15 01:38:38','2019-10-15 01:38:38'),(967,5,1000,5,1012,2818,'2019-10-15 01:00:16','2019-10-15 01:47:14','2019-10-15 01:47:14'),(968,3,1000,10,1111,2193,'2019-10-17 16:49:23','2019-10-17 17:25:57','2019-10-17 17:25:57'),(969,4,1000,10,1036,2395,'2019-10-17 16:49:23','2019-10-17 17:29:19','2019-10-17 17:29:19'),(970,7,1000,10,3563,7033,'2019-10-17 17:29:21','2019-10-17 19:26:35','2019-10-17 19:26:35'),(971,1,1000,8,0,70,'2019-10-17 19:26:35','2019-10-17 19:27:45','2019-10-17 19:27:45'),(972,2,1000,8,0,70,'2019-10-17 19:27:45','2019-10-17 19:28:56','2019-10-17 19:28:56'),(973,8,1000,10,4368,7683,'2019-10-17 17:25:57','2019-10-17 19:34:00','2019-10-17 19:34:00'),(974,6,1000,5,892,2197,'2019-10-17 19:34:03','2019-10-17 20:10:40','2019-10-17 20:10:40'),(975,5,1000,5,975,2832,'2019-10-17 19:34:03','2019-10-17 20:21:15','2019-10-17 20:21:15'),(976,2,1000,10,0,70,'2019-10-18 17:09:53','2019-10-18 17:11:04','2019-10-18 17:11:04'),(977,3,1000,10,1110,1713,'2019-10-18 17:11:04','2019-10-18 17:39:37','2019-10-18 17:39:37'),(978,1,1000,10,2764,9222,'2019-10-18 19:13:57','2019-10-18 21:47:39','2019-10-18 21:47:39'),(979,2,1000,10,2758,10336,'2019-10-18 19:13:57','2019-10-18 22:06:13','2019-10-18 22:06:13'),(980,3,1000,10,1110,1618,'2019-10-18 21:47:39','2019-10-18 22:14:38','2019-10-18 22:14:38'),(981,4,1000,10,1035,2624,'2019-10-18 22:06:13','2019-10-18 22:49:57','2019-10-18 22:49:57'),(982,8,1000,10,4368,7398,'2019-10-18 22:14:38','2019-10-19 00:17:56','2019-10-19 00:17:56'),(983,7,1000,10,3561,5469,'2019-10-18 23:16:16','2019-10-19 00:47:26','2019-10-19 00:47:26'),(984,6,1000,5,920,2294,'2019-10-19 03:47:04','2019-10-19 04:25:19','2019-10-19 04:25:19'),(985,5,1000,5,1019,2319,'2019-10-19 03:47:04','2019-10-19 04:25:44','2019-10-19 04:25:44'),(986,2,1000,10,2738,8754,'2019-10-20 14:09:37','2019-10-20 16:35:32','2019-10-20 16:35:32'),(987,3,1000,10,1110,1754,'2019-10-20 16:35:32','2019-10-20 17:04:47','2019-10-20 17:04:47'),(988,1,1000,10,2759,10952,'2019-10-20 14:09:37','2019-10-20 17:12:10','2019-10-20 17:12:10'),(989,4,1000,10,1035,2118,'2019-10-20 17:04:47','2019-10-20 17:40:05','2019-10-20 17:40:05'),(990,8,1000,10,4368,7464,'2019-10-20 17:12:10','2019-10-20 19:16:34','2019-10-20 19:16:34'),(991,7,1000,10,3561,6367,'2019-10-20 17:40:05','2019-10-20 19:26:13','2019-10-20 19:26:13'),(992,6,1000,5,913,2307,'2019-10-20 19:26:13','2019-10-20 20:04:40','2019-10-20 20:04:40'),(993,5,1000,5,1018,2451,'2019-10-20 19:26:13','2019-10-20 20:07:04','2019-10-20 20:07:04'),(994,1,1000,10,2736,5757,'2019-10-21 10:18:17','2019-10-21 11:54:14','2019-10-21 11:54:14'),(995,2,1000,10,2697,7312,'2019-10-21 10:18:17','2019-10-21 12:20:09','2019-10-21 12:20:09'),(996,3,1000,10,1144,1776,'2019-10-21 11:54:14','2019-10-21 12:23:50','2019-10-21 12:23:50'),(997,4,1000,10,1083,2719,'2019-10-21 12:20:09','2019-10-21 13:05:28','2019-10-21 13:05:28'),(998,8,1000,10,4368,7395,'2019-10-21 12:23:50','2019-10-21 14:27:06','2019-10-21 14:27:06'),(999,7,1000,10,3561,5915,'2019-10-21 13:05:28','2019-10-21 14:44:04','2019-10-21 14:44:04'),(1000,6,1000,5,916,2199,'2019-10-21 14:44:04','2019-10-21 15:20:43','2019-10-21 15:20:43'),(1001,5,1000,5,1015,2423,'2019-10-21 14:44:04','2019-10-21 15:24:28','2019-10-21 15:24:28'),(1002,1,1000,12,2726,7462,'2019-10-22 12:56:25','2019-10-22 15:00:47','2019-10-22 15:00:47'),(1003,2,1000,12,2686,8274,'2019-10-22 12:56:25','2019-10-22 15:14:20','2019-10-22 15:14:20'),(1004,3,1000,12,1144,1844,'2019-10-22 15:00:47','2019-10-22 15:31:32','2019-10-22 15:31:32'),(1005,4,1000,12,1083,1859,'2019-10-22 15:14:20','2019-10-22 15:45:20','2019-10-22 15:45:20'),(1006,8,1000,12,0,1380,'2019-10-22 15:31:32','2019-10-22 15:54:32','2019-10-22 15:54:32'),(1007,7,1000,12,0,1379,'2019-10-22 15:45:20','2019-10-22 16:08:19','2019-10-22 16:08:19'),(1008,2,1000,10,2690,6932,'2019-10-22 16:49:32','2019-10-22 18:45:04','2019-10-22 18:45:04'),(1009,3,1000,10,1144,1921,'2019-10-22 18:45:04','2019-10-22 19:17:06','2019-10-22 19:17:06'),(1010,1,1000,10,2719,10637,'2019-10-22 16:49:32','2019-10-22 19:46:49','2019-10-22 19:46:49'),(1011,4,1000,10,1083,1917,'2019-10-22 19:17:06','2019-10-22 19:49:03','2019-10-22 19:49:03'),(1012,8,1000,10,0,1381,'2019-10-22 19:46:49','2019-10-22 20:09:51','2019-10-22 20:09:51'),(1013,7,1000,10,0,1376,'2019-10-22 19:49:03','2019-10-22 20:11:59','2019-10-22 20:11:59'),(1014,6,1000,5,910,2305,'2019-10-22 20:11:59','2019-10-22 20:50:25','2019-10-22 20:50:25'),(1015,5,1000,5,1027,3086,'2019-10-22 20:11:59','2019-10-22 21:03:26','2019-10-22 21:03:26'),(1016,7,1000,10,0,418,'2019-10-28 14:04:58','2019-10-28 14:11:56','2019-10-28 14:11:56'),(1017,7,1000,10,0,417,'2019-10-28 14:24:37','2019-10-28 14:31:34','2019-10-28 14:31:34'),(1018,7,1000,10,1358,1777,'2019-10-28 16:36:05','2019-10-28 17:05:43','2019-10-28 17:05:43'),(1019,7,1000,10,1369,1797,'2019-10-29 16:27:49','2019-10-29 16:57:47','2019-10-29 16:57:47'),(1020,1,1000,10,2660,3697,'2019-10-30 16:54:25','2019-10-30 17:56:02','2019-10-30 17:56:02'),(1021,2,1000,10,2984,4784,'2019-10-30 16:54:25','2019-10-30 18:14:09','2019-10-30 18:14:09'),(1022,3,1000,10,1001,1650,'2019-10-30 17:56:02','2019-10-30 18:23:33','2019-10-30 18:23:33'),(1023,4,1000,10,951,1958,'2019-10-30 18:14:09','2019-10-30 18:46:48','2019-10-30 18:46:48'),(1024,7,1000,10,1375,1798,'2019-10-30 18:23:33','2019-10-30 18:53:31','2019-10-30 18:53:31'),(1025,5,1000,5,1016,1384,'2019-10-30 18:53:31','2019-10-30 19:16:36','2019-10-30 19:16:36'),(1026,6,1000,5,738,1925,'2019-10-30 18:53:31','2019-10-30 19:25:37','2019-10-30 19:25:37'),(1027,1,1000,10,2783,3230,'2019-10-31 16:20:16','2019-10-31 17:14:06','2019-10-31 17:14:06'),(1028,7,1000,10,3288,3733,'2019-10-31 16:20:16','2019-10-31 17:22:29','2019-10-31 17:22:29'),(1029,3,1000,10,1001,1433,'2019-10-31 17:22:29','2019-10-31 17:46:23','2019-10-31 17:46:23'),(1030,4,1000,10,951,1640,'2019-10-31 17:46:23','2019-10-31 18:13:44','2019-10-31 18:13:44'),(1031,2,1000,10,3169,3604,'2019-10-31 17:14:06','2019-10-31 18:14:10','2019-10-31 18:14:10'),(1032,5,1000,5,1008,1493,'2019-10-31 18:14:11','2019-10-31 18:39:05','2019-10-31 18:39:05'),(1033,6,1000,5,736,1964,'2019-10-31 18:14:11','2019-10-31 18:46:55','2019-10-31 18:46:55'),(1034,7,1000,10,3314,3737,'2019-11-02 09:04:58','2019-11-02 10:07:15','2019-11-02 10:07:15'),(1035,1,1000,10,2772,7322,'2019-11-02 09:04:58','2019-11-02 11:07:01','2019-11-02 11:07:01'),(1036,3,1000,10,1000,1423,'2019-11-02 11:07:01','2019-11-02 11:30:45','2019-11-02 11:30:45'),(1037,4,1000,10,950,1453,'2019-11-02 11:30:45','2019-11-02 11:54:58','2019-11-02 11:54:58'),(1038,2,1000,10,3194,8929,'2019-11-02 10:07:15','2019-11-02 12:36:05','2019-11-02 12:36:05'),(1039,5,1000,5,1006,1533,'2019-11-02 12:36:05','2019-11-02 13:01:39','2019-11-02 13:01:39'),(1040,6,1000,5,719,1763,'2019-11-02 12:36:05','2019-11-02 13:05:29','2019-11-02 13:05:29'),(1041,7,1000,10,3274,3694,'2019-11-04 16:28:47','2019-11-04 17:30:22','2019-11-04 17:30:22'),(1042,2,1000,10,3362,6232,'2019-11-04 17:30:22','2019-11-04 19:14:15','2019-11-04 19:14:15'),(1043,1,1000,10,2751,10128,'2019-11-04 16:28:47','2019-11-04 19:17:36','2019-11-04 19:17:36'),(1044,3,1000,10,1047,1660,'2019-11-04 19:14:15','2019-11-04 19:41:55','2019-11-04 19:41:55'),(1045,4,1000,10,997,1967,'2019-11-04 19:17:36','2019-11-04 19:50:24','2019-11-04 19:50:24'),(1046,5,1000,5,1039,1393,'2019-11-04 19:50:24','2019-11-04 20:13:38','2019-11-04 20:13:38'),(1047,6,1000,5,757,1805,'2019-11-04 19:50:24','2019-11-04 20:20:30','2019-11-04 20:20:30'),(1048,7,1000,10,4459,4982,'2019-11-07 17:26:35','2019-11-07 18:49:37','2019-11-07 18:49:37'),(1049,1,1000,10,2687,7863,'2019-11-07 17:26:35','2019-11-07 19:37:39','2019-11-07 19:37:39'),(1050,2,1000,10,3059,3923,'2019-11-07 18:49:37','2019-11-07 19:55:01','2019-11-07 19:55:01'),(1051,3,1000,10,1436,2103,'2019-11-07 19:37:39','2019-11-07 20:12:42','2019-11-07 20:12:42'),(1052,4,1000,10,1313,2134,'2019-11-07 19:55:01','2019-11-07 20:30:36','2019-11-07 20:30:36'),(1053,6,1000,5,749,1385,'2019-11-07 20:30:36','2019-11-07 20:53:41','2019-11-07 20:53:41'),(1054,5,1000,5,1047,1616,'2019-11-07 20:30:36','2019-11-07 20:57:32','2019-11-07 20:57:32'),(1055,5,1000,1,0,40,'2021-06-14 11:50:49','2021-06-14 11:51:30','2021-06-14 11:51:30'),(1056,5,500,6,1372,699,'2021-06-16 12:10:00','2021-06-16 12:21:39','2021-06-16 12:21:39'),(1057,6,500,6,1381,592,'2021-06-17 03:49:58','2021-06-17 03:59:50','2021-06-17 03:59:50'),(1058,5,500,6,1371,613,'2021-06-17 03:49:58','2021-06-17 04:00:12','2021-06-17 04:00:12'),(1059,1,1000,1,0,68,'2021-06-18 19:11:33','2021-06-18 19:12:41','2021-06-18 19:12:41'),(1060,1,1000,1,0,31,'2021-06-19 02:13:05','2021-06-19 02:13:37','2021-06-19 02:13:37'),(1061,1,1000,1,0,47,'2021-06-19 02:23:04','2021-06-19 02:23:52','2021-06-19 02:23:52'),(1062,1,1000,1,0,18,'2021-06-19 02:34:25','2021-06-19 02:34:43','2021-06-19 02:34:43'),(1063,1,1000,1,0,41,'2021-06-19 02:35:59','2021-06-19 02:36:40','2021-06-19 02:36:40'),(1064,1,1000,1,0,38,'2021-06-19 05:00:16','2021-06-19 05:00:54','2021-06-19 05:00:54'),(1065,1,1000,1,0,167,'2021-06-19 14:29:14','2021-06-19 14:32:01','2021-06-19 14:32:01'),(1066,1,1000,1,0,65,'2021-06-19 17:09:36','2021-06-19 17:10:41','2021-06-19 17:10:41'),(1067,1,500,1,5,24,'2021-06-19 17:47:10','2021-06-19 17:47:34','2021-06-19 17:47:34'),(1068,1,500,6,3099,1514,'2021-06-19 18:26:00','2021-06-19 18:51:15','2021-06-19 18:51:15'),(1069,5,500,6,1354,641,'2021-06-20 15:37:59','2021-06-20 15:48:41','2021-06-20 15:48:41'),(1070,6,500,6,1384,667,'2021-06-20 15:37:59','2021-06-20 15:49:07','2021-06-20 15:49:07'),(1071,1,500,6,2936,1593,'2021-06-20 15:37:59','2021-06-20 16:04:32','2021-06-20 16:04:32'),(1072,2,500,6,3868,2379,'2021-06-20 15:48:41','2021-06-20 16:28:20','2021-06-20 16:28:20'),(1073,6,500,6,1386,621,'2021-06-20 17:01:42','2021-06-20 17:12:04','2021-06-20 17:12:04'),(1074,5,500,6,1352,652,'2021-06-20 17:01:42','2021-06-20 17:12:34','2021-06-20 17:12:34'),(1075,1,500,6,2937,1296,'2021-06-20 17:01:42','2021-06-20 17:23:19','2021-06-20 17:23:19'),(1076,5,500,6,1351,507,'2021-06-20 19:30:35','2021-06-20 19:39:03','2021-06-20 19:39:03'),(1077,6,500,6,1395,510,'2021-06-20 19:30:35','2021-06-20 19:39:06','2021-06-20 19:39:06'),(1078,1,500,6,2937,1391,'2021-06-20 19:30:35','2021-06-20 19:53:47','2021-06-20 19:53:47'),(1079,2,500,6,2445,5580,'2021-06-20 19:39:03','2021-06-20 21:12:03','2021-06-20 21:12:03'),(1080,6,500,6,1388,490,'2021-06-21 02:31:55','2021-06-21 02:40:06','2021-06-21 02:40:06'),(1081,5,500,6,1416,531,'2021-06-21 02:31:55','2021-06-21 02:40:47','2021-06-21 02:40:47'),(1082,6,500,6,1397,494,'2021-06-21 03:33:37','2021-06-21 03:41:51','2021-06-21 03:41:51'),(1083,5,500,6,1414,516,'2021-06-21 03:33:37','2021-06-21 03:42:13','2021-06-21 03:42:13'),(1084,12,500,12,3898,1202,'2021-06-21 09:44:26','2021-06-21 10:04:29','2021-06-21 10:04:29'),(1085,12,1000,20,3900,750,'2021-06-21 12:20:19','2021-06-21 12:32:49','2021-06-21 12:32:49'),(1086,12,1000,10,3900,1377,'2021-06-21 12:48:01','2021-06-21 13:10:58','2021-06-21 13:10:58'),(1087,1,1000,20,2937,577,'2021-06-21 15:59:12','2021-06-21 16:08:49','2021-06-21 16:08:49'),(1088,2,1000,20,3047,1920,'2021-06-21 15:59:12','2021-06-21 16:31:12','2021-06-21 16:31:12'),(1089,2,500,24,3831,870,'2021-06-22 03:36:26','2021-06-22 03:50:57','2021-06-22 03:50:57'),(1090,6,500,24,1384,146,'2021-06-22 03:50:57','2021-06-22 03:53:23','2021-06-22 03:53:23'),(1091,1,500,24,2937,490,'2021-06-22 09:09:19','2021-06-22 09:15:13','2021-06-22 09:15:13'),(1092,12,500,24,3900,481,'2021-06-22 09:15:13','2021-06-22 09:23:14','2021-06-22 09:23:14'),(1093,5,500,24,1412,1130,'2021-06-22 09:23:19','2021-06-22 09:25:53','2021-06-22 09:25:53'),(1094,1,500,24,5874,553,'2021-06-22 11:38:57','2021-06-22 11:46:01','2021-06-22 11:46:01'),(1095,5,500,24,1443,365,'2021-06-22 11:49:03','2021-06-22 11:52:07','2021-06-22 11:52:07'),(1096,6,500,24,1372,359,'2021-06-22 11:55:11','2021-06-22 11:58:06','2021-06-22 11:58:06'),(1097,12,500,24,7766,877,'2021-06-22 12:01:09','2021-06-22 12:12:44','2021-06-22 12:12:44'),(1098,1,500,24,5872,651,'2021-06-23 04:47:07','2021-06-23 04:55:43','2021-06-23 04:55:43'),(1099,1,500,24,2931,513,'2021-06-23 07:10:04','2021-06-23 07:16:11','2021-06-23 07:16:11'),(1100,5,500,24,1403,375,'2021-06-23 07:19:27','2021-06-23 07:22:27','2021-06-23 07:22:27'),(1101,6,500,24,1373,374,'2021-06-23 07:25:41','2021-06-23 07:28:41','2021-06-23 07:28:41'),(1102,12,500,24,3897,829,'2021-06-23 07:31:56','2021-06-23 07:42:30','2021-06-23 07:42:30'),(1103,1,500,24,2937,587,'2021-06-23 16:36:06','2021-06-23 16:43:08','2021-06-23 16:43:08'),(1104,5,500,24,1411,335,'2021-06-23 17:12:33','2021-06-23 17:15:26','2021-06-23 17:15:26'),(1105,6,500,24,1362,385,'2021-06-23 17:18:59','2021-06-23 17:21:52','2021-06-23 17:21:52'),(1106,1,500,24,2936,525,'2021-06-23 17:25:28','2021-06-23 17:30:37','2021-06-23 17:30:37'),(1107,12,500,24,3792,874,'2021-06-23 17:34:12','2021-06-23 17:45:12','2021-06-23 17:45:12'),(1108,5,500,24,1404,280,'2021-06-23 18:16:59','2021-06-23 18:19:34','2021-06-23 18:19:34'),(1109,6,500,24,1388,377,'2021-06-23 18:22:52','2021-06-23 18:25:52','2021-06-23 18:25:52'),(1110,1,500,24,2936,621,'2021-06-24 03:46:49','2021-06-24 03:52:50','2021-06-24 03:52:50'),(1111,5,500,24,1399,454,'2021-06-24 03:57:40','2021-06-24 04:00:25','2021-06-24 04:00:25'),(1112,6,500,24,1367,485,'2021-06-24 04:05:13','2021-06-24 04:08:30','2021-06-24 04:08:30'),(1113,1,500,24,2935,625,'2021-06-24 04:53:49','2021-06-24 04:59:59','2021-06-24 04:59:59'),(1114,5,500,24,1428,465,'2021-06-24 05:04:51','2021-06-24 05:07:45','2021-06-24 05:07:45'),(1115,6,500,24,1380,513,'2021-06-24 05:12:55','2021-06-24 05:16:18','2021-06-24 05:16:18'),(1116,12,500,24,3898,778,'2021-06-24 05:21:20','2021-06-24 05:29:17','2021-06-24 05:29:17'),(1117,1,500,24,2734,700,'2021-06-30 07:56:23','2021-06-30 08:05:48','2021-06-30 08:05:48'),(1118,5,500,24,1455,439,'2021-06-30 08:10:16','2021-06-30 08:13:08','2021-06-30 08:13:08'),(1119,6,500,24,1350,477,'2021-06-30 08:17:45','2021-06-30 08:21:05','2021-06-30 08:21:05'),(1120,12,500,24,3629,706,'2021-06-30 08:25:25','2021-06-30 08:32:52','2021-06-30 08:32:52'),(1121,1,500,24,2837,531,'2021-07-01 17:07:20','2021-07-01 17:13:05','2021-07-01 17:13:05'),(1122,5,500,24,1453,424,'2021-07-01 17:17:36','2021-07-01 17:20:09','2021-07-01 17:20:09'),(1123,6,500,24,1350,467,'2021-07-01 17:24:51','2021-07-01 17:27:56','2021-07-01 17:27:56'),(1124,12,500,24,3605,708,'2021-07-01 17:32:29','2021-07-01 17:39:44','2021-07-01 17:39:44'),(1125,1,500,24,2801,716,'2021-07-03 08:55:55','2021-07-03 09:05:16','2021-07-03 09:05:16'),(1126,1,500,24,2802,746,'2021-07-03 09:21:17','2021-07-03 09:30:17','2021-07-03 09:30:17'),(1127,5,500,24,1414,446,'2021-07-03 09:34:59','2021-07-03 09:37:44','2021-07-03 09:37:44'),(1128,6,500,24,1334,453,'2021-07-03 09:42:20','2021-07-03 09:45:17','2021-07-03 09:45:17'),(1129,12,500,24,3603,958,'2021-07-03 09:49:55','2021-07-03 10:01:16','2021-07-03 10:01:16'),(1130,2,500,24,3470,3511,'2021-07-03 10:19:46','2021-07-03 11:15:40','2021-07-03 11:15:40'),(1131,3,1000,1,0,72,'2021-07-04 17:32:45','2021-07-04 17:33:27','2021-07-04 17:33:27'),(1132,3,1000,24,0,398,'2021-07-07 03:28:46','2021-07-07 03:32:34','2021-07-07 03:32:34'),(1133,3,1000,24,1155,444,'2021-07-07 04:08:52','2021-07-07 04:12:30','2021-07-07 04:12:30'),(1134,4,1000,24,789,395,'2021-07-07 04:36:42','2021-07-07 04:39:58','2021-07-07 04:39:58'),(1135,3,1000,24,1155,415,'2021-07-07 04:59:46','2021-07-07 05:03:34','2021-07-07 05:03:34'),(1136,3,1000,24,1155,353,'2021-07-07 05:17:44','2021-07-07 05:20:45','2021-07-07 05:20:45'),(1137,4,1000,24,789,451,'2021-07-07 05:25:45','2021-07-07 05:28:17','2021-07-07 05:28:17'),(1138,3,1000,24,1154,335,'2021-07-07 10:00:31','2021-07-07 10:03:33','2021-07-07 10:03:33'),(1139,4,1000,24,789,423,'2021-07-07 10:08:10','2021-07-07 10:10:37','2021-07-07 10:10:37'),(1140,3,1000,24,1219,445,'2021-07-07 10:58:22','2021-07-07 11:02:07','2021-07-07 11:02:07'),(1141,4,1000,24,777,527,'2021-07-07 11:07:12','2021-07-07 11:10:54','2021-07-07 11:10:54'),(1142,3,1000,24,1656,541,'2021-07-07 13:13:35','2021-07-07 13:18:24','2021-07-07 13:18:24'),(1143,4,1000,24,810,514,'2021-07-07 13:23:28','2021-07-07 13:26:58','2021-07-07 13:26:58');
/*!40000 ALTER TABLE `crawling_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `currency` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `short_code` varchar(3) DEFAULT NULL,
  `vnd_exchange_rate` decimal(10,2) DEFAULT NULL,
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (1,'Vietnam dong','VND',1.00,'6038a388579a11e8bd2234e6d72f3065','2017-07-14 23:49:02','2017-07-14 23:49:02'),(2,'Singapore Dollar','SGD',17500.00,'60397ee4579a11e8bd2234e6d72f3065','2017-07-14 23:44:31','2019-09-02 18:14:34'),(3,'Malaysian Ringgit','MYR',6600.00,'603a3b93579a11e8bd2234e6d72f3065','2017-07-14 23:44:31','2019-11-06 16:48:05');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `currency_BEFORE_INSERT` BEFORE INSERT ON `currency` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `error_summary`
--

DROP TABLE IF EXISTS `error_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `error_summary` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `run_uid` varchar(45) DEFAULT NULL,
  `message` tinytext NOT NULL,
  `stack_trace` mediumtext,
  `data` text,
  `status` int NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `error_summary`
--

LOCK TABLES `error_summary` WRITE;
/*!40000 ALTER TABLE `error_summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `error_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_id` bigint NOT NULL,
  `sub_brand` int DEFAULT NULL,
  `category_id` bigint NOT NULL,
  `code` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `uuid` varchar(32) DEFAULT NULL,
  `max_sale_rate` decimal(10,0) NOT NULL DEFAULT '0',
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `crawled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_item_brand1_idx` (`brand_id`),
  KEY `fk_item_category1_idx` (`category_id`),
  CONSTRAINT `fk_item_brand1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`),
  CONSTRAINT `fk_item_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `item_BEFORE_INSERT` BEFORE INSERT ON `item` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `item_country_detail`
--

DROP TABLE IF EXISTS `item_country_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_country_detail` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `item_id` bigint NOT NULL,
  `brand_country_id` bigint NOT NULL,
  `brand_id` bigint NOT NULL,
  `sub_brand` int DEFAULT NULL,
  `country_id` bigint NOT NULL,
  `category_crawler_id` bigint NOT NULL,
  `code` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` text NOT NULL,
  `old_price` decimal(19,2) DEFAULT NULL,
  `price` decimal(19,2) NOT NULL,
  `special_price` decimal(19,2) DEFAULT NULL,
  `sale_rate` int NOT NULL DEFAULT '0',
  `url` text NOT NULL,
  `parent_url` text,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `crawled` tinyint(1) NOT NULL DEFAULT '1',
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `brand` (`item_id`),
  KEY `fk_item_country_detail_country1_idx` (`country_id`),
  KEY `fk_item_country_detail_brand1_idx` (`brand_id`),
  KEY `fk_item_country_detail_category_crawler1_idx` (`category_crawler_id`),
  CONSTRAINT `fk_item_country_detail_brand1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`),
  CONSTRAINT `fk_item_country_detail_category_crawler1` FOREIGN KEY (`category_crawler_id`) REFERENCES `category_crawler` (`id`),
  CONSTRAINT `fk_item_country_detail_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  CONSTRAINT `fk_item_country_detail_item1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_country_detail`
--

LOCK TABLES `item_country_detail` WRITE;
/*!40000 ALTER TABLE `item_country_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_country_detail` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `item_country_detail_BEFORE_INSERT` BEFORE INSERT ON `item_country_detail` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `item_country_detail_AFTER_INSERT` AFTER INSERT ON `item_country_detail` FOR EACH ROW BEGIN
	INSERT INTO `price_history` (item_detail_id, old_price_n, price_n, special_price_n) 
    VALUES (NEW.id, NEW.old_price, NEW.price, NEW.special_price);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `item_country_detail_AFTER_UPDATE` AFTER UPDATE ON `item_country_detail` FOR EACH ROW BEGIN
	INSERT INTO `price_history` (item_detail_id, old_price, price, special_price, old_price_n, price_n, special_price_n) 
    VALUES (NEW.id, OLD.old_price, OLD.price, OLD.special_price, NEW.old_price, NEW.price, NEW.special_price);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `item_image`
--

DROP TABLE IF EXISTS `item_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_image` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `item_detail_id` bigint NOT NULL,
  `thumb_url` text,
  `url` text NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_item_image_item_country_detail1_idx` (`item_detail_id`),
  CONSTRAINT `fk_item_image_item_country_detail1` FOREIGN KEY (`item_detail_id`) REFERENCES `item_country_detail` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_image`
--

LOCK TABLES `item_image` WRITE;
/*!40000 ALTER TABLE `item_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_image` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `item_image_BEFORE_INSERT` BEFORE INSERT ON `item_image` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `item_stock`
--

DROP TABLE IF EXISTS `item_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_stock` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `item_detail_id` bigint NOT NULL,
  `color_code` varchar(100) DEFAULT NULL,
  `size_code` varchar(100) DEFAULT NULL,
  `stock` int NOT NULL DEFAULT '0',
  `old_price` decimal(19,2) DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `special_price` decimal(19,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`item_detail_id`,`color_code`,`size_code`),
  CONSTRAINT `fk_item_stock_item_country_detail1` FOREIGN KEY (`item_detail_id`) REFERENCES `item_country_detail` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_stock`
--

LOCK TABLES `item_stock` WRITE;
/*!40000 ALTER TABLE `item_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_stock` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `item_stock_BEFORE_INSERT` BEFORE INSERT ON `item_stock` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `key_value`
--

DROP TABLE IF EXISTS `key_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `key_value` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `key` varchar(256) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_uniq` (`key`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=702 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `key_value`
--

LOCK TABLES `key_value` WRITE;
/*!40000 ALTER TABLE `key_value` DISABLE KEYS */;
INSERT INTO `key_value` VALUES (1,'SMA007','XXL','2019-09-05 17:00:22','2019-09-06 13:50:00'),(2,'SMA004','M','2019-09-05 17:00:22','2019-09-06 13:50:00'),(3,'SMA005','L','2019-09-05 17:00:22','2019-09-06 13:50:00'),(4,'SMA002','XS','2019-09-05 17:00:22','2019-09-06 13:50:00'),(5,'SMA003','S','2019-09-05 17:00:22','2019-09-06 13:50:00'),(6,'SMA006','XL','2019-09-05 17:00:22','2019-09-06 13:50:00'),(7,'BRL801','65/70 DEF','2019-09-05 17:00:22','2019-09-06 13:58:27'),(8,'BRL802','75/80 ABC','2019-09-05 17:00:22','2019-09-06 13:58:27'),(9,'BRL803','75/80 DEF','2019-09-05 17:00:22','2019-09-06 13:58:27'),(10,'BRL804','85/90 ABC','2019-09-05 17:00:22','2019-09-06 13:58:27'),(11,'BRL805','85/90 DEF','2019-09-05 17:00:22','2019-09-06 13:58:27'),(12,'BRL800','65/70 ABC','2019-09-05 17:00:22','2019-09-06 13:58:27'),(13,'CMD076','CMD076','2019-09-05 17:00:22','2019-09-05 17:00:22'),(14,'CMD079','CMD079','2019-09-05 17:00:22','2019-09-05 17:00:22'),(15,'CMD082','CMD082','2019-09-05 17:00:22','2019-09-05 17:00:22'),(16,'CMD085','CMD085','2019-09-05 17:00:22','2019-09-05 17:00:22'),(17,'CMD088','CMD088','2019-09-05 17:00:22','2019-09-05 17:00:22'),(18,'CMD095','CMD095','2019-09-05 17:00:22','2019-09-05 17:00:22'),(19,'CMD100','CMD100','2019-09-05 17:00:22','2019-09-05 17:00:22'),(20,'CMD073','CMD073','2019-09-05 17:00:22','2019-09-05 17:00:22'),(21,'SMA008','3XL','2019-09-05 17:00:22','2019-09-06 13:50:58'),(22,'CMD091','CMD091','2019-09-05 17:00:22','2019-09-05 17:00:22'),(23,'CMA080','80cm','2019-09-05 17:00:22','2019-09-06 14:00:44'),(24,'CMA090','90cm','2019-09-05 17:00:22','2019-09-06 14:00:44'),(25,'INS027','27 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(26,'INS028','28 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(27,'INS029','29 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(28,'INS030','30 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(29,'INS031','31 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(30,'INS032','32 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(31,'INS033','33 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(32,'INS034','34 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(33,'INS035','35 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(34,'KSC016','KSC016','2019-09-05 17:00:22','2019-09-05 17:00:22'),(35,'KSC022','KSC022','2019-09-05 17:00:22','2019-09-05 17:00:22'),(36,'KSC019','KSC019','2019-09-05 17:00:22','2019-09-05 17:00:22'),(37,'INS036','36 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(38,'INS038','38 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(39,'SIZ999','SIZ999','2019-09-05 17:00:22','2019-09-05 17:00:22'),(40,'CMB050','CMB050','2019-09-05 17:00:22','2019-09-05 17:00:22'),(41,'CMB060','CMB060','2019-09-05 17:00:22','2019-09-05 17:00:22'),(42,'CMB070','CMB070','2019-09-05 17:00:22','2019-09-05 17:00:22'),(43,'CMB080','CMB080','2019-09-05 17:00:22','2019-09-05 17:00:22'),(44,'CMB090','CMB090','2019-09-05 17:00:22','2019-09-05 17:00:22'),(45,'INS022','22 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(46,'INS023','23 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(47,'INS024','24 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(48,'INS025','25 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(49,'INS026','26 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(50,'INS021','21 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(51,'CMA100','100cm','2019-09-05 17:00:22','2019-09-06 14:00:44'),(52,'CMC110','110cm','2019-09-05 17:00:22','2019-09-06 14:03:11'),(53,'CMC120','120cm','2019-09-05 17:00:22','2019-09-06 14:03:11'),(54,'CMC130','130cm','2019-09-05 17:00:22','2019-09-06 14:03:11'),(55,'CMC140','140cm','2019-09-05 17:00:22','2019-09-06 14:03:11'),(56,'CMC150','150cm','2019-09-05 17:00:22','2019-09-06 14:03:11'),(57,'CMC100','100cm','2019-09-05 17:00:22','2019-09-06 14:03:11'),(58,'WSC023','23-25cm','2019-09-05 17:00:22','2019-09-06 13:57:09'),(59,'CMC160','160cm','2019-09-05 17:00:22','2019-09-06 14:03:11'),(60,'MSC025','MSC025','2019-09-05 17:00:22','2019-09-05 17:00:22'),(61,'WSC025','25-27cm','2019-09-05 17:00:22','2019-09-06 13:57:09'),(62,'WBJ003','WBJ003','2019-09-05 17:00:22','2019-09-05 17:00:22'),(63,'SSE350','35','2019-09-05 17:00:22','2019-09-06 13:55:54'),(64,'SSE390','39','2019-09-05 17:00:22','2019-09-06 13:55:54'),(65,'BSC009','BSC009','2019-09-05 17:00:22','2019-09-05 17:00:22'),(66,'BSC012','BSC012','2019-09-05 17:00:22','2019-09-05 17:00:22'),(67,'SSE400','40','2019-09-05 17:00:22','2019-09-06 13:55:54'),(68,'CMD058','CMD058','2019-09-05 17:00:22','2019-09-05 17:00:22'),(69,'SSE360','36','2019-09-05 17:00:22','2019-09-06 13:55:54'),(70,'SSE430','43','2019-09-05 17:00:22','2019-09-06 13:55:54'),(71,'CMA110','110cm','2019-09-05 17:00:22','2019-09-06 14:00:44'),(72,'CMA070','70cm','2019-09-05 17:00:22','2019-09-06 14:00:44'),(73,'SMA009','SMA009','2019-09-05 17:00:22','2019-09-05 17:00:22'),(74,'CMD070','CMD070','2019-09-05 17:00:22','2019-09-05 17:00:22'),(75,'CMD061','CMD061','2019-09-05 17:00:22','2019-09-05 17:00:22'),(76,'CMD064','CMD064','2019-09-05 17:00:22','2019-09-05 17:00:22'),(77,'CMD067','CMD067','2019-09-05 17:00:22','2019-09-05 17:00:22'),(78,'SSE370','37','2019-09-05 17:00:22','2019-09-06 13:55:54'),(79,'SSE380','38','2019-09-05 17:00:22','2019-09-06 13:55:54'),(80,'KSS016','KSS016','2019-09-05 17:00:22','2019-09-05 17:00:22'),(81,'KSS020','KSS020','2019-09-05 17:00:22','2019-09-05 17:00:22'),(82,'KSS019','KSS019','2019-09-05 17:00:22','2019-09-05 17:00:22'),(83,'KSS022','KSS022','2019-09-05 17:00:22','2019-09-05 17:00:22'),(84,'SMM004','SMM004','2019-09-05 17:00:22','2019-09-05 17:00:22'),(85,'FRE001','FRE001','2019-09-05 17:00:22','2019-09-05 17:00:22'),(86,'SMM005','SMM005','2019-09-05 17:00:22','2019-09-05 17:00:22'),(87,'SSE385','38.5','2019-09-05 17:00:22','2019-09-06 13:55:54'),(88,'INS040','40 inch','2019-09-05 17:00:22','2019-09-06 13:53:44'),(128,'2XS','2XS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(129,'A/L','A/L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(130,'A/M','A/M','2019-09-05 17:02:35','2019-09-05 17:02:35'),(131,'A/S','A/S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(132,'A/XL','A/XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(133,'A/XS','A/XS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(134,'A2XL','A2XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(135,'A2XS','A2XS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(136,'L','L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(137,'M','M','2019-09-05 17:02:35','2019-09-05 17:02:35'),(138,'S','S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(139,'XL','XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(140,'XS','XS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(141,'10 UK','10 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(142,'10.5 UK','10.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(143,'11 UK','11 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(144,'11.5 UK','11.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(145,'12 UK','12 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(146,'12.5 UK','12.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(147,'13.5 UK','13.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(148,'6 UK','6 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(149,'6.5 UK','6.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(150,'7 UK','7 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(151,'7.5 UK','7.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(152,'8 UK','8 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(153,'8.5 UK','8.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(154,'9 UK','9 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(155,'9.5 UK','9.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(156,'2XL','2XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(157,'3.5 UK','3.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(158,'4 UK','4 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(159,'4.5 UK','4.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(160,'5 UK','5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(161,'5.5 UK','5.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(162,'A/2XL','A/2XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(163,'A/3XL','A/3XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(164,'A/4XL','A/4XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(165,'28','28','2019-09-05 17:02:35','2019-09-05 17:02:35'),(166,'30','30','2019-09-05 17:02:35','2019-09-05 17:02:35'),(167,'32','32','2019-09-05 17:02:35','2019-09-05 17:02:35'),(168,'34','34','2019-09-05 17:02:35','2019-09-05 17:02:35'),(169,'36','36','2019-09-05 17:02:35','2019-09-05 17:02:35'),(170,'38','38','2019-09-05 17:02:35','2019-09-05 17:02:35'),(171,'40','40','2019-09-05 17:02:35','2019-09-05 17:02:35'),(172,'42','42','2019-09-05 17:02:35','2019-09-05 17:02:35'),(173,'44','44','2019-09-05 17:02:35','2019-09-05 17:02:35'),(174,'46','46','2019-09-05 17:02:35','2019-09-05 17:02:35'),(175,'48','48','2019-09-05 17:02:35','2019-09-05 17:02:35'),(176,'J/L','J/L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(177,'J/M','J/M','2019-09-05 17:02:35','2019-09-05 17:02:35'),(178,'J/OT','J/OT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(179,'J/S','J/S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(180,'J/XOT','J/XOT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(181,'J/XS','J/XS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(182,'J2XOT','J2XOT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(183,'J/2XS','J/2XS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(184,'J/3XS','J/3XS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(185,'J3XOT','J3XOT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(186,'J4XOT','J4XOT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(187,'104','104','2019-09-05 17:02:35','2019-09-05 17:02:35'),(188,'110','110','2019-09-05 17:02:35','2019-09-05 17:02:35'),(189,'116','116','2019-09-05 17:02:35','2019-09-05 17:02:35'),(190,'92','92','2019-09-05 17:02:35','2019-09-05 17:02:35'),(191,'98','98','2019-09-05 17:02:35','2019-09-05 17:02:35'),(192,'70A','70A','2019-09-05 17:02:35','2019-09-05 17:02:35'),(193,'70B','70B','2019-09-05 17:02:35','2019-09-05 17:02:35'),(194,'75A','75A','2019-09-05 17:02:35','2019-09-05 17:02:35'),(195,'75B','75B','2019-09-05 17:02:35','2019-09-05 17:02:35'),(196,'80A','80A','2019-09-05 17:02:35','2019-09-05 17:02:35'),(197,'80B','80B','2019-09-05 17:02:35','2019-09-05 17:02:35'),(198,'85B','85B','2019-09-05 17:02:35','2019-09-05 17:02:35'),(199,'NS','NS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(200,'14.5 UK','14.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(201,'122','122','2019-09-05 17:02:35','2019-09-05 17:02:35'),(202,'128','128','2019-09-05 17:02:35','2019-09-05 17:02:35'),(203,'A/2XS','A/2XS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(204,'134','134','2019-09-05 17:02:35','2019-09-05 17:02:35'),(205,'140','140','2019-09-05 17:02:35','2019-09-05 17:02:35'),(206,'65A','65A','2019-09-05 17:02:35','2019-09-05 17:02:35'),(207,'65B','65B','2019-09-05 17:02:35','2019-09-05 17:02:35'),(208,'65C','65C','2019-09-05 17:02:35','2019-09-05 17:02:35'),(209,'65D','65D','2019-09-05 17:02:35','2019-09-05 17:02:35'),(210,'70C','70C','2019-09-05 17:02:35','2019-09-05 17:02:35'),(211,'70D','70D','2019-09-05 17:02:35','2019-09-05 17:02:35'),(212,'75C','75C','2019-09-05 17:02:35','2019-09-05 17:02:35'),(213,'75D','75D','2019-09-05 17:02:35','2019-09-05 17:02:35'),(214,'75E','75E','2019-09-05 17:02:35','2019-09-05 17:02:35'),(215,'80C','80C','2019-09-05 17:02:35','2019-09-05 17:02:35'),(216,'80D','80D','2019-09-05 17:02:35','2019-09-05 17:02:35'),(217,'80E','80E','2019-09-05 17:02:35','2019-09-05 17:02:35'),(218,'85A','85A','2019-09-05 17:02:35','2019-09-05 17:02:35'),(219,'85C','85C','2019-09-05 17:02:35','2019-09-05 17:02:35'),(220,'85D','85D','2019-09-05 17:02:35','2019-09-05 17:02:35'),(221,'85E','85E','2019-09-05 17:02:35','2019-09-05 17:02:35'),(222,'2XS4','2XS4','2019-09-05 17:02:35','2019-09-05 17:02:35'),(223,'L 3\"','L 3\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(224,'L 4\"','L 4\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(225,'M 3\"','M 3\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(226,'M 4\"','M 4\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(227,'S 3\"','S 3\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(228,'S 4\"','S 4\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(229,'XL3\"','XL3\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(230,'XL4\"','XL4\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(231,'XS3\"','XS3\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(232,'XS4\"','XS4\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(233,'2XS3','2XS3','2019-09-05 17:02:35','2019-09-05 17:02:35'),(234,'15 UK','15 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(235,'16 UK','16 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(236,'17 UK','17 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(237,'2XLL','2XLL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(238,'2XLT','2XLT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(239,'3XL','3XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(240,'3XLL','3XLL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(241,'3XLT','3XLT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(242,'4XL','4XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(243,'4XLT','4XLT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(244,'L/L','L/L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(245,'LT','LT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(246,'M/L','M/L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(247,'MT','MT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(248,'ST','ST','2019-09-05 17:02:35','2019-09-05 17:02:35'),(249,'XL/L','XL/L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(250,'XLT','XLT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(251,'320','320','2019-09-05 17:02:35','2019-09-05 17:02:35'),(252,'10','10','2019-09-05 17:02:35','2019-09-05 17:02:35'),(253,'10.5','10.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(254,'11','11','2019-09-05 17:02:35','2019-09-05 17:02:35'),(255,'11.5','11.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(256,'12','12','2019-09-05 17:02:35','2019-09-05 17:02:35'),(257,'12.5','12.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(258,'13.5','13.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(259,'3.5','3.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(260,'4','4','2019-09-05 17:02:35','2019-09-05 17:02:35'),(261,'4.5','4.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(262,'5','5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(263,'5.5','5.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(264,'6','6','2019-09-05 17:02:35','2019-09-05 17:02:35'),(265,'6.5','6.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(266,'7','7','2019-09-05 17:02:35','2019-09-05 17:02:35'),(267,'7.5','7.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(268,'8','8','2019-09-05 17:02:35','2019-09-05 17:02:35'),(269,'8.5','8.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(270,'9','9','2019-09-05 17:02:35','2019-09-05 17:02:35'),(271,'9.5','9.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(272,'146','146','2019-09-05 17:02:35','2019-09-05 17:02:35'),(273,'152','152','2019-09-05 17:02:35','2019-09-05 17:02:35'),(274,'158','158','2019-09-05 17:02:35','2019-09-05 17:02:35'),(275,'164','164','2019-09-05 17:02:35','2019-09-05 17:02:35'),(276,'170','170','2019-09-05 17:02:35','2019-09-05 17:02:35'),(277,'176','176','2019-09-05 17:02:35','2019-09-05 17:02:35'),(278,'13 UK','13 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(279,'2XLS','2XLS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(280,'2XSL','2XSL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(281,'2XSS','2XSS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(282,'L/S','L/S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(283,'M/S','M/S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(284,'S/L','S/L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(285,'S/S','S/S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(286,'XL/S','XL/S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(287,'XS/L','XS/L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(288,'XS/S','XS/S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(289,'J/O','J/O','2019-09-05 17:02:35','2019-09-05 17:02:35'),(290,'J/XO','J/XO','2019-09-05 17:02:35','2019-09-05 17:02:35'),(291,'1 UK','1 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(292,'1.5 UK','1.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(293,'10.5K','10.5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(294,'10K','10K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(295,'11.5K','11.5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(296,'11K','11K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(297,'12.5K','12.5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(298,'12K','12K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(299,'13.5K','13.5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(300,'13K','13K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(301,'2 UK','2 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(302,'2.5 UK','2.5 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(303,'3 UK','3 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(304,'2XOT','2XOT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(305,'3XOT','3XOT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(306,'A3XL','A3XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(307,'OT','OT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(308,'XOT','XOT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(309,'68','68','2019-09-05 17:02:35','2019-09-05 17:02:35'),(310,'74','74','2019-09-05 17:02:35','2019-09-05 17:02:35'),(311,'80','80','2019-09-05 17:02:35','2019-09-05 17:02:35'),(312,'86','86','2019-09-05 17:02:35','2019-09-05 17:02:35'),(313,'3','3','2019-09-05 17:02:35','2019-09-05 17:02:35'),(314,'LL','LL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(315,'ML','ML','2019-09-05 17:02:35','2019-09-05 17:02:35'),(316,'SL','SL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(317,'XLL','XLL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(318,'1','1','2019-09-05 17:02:35','2019-09-05 17:02:35'),(319,'OSFL','OSFL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(320,'OSFM','OSFM','2019-09-05 17:02:35','2019-09-05 17:02:35'),(321,'OSFW','OSFW','2019-09-05 17:02:35','2019-09-05 17:02:35'),(322,'OSFY','OSFY','2019-09-05 17:02:35','2019-09-05 17:02:35'),(323,'2XS2','2XS2','2019-09-05 17:02:35','2019-09-05 17:02:35'),(324,'L 2\"','L 2\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(325,'M 2\"','M 2\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(326,'S 2\"','S 2\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(327,'XL2\"','XL2\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(328,'XS2\"','XS2\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(329,'14 UK','14 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(330,'Large','Large','2019-09-05 17:02:35','2019-09-05 17:02:35'),(331,'Youth','Youth','2019-09-05 17:02:35','2019-09-05 17:02:35'),(332,'50','50','2019-09-05 17:02:35','2019-09-05 17:02:35'),(333,'52','52','2019-09-05 17:02:35','2019-09-05 17:02:35'),(334,'3133','3133','2019-09-05 17:02:35','2019-09-05 17:02:35'),(335,'3436','3436','2019-09-05 17:02:35','2019-09-05 17:02:35'),(336,'3739','3739','2019-09-05 17:02:35','2019-09-05 17:02:35'),(337,'4042','4042','2019-09-05 17:02:35','2019-09-05 17:02:35'),(338,'4345','4345','2019-09-05 17:02:35','2019-09-05 17:02:35'),(339,'4648','4648','2019-09-05 17:02:35','2019-09-05 17:02:35'),(340,'2XTG','2XTG','2019-09-05 17:02:35','2019-09-05 17:02:35'),(341,'2XTH','2XTH','2019-09-05 17:02:35','2019-09-05 17:02:35'),(342,'3XTG','3XTG','2019-09-05 17:02:35','2019-09-05 17:02:35'),(343,'3XTH','3XTH','2019-09-05 17:02:35','2019-09-05 17:02:35'),(344,'4XTG','4XTG','2019-09-05 17:02:35','2019-09-05 17:02:35'),(345,'4XTH','4XTH','2019-09-05 17:02:35','2019-09-05 17:02:35'),(346,'L/G','L/G','2019-09-05 17:02:35','2019-09-05 17:02:35'),(347,'LTGH','LTGH','2019-09-05 17:02:35','2019-09-05 17:02:35'),(348,'M/M','M/M','2019-09-05 17:02:35','2019-09-05 17:02:35'),(349,'MTMH','MTMH','2019-09-05 17:02:35','2019-09-05 17:02:35'),(350,'S/P','S/P','2019-09-05 17:02:35','2019-09-05 17:02:35'),(351,'STPH','STPH','2019-09-05 17:02:35','2019-09-05 17:02:35'),(352,'XLTG','XLTG','2019-09-05 17:02:35','2019-09-05 17:02:35'),(353,'XSTP','XSTP','2019-09-05 17:02:35','2019-09-05 17:02:35'),(354,'XTGH','XTGH','2019-09-05 17:02:35','2019-09-05 17:02:35'),(355,'18 UK','18 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(356,'3942','3942','2019-09-05 17:02:35','2019-09-05 17:02:35'),(357,'4346','4346','2019-09-05 17:02:35','2019-09-05 17:02:35'),(358,'2730','2730','2019-09-05 17:02:35','2019-09-05 17:02:35'),(359,'3134','3134','2019-09-05 17:02:35','2019-09-05 17:02:35'),(360,'3538','3538','2019-09-05 17:02:35','2019-09-05 17:02:35'),(361,'OSFC','OSFC','2019-09-05 17:02:35','2019-09-05 17:02:35'),(362,'1922','1922','2019-09-05 17:02:35','2019-09-05 17:02:35'),(363,'2326','2326','2019-09-05 17:02:35','2019-09-05 17:02:35'),(364,'4750','4750','2019-09-05 17:02:35','2019-09-05 17:02:35'),(365,'5154','5154','2019-09-05 17:02:35','2019-09-05 17:02:35'),(366,'18','18','2019-09-05 17:02:35','2019-09-05 17:02:35'),(367,'19','19','2019-09-05 17:02:35','2019-09-05 17:02:35'),(368,'20','20','2019-09-05 17:02:35','2019-09-05 17:02:35'),(369,'21','21','2019-09-05 17:02:35','2019-09-05 17:02:35'),(370,'22','22','2019-09-05 17:02:35','2019-09-05 17:02:35'),(371,'23','23','2019-09-05 17:02:35','2019-09-05 17:02:35'),(372,'24','24','2019-09-05 17:02:35','2019-09-05 17:02:35'),(373,'25','25','2019-09-05 17:02:35','2019-09-05 17:02:35'),(374,'3XS','3XS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(375,'LAB','LAB','2019-09-05 17:02:35','2019-09-05 17:02:35'),(376,'LCD','LCD','2019-09-05 17:02:35','2019-09-05 17:02:35'),(377,'MAB','MAB','2019-09-05 17:02:35','2019-09-05 17:02:35'),(378,'MCD','MCD','2019-09-05 17:02:35','2019-09-05 17:02:35'),(379,'SAB','SAB','2019-09-05 17:02:35','2019-09-05 17:02:35'),(380,'SCD','SCD','2019-09-05 17:02:35','2019-09-05 17:02:35'),(381,'XSAB','XSAB','2019-09-05 17:02:35','2019-09-05 17:02:35'),(382,'XSCD','XSCD','2019-09-05 17:02:35','2019-09-05 17:02:35'),(383,'3943','3943','2019-09-05 17:02:35','2019-09-05 17:02:35'),(384,'4449','4449','2019-09-05 17:02:35','2019-09-05 17:02:35'),(385,'34L','34L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(386,'36L','36L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(387,'19 UK','19 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(388,'20 UK','20 UK','2019-09-05 17:02:35','2019-09-05 17:02:35'),(389,'Kids','TRẺ EM','2019-09-05 17:02:35','2021-07-03 09:57:07'),(390,'3K','3K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(391,'4K','4K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(392,'5K','5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(393,'6K','6K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(394,'7K','7K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(395,'8K','8K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(396,'9K','9K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(397,'5.5K','5.5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(398,'6.5K','6.5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(399,'7.5K','7.5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(400,'8.5K','8.5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(401,'9.5K','9.5K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(402,'L/XL','L/XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(403,'S/M','S/M','2019-09-05 17:02:35','2019-09-05 17:02:35'),(404,'1K','1K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(405,'2K','2K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(406,'5XL','5XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(407,'0K','0K','2019-09-05 17:02:35','2019-09-05 17:02:35'),(408,'L 7\"','L 7\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(409,'L 9\"','L 9\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(410,'M 7\"','M 7\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(411,'M 9\"','M 9\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(412,'S 7\"','S 7\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(413,'S 9\"','S 9\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(414,'XL7\"','XL7\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(415,'XL9\"','XL9\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(416,'XS7\"','XS7\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(417,'XS9\"','XS9\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(418,'54','54','2019-09-05 17:02:35','2019-09-05 17:02:35'),(419,'56','56','2019-09-05 17:02:35','2019-09-05 17:02:35'),(420,'58','58','2019-09-05 17:02:35','2019-09-05 17:02:35'),(421,'60','60','2019-09-05 17:02:35','2019-09-05 17:02:35'),(422,'62','62','2019-09-05 17:02:35','2019-09-05 17:02:35'),(423,'4XLL','4XLL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(424,'2330','2330','2019-09-05 17:02:35','2019-09-05 17:02:35'),(425,'2430','2430','2019-09-05 17:02:35','2019-09-05 17:02:35'),(426,'2530','2530','2019-09-05 17:02:35','2019-09-05 17:02:35'),(427,'2630','2630','2019-09-05 17:02:35','2019-09-05 17:02:35'),(428,'2830','2830','2019-09-05 17:02:35','2019-09-05 17:02:35'),(429,'2832','2832','2019-09-05 17:02:35','2019-09-05 17:02:35'),(430,'2834','2834','2019-09-05 17:02:35','2019-09-05 17:02:35'),(431,'2836','2836','2019-09-05 17:02:35','2019-09-05 17:02:35'),(432,'2930','2930','2019-09-05 17:02:35','2019-09-05 17:02:35'),(433,'2932','2932','2019-09-05 17:02:35','2019-09-05 17:02:35'),(434,'2934','2934','2019-09-05 17:02:35','2019-09-05 17:02:35'),(435,'2936','2936','2019-09-05 17:02:35','2019-09-05 17:02:35'),(436,'3030','3030','2019-09-05 17:02:35','2019-09-05 17:02:35'),(437,'3032','3032','2019-09-05 17:02:35','2019-09-05 17:02:35'),(438,'3034','3034','2019-09-05 17:02:35','2019-09-05 17:02:35'),(439,'3036','3036','2019-09-05 17:02:35','2019-09-05 17:02:35'),(440,'3130','3130','2019-09-05 17:02:35','2019-09-05 17:02:35'),(441,'3132','3132','2019-09-05 17:02:35','2019-09-05 17:02:35'),(442,'3136','3136','2019-09-05 17:02:35','2019-09-05 17:02:35'),(443,'3230','3230','2019-09-05 17:02:35','2019-09-05 17:02:35'),(444,'3232','3232','2019-09-05 17:02:35','2019-09-05 17:02:35'),(445,'3234','3234','2019-09-05 17:02:35','2019-09-05 17:02:35'),(446,'3236','3236','2019-09-05 17:02:35','2019-09-05 17:02:35'),(447,'3330','3330','2019-09-05 17:02:35','2019-09-05 17:02:35'),(448,'3332','3332','2019-09-05 17:02:35','2019-09-05 17:02:35'),(449,'3334','3334','2019-09-05 17:02:35','2019-09-05 17:02:35'),(450,'3336','3336','2019-09-05 17:02:35','2019-09-05 17:02:35'),(451,'3430','3430','2019-09-05 17:02:35','2019-09-05 17:02:35'),(452,'3432','3432','2019-09-05 17:02:35','2019-09-05 17:02:35'),(453,'3434','3434','2019-09-05 17:02:35','2019-09-05 17:02:35'),(454,'3630','3630','2019-09-05 17:02:35','2019-09-05 17:02:35'),(455,'3632','3632','2019-09-05 17:02:35','2019-09-05 17:02:35'),(456,'3634','3634','2019-09-05 17:02:35','2019-09-05 17:02:35'),(457,'3636','3636','2019-09-05 17:02:35','2019-09-05 17:02:35'),(458,'3830','3830','2019-09-05 17:02:35','2019-09-05 17:02:35'),(459,'3832','3832','2019-09-05 17:02:35','2019-09-05 17:02:35'),(460,'3834','3834','2019-09-05 17:02:35','2019-09-05 17:02:35'),(461,'3XLS','3XLS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(462,'Toddlers','Toddlers','2019-09-05 17:02:35','2019-09-05 17:02:35'),(463,'4951','4951','2019-09-05 17:02:35','2019-09-05 17:02:35'),(464,'2XL5','2XL5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(465,'L 5\"','L 5\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(466,'M 5\"','M 5\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(467,'S 5\"','S 5\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(468,'XL5\"','XL5\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(469,'XS5\"','XS5\"','2019-09-05 17:02:35','2019-09-05 17:02:35'),(470,'J2XO','J2XO','2019-09-05 17:02:35','2019-09-05 17:02:35'),(471,'2XL7','2XL7','2019-09-05 17:02:35','2019-09-05 17:02:35'),(472,'2XL9','2XL9','2019-09-05 17:02:35','2019-09-05 17:02:35'),(473,'2XO','2XO','2019-09-05 17:02:35','2019-09-05 17:02:35'),(474,'4XO','4XO','2019-09-05 17:02:35','2019-09-05 17:02:35'),(475,'6XO','6XO','2019-09-05 17:02:35','2019-09-05 17:02:35'),(476,'O','O','2019-09-05 17:02:35','2019-09-05 17:02:35'),(477,'3530','3530','2019-09-05 17:02:35','2019-09-05 17:02:35'),(478,'3532','3532','2019-09-05 17:02:35','2019-09-05 17:02:35'),(479,'3534','3534','2019-09-05 17:02:35','2019-09-05 17:02:35'),(480,'4030','4030','2019-09-05 17:02:35','2019-09-05 17:02:35'),(481,'4032','4032','2019-09-05 17:02:35','2019-09-05 17:02:35'),(482,'4034','4034','2019-09-05 17:02:35','2019-09-05 17:02:35'),(483,'4230','4230','2019-09-05 17:02:35','2019-09-05 17:02:35'),(484,'4232','4232','2019-09-05 17:02:35','2019-09-05 17:02:35'),(485,'J/2XO','J/2XO','2019-09-05 17:02:35','2019-09-05 17:02:35'),(486,'FUTS','FUTS','2019-09-05 17:02:35','2019-09-05 17:02:35'),(487,'OSFT','OSFT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(488,'A4XL','A4XL','2019-09-05 17:02:35','2019-09-05 17:02:35'),(489,'3XO','3XO','2019-09-05 17:02:35','2019-09-05 17:02:35'),(490,'XO','XO','2019-09-05 17:02:35','2019-09-05 17:02:35'),(491,'2XSA','2XSA','2019-09-05 17:02:35','2019-09-05 17:02:35'),(492,'2XSC','2XSC','2019-09-05 17:02:35','2019-09-05 17:02:35'),(493,'XLAB','XLAB','2019-09-05 17:02:35','2019-09-05 17:02:35'),(494,'XLCD','XLCD','2019-09-05 17:02:35','2019-09-05 17:02:35'),(495,'OSFB','OSFB','2019-09-05 17:02:35','2019-09-05 17:02:35'),(496,'2','2','2019-09-05 17:02:35','2019-09-05 17:02:35'),(497,'2.5','2.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(498,'5XLT','5XLT','2019-09-05 17:02:35','2019-09-05 17:02:35'),(499,'1-2.5','1-2.5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(500,'2.5-5','2.5-5','2019-09-05 17:02:35','2019-09-05 17:02:35'),(501,'9k-12k','9k-12k','2019-09-05 17:02:35','2019-09-05 17:02:35'),(502,'OSFA','OSFA','2019-09-05 17:02:35','2019-09-05 17:02:35'),(503,'10L','10L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(504,'10S','10S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(505,'11L','11L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(506,'11S','11S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(507,'12L','12L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(508,'12S','12S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(509,'13','13','2019-09-05 17:02:35','2019-09-05 17:02:35'),(510,'13S','13S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(511,'14','14','2019-09-05 17:02:35','2019-09-05 17:02:35'),(512,'2L','2L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(513,'2S','2S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(514,'3L','3L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(515,'3S','3S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(516,'4L','4L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(517,'4S','4S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(518,'5L','5L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(519,'5S','5S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(520,'6L','6L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(521,'6S','6S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(522,'7L','7L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(523,'7S','7S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(524,'8L','8L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(525,'8S','8S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(526,'9L','9L','2019-09-05 17:02:35','2019-09-05 17:02:35'),(527,'9S','9S','2019-09-05 17:02:35','2019-09-05 17:02:35'),(639,'Long','Long','2019-09-05 17:02:44','2019-09-05 17:02:44'),(640,'R','R','2019-09-05 17:02:44','2019-09-05 17:02:44'),(641,'35','35','2019-09-05 17:02:44','2019-09-05 17:02:44'),(642,'37','37','2019-09-05 17:02:44','2019-09-05 17:02:44'),(643,'39','39','2019-09-05 17:02:44','2019-09-05 17:02:44'),(644,'Compact','Compact','2019-09-05 17:02:44','2019-09-05 17:02:44'),(645,'41','41','2019-09-05 17:02:44','2019-09-05 17:02:44'),(646,'Short','Short','2019-09-05 17:02:44','2019-09-05 17:02:44'),(647,'43','43','2019-09-05 17:02:44','2019-09-05 17:02:44'),(648,'45','45','2019-09-05 17:02:44','2019-09-05 17:02:44'),(649,'100','100','2019-09-05 17:02:44','2019-09-05 17:02:44'),(650,'105','105','2019-09-05 17:02:44','2019-09-05 17:02:44'),(651,'115','115','2019-09-05 17:02:44','2019-09-05 17:02:44'),(652,'120','120','2019-09-05 17:02:44','2019-09-05 17:02:44'),(653,'125','125','2019-09-05 17:02:44','2019-09-05 17:02:44'),(654,'Regular','Regular','2019-09-05 17:02:53','2019-09-05 17:02:53'),(655,'26','26','2019-09-05 17:02:53','2019-09-05 17:02:53'),(656,'27','27','2019-09-05 17:02:53','2019-09-05 17:02:53'),(657,'29','29','2019-09-05 17:02:53','2019-09-05 17:02:53'),(658,'16','16','2019-09-05 17:02:53','2019-09-05 17:02:53'),(659,'17','17','2019-09-05 17:02:53','2019-09-05 17:02:53'),(660,'31','31','2019-09-05 17:02:53','2019-09-05 17:02:53'),(661,'XXS','XXS','2019-09-05 17:02:53','2019-09-05 17:02:53'),(662,'BASKETBALL','BÓNG','2021-06-23 16:56:37','2021-06-23 16:56:37'),(663,'WOMEN','NỮ','2021-06-23 16:56:37','2021-06-23 16:56:37'),(664,'MEN','NAM','2021-06-23 16:56:37','2021-06-23 16:56:37'),(665,'SWIMMING','BƠI LỘI','2021-06-23 16:56:37','2021-06-23 16:56:37'),(666,'FOOTBALL','BÓNG ĐÁ','2021-06-23 16:56:37','2021-06-23 16:56:37'),(667,'TRAINING','TẬP LUYỆN','2021-06-23 16:56:37','2021-06-23 16:56:37'),(668,'GOLF','ĐÁNH GÔN','2021-06-23 16:56:37','2021-06-23 16:56:37'),(669,'RUNNING','CHẠY','2021-06-23 16:56:37','2021-06-23 16:56:37'),(671,'ACCESSORIES','PHỤ KIỆN','2021-06-23 16:57:02','2021-06-23 16:57:02'),(672,'CLOTHING','QUẦN ÁO','2021-06-23 16:57:02','2021-06-23 16:57:02'),(673,'SHOES','GIÀY','2021-06-23 16:57:02','2021-06-23 16:57:02'),(674,'TENNIS','QUẦN VỢT','2021-06-23 16:57:02','2021-06-23 16:57:02'),(675,'LIFESTYLE','PHONG CÁCH SỐNG','2021-06-23 16:57:02','2021-06-23 16:57:02'),(676,'BABY','SƠ SINH','2021-06-23 16:57:02','2021-06-23 16:57:02'),(677,'UV PROTECTION','CHỐNG UV','2021-06-23 16:57:02','2021-06-23 16:57:02'),(678,'TOPS','ÁO','2021-06-23 16:57:02','2021-06-23 16:57:02'),(679,'SPORT UTILITY WEAR','ĐỒ THỂ THAO','2021-06-23 16:57:02','2021-06-23 16:57:02'),(680,'OUTERWEAR','ÁO KHOÁC','2021-06-23 16:57:02','2021-06-23 16:57:02'),(681,'MATERNITY','ĐỒ BẦU','2021-06-23 16:57:02','2021-06-23 16:57:02'),(682,'LOUNGEWEAR','THỜI TRANG Ở NHÀ','2021-06-23 16:57:02','2021-06-23 16:57:02'),(683,'LINEN COLLECTION','ĐỒ LEN','2021-06-23 16:57:02','2021-06-23 16:57:02'),(684,'INNERWEAR','ĐỒ LÓT','2021-06-23 16:57:02','2021-06-23 16:57:02'),(685,'HEATTECH','ĐỒ GIỮ NHIỆT','2021-06-23 16:57:02','2021-07-03 10:01:20'),(686,'BOTTOMS','QUẦN / CHÂN VÁY','2021-06-23 16:57:02','2021-06-23 16:57:02'),(687,'DRESSES & JUMPSUITS','ĐẦM / JUMPSUITS','2021-06-23 16:57:02','2021-06-23 16:57:02'),(688,'AIRISM','AIRISM','2021-06-23 16:57:02','2021-06-23 16:57:02'),(689,'TODDLER (6-18 MONTHS)','SƠ SINH (6-18 THÁNG)','2021-06-23 16:57:02','2021-06-23 16:57:02'),(690,'NEWBORN (0-6 MONTHS)','SƠ SINH (0-6 THÁNG)','2021-06-23 16:57:02','2021-06-23 16:57:02'),(691,'ATHLETICS','THỂ THAO','2021-07-03 10:01:20','2021-07-03 10:03:18'),(692,'SPORT INSPIRED','CẢM HỨNG','2021-07-03 10:01:20','2021-07-03 10:03:18'),(693,'BAGS','TÚI XÁCH','2021-07-07 10:10:11','2021-07-07 10:10:11');
/*!40000 ALTER TABLE `key_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_history`
--

DROP TABLE IF EXISTS `price_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `price_history` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `item_detail_id` bigint NOT NULL,
  `old_price` decimal(19,2) DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `special_price` decimal(19,2) DEFAULT NULL,
  `old_price_n` decimal(19,2) DEFAULT NULL,
  `price_n` decimal(19,2) DEFAULT NULL,
  `special_price_n` decimal(19,2) DEFAULT NULL,
  `uuid` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_detail_id`),
  CONSTRAINT `fk_price_history_item_country_detail1` FOREIGN KEY (`item_detail_id`) REFERENCES `item_country_detail` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_history`
--

LOCK TABLES `price_history` WRITE;
/*!40000 ALTER TABLE `price_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_history` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `crawler` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ntanh`@`%`*/ /*!50003 TRIGGER `price_history_BEFORE_INSERT` BEFORE INSERT ON `price_history` FOR EACH ROW BEGIN
	IF new.uuid IS NULL
	THEN
		SET new.uuid = REPLACE(UUID(),'-','');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `crawler` CHARACTER SET utf8mb3 COLLATE utf8_general_ci ;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `username` varchar(16) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_category`
--

DROP TABLE IF EXISTS `wp_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_id` bigint NOT NULL,
  `category_id` bigint NOT NULL,
  `wp_category_id` bigint DEFAULT NULL,
  `wp_parent_category_id` bigint DEFAULT NULL,
  `wp_category` json DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_category`
--

LOCK TABLES `wp_category` WRITE;
/*!40000 ALTER TABLE `wp_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_image`
--

DROP TABLE IF EXISTS `wp_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_image` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` text,
  `image` json DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `image-image-unq` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_image`
--

LOCK TABLES `wp_image` WRITE;
/*!40000 ALTER TABLE `wp_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_item`
--

DROP TABLE IF EXISTS `wp_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_item` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_id` bigint NOT NULL,
  `item_id` bigint NOT NULL,
  `wp_product_id` varchar(45) NOT NULL,
  `images` json DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `removed_in_wp` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_item`
--

LOCK TABLES `wp_item` WRITE;
/*!40000 ALTER TABLE `wp_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_item_detail`
--

DROP TABLE IF EXISTS `wp_item_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_item_detail` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `wp_item_id` bigint NOT NULL,
  `brand_id` bigint NOT NULL,
  `item_detail_id` bigint NOT NULL,
  `wp_product_variation_id` varchar(45) NOT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `removed_in_wp` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`wp_product_variation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_item_detail`
--

LOCK TABLES `wp_item_detail` WRITE;
/*!40000 ALTER TABLE `wp_item_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_item_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_item_image`
--

DROP TABLE IF EXISTS `wp_item_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_item_image` (
  `wp_item_id` bigint NOT NULL,
  `wp_image_id` bigint NOT NULL,
  PRIMARY KEY (`wp_item_id`,`wp_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_item_image`
--

LOCK TABLES `wp_item_image` WRITE;
/*!40000 ALTER TABLE `wp_item_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_item_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'crawler'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-07 21:39:30
