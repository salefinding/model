SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE `crawler`.`item_stock`;
TRUNCATE `crawler`.`item_image`;
TRUNCATE `crawler`.`price_history`;
TRUNCATE `crawler`.`item_country_detail`;
TRUNCATE `crawler`.`item`;
TRUNCATE `crawler`.`category_crawler`;
TRUNCATE `crawler`.`wpCategory`;
TRUNCATE `crawler`.`error_summary`;
TRUNCATE `crawler`.`crawling_data`;

SET FOREIGN_KEY_CHECKS = 1;

SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE `api`.`item_stock`;
TRUNCATE `api`.`item_image`;
TRUNCATE `api`.`item_country_detail`;
TRUNCATE `api`.`item`;
TRUNCATE `api`.`wpCategory`;

SET FOREIGN_KEY_CHECKS = 1;
