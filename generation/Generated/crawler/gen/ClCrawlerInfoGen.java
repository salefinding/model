package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * A class which represents the crawler_info table in the crawler Database.
 */
@MappedSuperclass
@Getter
@Setter
public class ClCrawlerInfoGen extends Model {

    @Id
    @Column(name = "id")
    protected Long id;
    public static final String IdColumn = "id";

    @Column(name = "crawler_class", nullable = false, length = 65535)
    protected String crawlerClass;
    public static final String CrawlerClassColumn = "crawlerClass";

    @Column(name = "crawler_factory_class", nullable = false, length = 65535)
    protected String crawlerFactoryClass;
    public static final String CrawlerFactoryClassColumn = "crawlerFactoryClass";

    @Column(name = "parser_class", nullable = false, length = 65535)
    protected String parserClass;
    public static final String ParserClassColumn = "parserClass";

    @Column(name = "category_parser_class", nullable = false, length = 65535)
    protected String categoryParserClass;
    public static final String CategoryParserClassColumn = "categoryParserClass";

    @Column(name = "filter", nullable = false, length = 400)
    protected String filter;
    public static final String FilterColumn = "filter";

    @Column(name = "politeness_delay", nullable = false)
    protected Integer politenessDelay;
    public static final String PolitenessDelayColumn = "politenessDelay";

    @Column(name = "uuid", length = 32)
    protected String uuid;
    public static final String UuidColumn = "uuid";

    @Column(name = "created_at", nullable = false)
    @CreatedTimestamp
    protected java.sql.Timestamp createdAt;
    public static final String CreatedAtColumn = "createdAt";

    @Column(name = "updated_at", nullable = false)
    @UpdatedTimestamp
    protected java.sql.Timestamp updatedAt;
    public static final String UpdatedAtColumn = "updatedAt";

}
