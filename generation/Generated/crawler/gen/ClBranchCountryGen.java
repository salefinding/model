package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * A class which represents the brand_country table in the crawler Database.
 */
@MappedSuperclass
@Getter
@Setter
public class ClBrandCountryGen extends Model {

    @Id
    @Column(name = "id")
    protected Long id;
    public static final String IdColumn = "id";

    @Column(name = "brand_id", nullable = false)
    protected Long brandId;
    public static final String BrandIdColumn = "brandId";

    @Column(name = "country_id", nullable = false)
    protected Long countryId;
    public static final String CountryIdColumn = "countryId";

    @Column(name = "group_id", nullable = false)
    protected Integer groupId;
    public static final String GroupIdColumn = "groupId";

    @Column(name = "crawler_info_id", nullable = false)
    protected Long crawlerInfoId;
    public static final String CrawlerInfoIdColumn = "crawlerInfoId";

    @Column(name = "crawler_class", nullable = false, length = 65535)
    protected String crawlerClass;
    public static final String CrawlerClassColumn = "crawlerClass";

    @Column(name = "parser_class", nullable = false, length = 65535)
    protected String parserClass;
    public static final String ParserClassColumn = "parserClass";

    @Column(name = "category_parser_class", nullable = false, length = 65535)
    protected String categoryParserClass;
    public static final String CategoryParserClassColumn = "categoryParserClass";

    @Column(name = "crawler_factory_class", nullable = false, length = 65535)
    protected String crawlerFactoryClass;
    public static final String CrawlerFactoryClassColumn = "crawlerFactoryClass";

    @Column(name = "start_url", nullable = false, length = 65535)
    protected String startUrl;
    public static final String StartUrlColumn = "startUrl";

    @Column(name = "check_stock_api_uri", length = 65535)
    protected String checkStockApiUri;
    public static final String CheckStockApiUriColumn = "checkStockApiUri";

    @Column(name = "filter", nullable = false, length = 65535)
    protected String filter;
    public static final String FilterColumn = "filter";

    @Column(name = "politeness_delay", nullable = false)
    protected Integer politenessDelay;
    public static final String PolitenessDelayColumn = "politenessDelay";

    @Column(name = "number_of_thread", nullable = false)
    protected Integer numberOfThread;
    public static final String NumberOfThreadColumn = "numberOfThread";

    @Column(name = "last_crawling_time", nullable = false)
    protected Long lastCrawlingTime;
    public static final String LastCrawlingTimeColumn = "lastCrawlingTime";

    @Column(name = "last_crawling_at")
    protected java.sql.Timestamp lastCrawlingAt;
    public static final String LastCrawlingAtColumn = "lastCrawlingAt";

    @Column(name = "use_selenium", nullable = false)
    protected Boolean useSelenium;
    public static final String UseSeleniumColumn = "useSelenium";

    @Column(name = "enable", nullable = false)
    protected Boolean enable;
    public static final String EnableColumn = "enable";

    @Column(name = "uuid", length = 32)
    protected String uuid;
    public static final String UuidColumn = "uuid";

    @Column(name = "created_at", nullable = false)
    @CreatedTimestamp
    protected java.sql.Timestamp createdAt;
    public static final String CreatedAtColumn = "createdAt";

    @Column(name = "updated_at", nullable = false)
    @UpdatedTimestamp
    protected java.sql.Timestamp updatedAt;
    public static final String UpdatedAtColumn = "updatedAt";

}
