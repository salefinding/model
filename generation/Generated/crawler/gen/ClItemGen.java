package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * A class which represents the item table in the crawler Database.
 */
@MappedSuperclass
@Getter
@Setter
public class ClItemGen extends Model {

    @Id
    @Column(name = "id")
    protected Long id;
    public static final String IdColumn = "id";

    @Column(name = "brand_id", nullable = false)
    protected Long brandId;
    public static final String BrandIdColumn = "brandId";

    @Column(name = "code", nullable = false, length = 200)
    protected String code;
    public static final String CodeColumn = "code";

    @Column(name = "name", nullable = false, length = 200)
    protected String name;
    public static final String NameColumn = "name";

    @Column(name = "uuid", length = 32)
    protected String uuid;
    public static final String UuidColumn = "uuid";

    @Column(name = "created_at", nullable = false)
    @CreatedTimestamp
    protected java.sql.Timestamp createdAt;
    public static final String CreatedAtColumn = "createdAt";

    @Column(name = "updated_at", nullable = false)
    @UpdatedTimestamp
    protected java.sql.Timestamp updatedAt;
    public static final String UpdatedAtColumn = "updatedAt";

}
