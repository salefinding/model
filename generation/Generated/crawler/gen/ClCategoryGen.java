package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * A class which represents the category table in the crawler Database.
 */
@MappedSuperclass
@Getter
@Setter
public class ClCategoryGen extends Model {

    @Id
    @Column(name = "id")
    protected Long id;
    public static final String IdColumn = "id";

    @Column(name = "brand_id", nullable = false)
    protected Long brandId;
    public static final String BrandIdColumn = "brandId";

    @Column(name = "cat1", length = 200)
    protected String cat1;
    public static final String Cat1Column = "cat1";

    @Column(name = "cat2", length = 200)
    protected String cat2;
    public static final String Cat2Column = "cat2";

    @Column(name = "cat3", length = 200)
    protected String cat3;
    public static final String Cat3Column = "cat3";

    @Column(name = "cat4", length = 200)
    protected String cat4;
    public static final String Cat4Column = "cat4";

    @Column(name = "cat5", length = 200)
    protected String cat5;
    public static final String Cat5Column = "cat5";

    @Column(name = "available", nullable = false)
    protected Boolean available;
    public static final String AvailableColumn = "available";

    @Column(name = "uuid", length = 32)
    protected String uuid;
    public static final String UuidColumn = "uuid";

    @Column(name = "created_at", nullable = false)
    @CreatedTimestamp
    protected java.sql.Timestamp createdAt;
    public static final String CreatedAtColumn = "createdAt";

    @Column(name = "updated_at", nullable = false)
    @UpdatedTimestamp
    protected java.sql.Timestamp updatedAt;
    public static final String UpdatedAtColumn = "updatedAt";

}
