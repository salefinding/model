package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * A class which represents the price_history table in the crawler Database.
 */
@MappedSuperclass
@Getter
@Setter
public class ClPriceHistoryGen extends Model {

    @Id
    @Column(name = "id")
    protected Long id;
    public static final String IdColumn = "id";

    @Column(name = "item_detail_id", nullable = false)
    protected Long itemDetailId;
    public static final String ItemDetailIdColumn = "itemDetailId";

    @Column(name = "old_price")
    protected java.math.BigDecimal oldPrice;
    public static final String OldPriceColumn = "oldPrice";

    @Column(name = "price")
    protected java.math.BigDecimal price;
    public static final String PriceColumn = "price";

    @Column(name = "special_price")
    protected java.math.BigDecimal specialPrice;
    public static final String SpecialPriceColumn = "specialPrice";

    @Column(name = "old_price_n")
    protected java.math.BigDecimal oldPriceN;
    public static final String OldPriceNColumn = "oldPriceN";

    @Column(name = "price_n")
    protected java.math.BigDecimal priceN;
    public static final String PriceNColumn = "priceN";

    @Column(name = "special_price_n")
    protected java.math.BigDecimal specialPriceN;
    public static final String SpecialPriceNColumn = "specialPriceN";

    @Column(name = "uuid", length = 32)
    protected String uuid;
    public static final String UuidColumn = "uuid";

    @Column(name = "created_at", nullable = false)
    @CreatedTimestamp
    protected java.sql.Timestamp createdAt;
    public static final String CreatedAtColumn = "createdAt";

}
