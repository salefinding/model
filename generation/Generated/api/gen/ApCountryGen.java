package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * A class which represents the country table in the salefinding_api Database.
 */
@MappedSuperclass
@Getter
@Setter
public class ApCountryGen extends Model {

    @Id
    @Column(name = "id")
    protected Long id;
    public static final String IdColumn = "id";

    @Column(name = "name", length = 200)
    protected String name;
    public static final String NameColumn = "name";

    @Column(name = "short_code", length = 45)
    protected String shortCode;
    public static final String ShortCodeColumn = "shortCode";

    @Column(name = "currency_id", nullable = false)
    protected Long currencyId;
    public static final String CurrencyIdColumn = "currencyId";

    @Column(name = "uuid", length = 32)
    protected String uuid;
    public static final String UuidColumn = "uuid";

    @Column(name = "created_at", nullable = false)
    @CreatedTimestamp
    protected java.sql.Timestamp createdAt;
    public static final String CreatedAtColumn = "createdAt";

    @Column(name = "updated_at", nullable = false)
    @UpdatedTimestamp
    protected java.sql.Timestamp updatedAt;
    public static final String UpdatedAtColumn = "updatedAt";

}
