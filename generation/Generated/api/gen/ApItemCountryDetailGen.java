package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * A class which represents the item_country_detail table in the salefinding_api Database.
 */
@MappedSuperclass
@Getter
@Setter
public class ApItemCountryDetailGen extends Model {

    @Id
    @Column(name = "id")
    protected Long id;
    public static final String IdColumn = "id";

    @Column(name = "item_id", nullable = false)
    protected Long itemId;
    public static final String ItemIdColumn = "itemId";

    @Column(name = "country_id", nullable = false)
    protected Long countryId;
    public static final String CountryIdColumn = "countryId";

    @Column(name = "category_id", nullable = false)
    protected Long categoryId;
    public static final String CategoryIdColumn = "categoryId";

    @Column(name = "code", nullable = false, length = 200)
    protected String code;
    public static final String CodeColumn = "code";

    @Column(name = "name", nullable = false, length = 200)
    protected String name;
    public static final String NameColumn = "name";

    @Column(name = "image", nullable = false, length = 65535)
    protected String image;
    public static final String ImageColumn = "image";

    @Column(name = "old_price")
    protected java.math.BigDecimal oldPrice;
    public static final String OldPriceColumn = "oldPrice";

    @Column(name = "price", nullable = false)
    protected java.math.BigDecimal price;
    public static final String PriceColumn = "price";

    @Column(name = "special_price")
    protected java.math.BigDecimal specialPrice;
    public static final String SpecialPriceColumn = "specialPrice";

    @Column(name = "sale_rate", nullable = false)
    protected Integer saleRate;
    public static final String SaleRateColumn = "saleRate";

    @Column(name = "url", nullable = false, length = 65535)
    protected String url;
    public static final String UrlColumn = "url";

    @Column(name = "available", nullable = false)
    protected Boolean available;
    public static final String AvailableColumn = "available";

    @Column(name = "uuid", nullable = false, length = 32)
    protected String uuid;
    public static final String UuidColumn = "uuid";

    @Column(name = "created_at", nullable = false)
    @CreatedTimestamp
    protected java.sql.Timestamp createdAt;
    public static final String CreatedAtColumn = "createdAt";

    @Column(name = "updated_at", nullable = false)
    @UpdatedTimestamp
    protected java.sql.Timestamp updatedAt;
    public static final String UpdatedAtColumn = "updatedAt";

}
