package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * A class which represents the order table in the salefinding_api Database.
 */
@MappedSuperclass
@Getter
@Setter
public class ApOrderGen extends Model {

    @Id
    @Column(name = "id")
    protected Long id;
    public static final String IdColumn = "id";

    @Column(name = "user_id", nullable = false)
    protected Long userId;
    public static final String UserIdColumn = "userId";

    @Column(name = "user_address_id", nullable = false)
    protected Long userAddressId;
    public static final String UserAddressIdColumn = "userAddressId";

    @Column(name = "order_detail", length = 65535)
    protected String orderDetail;
    public static final String OrderDetailColumn = "orderDetail";

    @Column(name = "total_amount")
    protected java.math.BigDecimal totalAmount;
    public static final String TotalAmountColumn = "totalAmount";

    @Column(name = "status", nullable = false)
    protected Integer status;
    public static final String StatusColumn = "status";

    @Column(name = "payment_status", nullable = false)
    protected Integer paymentStatus;
    public static final String PaymentStatusColumn = "paymentStatus";

    @Column(name = "uuid", length = 32)
    protected String uuid;
    public static final String UuidColumn = "uuid";

    @Column(name = "created_at", nullable = false)
    @CreatedTimestamp
    protected java.sql.Timestamp createdAt;
    public static final String CreatedAtColumn = "createdAt";

    @Column(name = "updated_at", nullable = false)
    @UpdatedTimestamp
    protected java.sql.Timestamp updatedAt;
    public static final String UpdatedAtColumn = "updatedAt";

}
