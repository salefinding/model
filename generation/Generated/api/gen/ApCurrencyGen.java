package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * A class which represents the currency table in the salefinding_api Database.
 */
@MappedSuperclass
@Getter
@Setter
public class ApCurrencyGen extends Model {

    @Id
    @Column(name = "id")
    protected Long id;
    public static final String IdColumn = "id";

    @Column(name = "name", length = 200)
    protected String name;
    public static final String NameColumn = "name";

    @Column(name = "short_code", length = 3)
    protected String shortCode;
    public static final String ShortCodeColumn = "shortCode";

    @Column(name = "vnd_exchange_rate")
    protected java.math.BigDecimal vndExchangeRate;
    public static final String VndExchangeRateColumn = "vndExchangeRate";

    @Column(name = "uuid", length = 32)
    protected String uuid;
    public static final String UuidColumn = "uuid";

    @Column(name = "created_at", nullable = false)
    @CreatedTimestamp
    protected java.sql.Timestamp createdAt;
    public static final String CreatedAtColumn = "createdAt";

    @Column(name = "updated_at", nullable = false)
    @UpdatedTimestamp
    protected java.sql.Timestamp updatedAt;
    public static final String UpdatedAtColumn = "updatedAt";

}
