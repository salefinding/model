package com.salefinding.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.TextNode;

import java.io.IOException;

public class SFObjectMapper extends ObjectMapper {

  public SFObjectMapper(boolean useSnakeCase) {
    if (useSnakeCase) {
      this.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    }
    this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    this.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    this.setDefaultPropertyInclusion(
        JsonInclude.Value.construct(JsonInclude.Include.NON_NULL, JsonInclude.Include.NON_NULL));
  }

  public static JsonNode toJsonNode(Object object, boolean useSnakeCase) {
    JsonNode result = null;
    try {
      if (object instanceof JsonNode) {
        result = (JsonNode) object;
      } else {
        ObjectMapper mapper = new SFObjectMapper(useSnakeCase);
        if (object instanceof String) {
          result = mapper.readTree((String) object);
        } else {
          result = mapper.valueToTree(object);
        }
      }
    } catch (Exception ex) {
      System.out.println(
          String.format("Cannot convert %s to json %s", object.toString(), ex.getMessage()));
      if (object instanceof String) {
        result = new TextNode((String) object);
      }
    }
    return result;
  }

  public static <T> T toObject(JsonNode node, Class<T> clazz, boolean useSnakeCase)
      throws JsonProcessingException {
    return new SFObjectMapper(useSnakeCase).treeToValue(node, clazz);
  }

  public static <T> T toObject(String jsonString, Class<T> clazz, boolean usingSnakeCase)
      throws IOException {
    return new SFObjectMapper(usingSnakeCase).readValue(jsonString, clazz);
  }

  public static <T> T toObject(JsonNode node, TypeReference<T> clazz, boolean useSnakeCase)
      throws IOException {
    return new SFObjectMapper(useSnakeCase).readerFor(clazz).readValue(node);
  }

  public static <T> T toObject(JsonNode node, JavaType clazz, boolean useSnakeCase)
      throws IOException {
    return new SFObjectMapper(useSnakeCase).readerFor(clazz).readValue(node);
  }
}
