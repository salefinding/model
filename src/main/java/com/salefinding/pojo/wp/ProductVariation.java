package com.salefinding.pojo.wp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.salefinding.models.crawler.ClItemCountryDetail;
import com.salefinding.models.crawler.ClWpItemDetail;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProductVariation {

  @JsonProperty("id")
  public Long id;

  @JsonProperty("date_created")
  public String dateCreated;

  @JsonProperty("date_created_gmt")
  public String dateCreatedGmt;

  @JsonProperty("date_modified")
  public String dateModified;

  @JsonProperty("date_modified_gmt")
  public String dateModifiedGmt;

  @JsonProperty("description")
  public String description;

  @JsonProperty("permalink")
  public String permalink;

  @EqualsAndHashCode.Include
  @JsonProperty("sku")
  public String sku;

  @JsonProperty("price")
  public String price;

  @JsonProperty("regular_price")
  public String regularPrice;

  @JsonProperty("sale_price")
  public String salePrice;

  @JsonProperty("date_on_sale_from")
  public Object dateOnSaleFrom;

  @JsonProperty("date_on_sale_from_gmt")
  public Object dateOnSaleFromGmt;

  @JsonProperty("date_on_sale_to")
  public Object dateOnSaleTo;

  @JsonProperty("date_on_sale_to_gmt")
  public Object dateOnSaleToGmt;

  @JsonProperty("on_sale")
  public Boolean onSale;

  @JsonProperty("status")
  public String status;

  @JsonProperty("purchasable")
  public Boolean purchasable;

  @JsonProperty("virtual")
  public Boolean virtual;

  @JsonProperty("downloadable")
  public Boolean downloadable;

  @JsonProperty("downloads")
  public List<Object> downloads = null;

  @JsonProperty("download_limit")
  public Integer downloadLimit;

  @JsonProperty("download_expiry")
  public Integer downloadExpiry;

  @JsonProperty("tax_status")
  public String taxStatus;

  @JsonProperty("tax_class")
  public String taxClass;

  @JsonProperty("manage_stock")
  public Boolean manageStock;

  @JsonProperty("stock_quantity")
  public Object stockQuantity;

  @JsonProperty("stock_status")
  public String stockStatus;

  @JsonProperty("backorders")
  public String backorders;

  @JsonProperty("backorders_allowed")
  public Boolean backordersAllowed;

  @JsonProperty("backordered")
  public Boolean backordered;

  @JsonProperty("weight")
  public String weight;

  @JsonProperty("dimensions")
  public Dimensions dimensions;

  @JsonProperty("shipping_class")
  public String shippingClass;

  @JsonProperty("shipping_class_id")
  public Integer shippingClassId;

  @JsonProperty("image")
  public WpImage image;

  @JsonProperty("attributes")
  public List<DefaultAttribute> attributes = null;

  @JsonProperty("menu_order")
  public Integer menuOrder;

  @JsonProperty("meta_data")
  public List<MetaData> metaData = null;

  @JsonIgnore public ClItemCountryDetail clItemDetail;

  @JsonIgnore public ClWpItemDetail clWpItemDetail;

  public ProductVariation(ClItemCountryDetail clItemDetail) {
    this.clItemDetail = clItemDetail;
  }
}
