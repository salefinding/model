package com.salefinding.pojo.wp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
public class MetaData {

  @JsonProperty("id")
  public Long id;

  @JsonProperty("key")
  public String key;

  @JsonProperty("value")
  public String value;
}
