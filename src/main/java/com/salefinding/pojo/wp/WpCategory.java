package com.salefinding.pojo.wp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class WpCategory {

  @JsonProperty("id")
  @EqualsAndHashCode.Include
  public Long id;

  @JsonProperty("name")
  public String name;

  @JsonProperty("slug")
  public String slug;

  @JsonProperty("parent")
  public Long parent;

  @JsonProperty("description")
  public String description;

  @JsonProperty("display")
  public String display;

  @JsonProperty("image")
  public WpImage image;

  @JsonProperty("menu_order")
  public Integer menuOrder;

  @JsonProperty("count")
  public Integer count;

  public WpCategory(String name) {
    this.name = name.toUpperCase();
    this.description = this.name;
    this.slug = this.description.toLowerCase();
    this.display = "products";
  }

  public WpCategory(String name, Long parent, String parentDes) {
    this.name = name.toUpperCase();
    this.parent = parent;
    this.description = parentDes + "-" + this.name;
    this.slug = this.description.toLowerCase();
    this.display = "products";
  }
}
