package com.salefinding.pojo.wp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.salefinding.models.crawler.ClWpImage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class WpImage {

  @JsonProperty("id")
  public Long id;

  @JsonProperty("date_created")
  public String dateCreated;

  @JsonProperty("date_created_gmt")
  public String dateCreatedGmt;

  @JsonProperty("date_modified")
  public String dateModified;

  @JsonProperty("date_modified_gmt")
  public String dateModifiedGmt;

  @JsonProperty("src")
  public String src;

  @EqualsAndHashCode.Include
  @JsonProperty("name")
  public String name;

  @JsonProperty("alt")
  public String alt;

  @JsonIgnore public ClWpImage wpImage;

  public WpImage(String src) {
    if (src != null) {
      if (src.startsWith("//")) {
        src = "https:" + src;
      }
      name = src.substring(src.lastIndexOf('/') + 1);
      alt = name.lastIndexOf('.') > 0 ? name.substring(0, name.lastIndexOf('.')) : name;
    }
    this.src = src;
  }

  public WpImage(String name, String src) {
    this(src);
    this.name = name;
    if (name != null) {
      alt = name.lastIndexOf('.') > 0 ? name.substring(0, name.lastIndexOf('.')) : name;
    }
  }
}
