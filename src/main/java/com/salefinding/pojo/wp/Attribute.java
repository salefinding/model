package com.salefinding.pojo.wp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
public class Attribute extends BaseAttribute {

  @JsonProperty("position")
  public Integer position;

  @JsonProperty("visible")
  public Boolean visible;

  @JsonProperty("variation")
  public Boolean variation;

  @JsonProperty("options")
  public Set<String> options = null;

  public Attribute(
      BaseAttribute baseAttribute,
      Integer position,
      Boolean visible,
      Boolean variation,
      Set<String> options) {
    this.id = baseAttribute.id;
    this.position = position;
    this.visible = visible;
    this.variation = variation;
    this.options = options;
  }
}
