package com.salefinding.pojo.wp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
public class Dimensions {

  @JsonProperty("length")
  public String length;

  @JsonProperty("width")
  public String width;

  @JsonProperty("height")
  public String height;
}
