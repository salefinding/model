package com.salefinding.pojo.wp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
public class DefaultAttribute extends BaseAttribute {

  @JsonProperty("option")
  public String option;

  public DefaultAttribute(BaseAttribute baseAttribute, String option) {
    this.id = baseAttribute.id;
    this.option = option;
  }
}
