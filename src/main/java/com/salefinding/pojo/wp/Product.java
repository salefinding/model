package com.salefinding.pojo.wp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.salefinding.models.crawler.ClWpItem;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Product {

  @JsonProperty("id")
  public Long id;

  @JsonProperty("name")
  public String name;

  @JsonProperty("slug")
  public String slug;

  @JsonProperty("permalink")
  public String permalink;

  @JsonProperty("date_created")
  public String dateCreated;

  @JsonProperty("date_created_gmt")
  public String dateCreatedGmt;

  @JsonProperty("date_modified")
  public String dateModified;

  @JsonProperty("date_modified_gmt")
  public String dateModifiedGmt;

  @JsonProperty("type")
  public String type;

  @JsonProperty("status")
  public String status;

  @JsonProperty("featured")
  public Boolean featured;

  @JsonProperty("catalog_visibility")
  public String catalogVisibility;

  @JsonProperty("description")
  public String description;

  @JsonProperty("short_description")
  public String shortDescription;

  @EqualsAndHashCode.Include
  @JsonProperty("sku")
  public String sku;

  @JsonProperty("price")
  public String price;

  @JsonProperty("regular_price")
  public String regularPrice;

  @JsonProperty("sale_price")
  public String salePrice;

  @JsonProperty("date_on_sale_from")
  public Object dateOnSaleFrom;

  @JsonProperty("date_on_sale_from_gmt")
  public Object dateOnSaleFromGmt;

  @JsonProperty("date_on_sale_to")
  public Object dateOnSaleTo;

  @JsonProperty("date_on_sale_to_gmt")
  public Object dateOnSaleToGmt;

  @JsonProperty("price_html")
  public String priceHtml;

  @JsonProperty("on_sale")
  public Boolean onSale;

  @JsonProperty("purchasable")
  public Boolean purchasable;

  @JsonProperty("total_sales")
  public Integer totalSales;

  @JsonProperty("virtual")
  public Boolean virtual;

  @JsonProperty("downloadable")
  public Boolean downloadable;

  @JsonProperty("downloads")
  public List<Object> downloads = null;

  @JsonProperty("download_limit")
  public Integer downloadLimit;

  @JsonProperty("download_expiry")
  public Integer downloadExpiry;

  @JsonProperty("external_url")
  public String externalUrl;

  @JsonProperty("button_text")
  public String buttonText;

  @JsonProperty("tax_status")
  public String taxStatus;

  @JsonProperty("tax_class")
  public String taxClass;

  @JsonProperty("manage_stock")
  public Boolean manageStock;

  @JsonProperty("stock_quantity")
  public Object stockQuantity;

  @JsonProperty("stock_status")
  public String stockStatus;

  @JsonProperty("backorders")
  public String backorders;

  @JsonProperty("backorders_allowed")
  public Boolean backordersAllowed;

  @JsonProperty("backordered")
  public Boolean backordered;

  @JsonProperty("sold_individually")
  public Boolean soldIndividually;

  @JsonProperty("weight")
  public String weight;

  @JsonProperty("dimensions")
  public Dimensions dimensions;

  @JsonProperty("shipping_required")
  public Boolean shippingRequired;

  @JsonProperty("shipping_taxable")
  public Boolean shippingTaxable;

  @JsonProperty("shipping_class")
  public String shippingClass;

  @JsonProperty("shipping_class_id")
  public Integer shippingClassId;

  @JsonProperty("reviews_allowed")
  public Boolean reviewsAllowed;

  @JsonProperty("average_rating")
  public String averageRating;

  @JsonProperty("rating_count")
  public Integer ratingCount;

  @JsonProperty("related_ids")
  public List<Integer> relatedIds = null;

  @JsonProperty("upsell_ids")
  public List<Object> upsellIds = null;

  @JsonProperty("cross_sell_ids")
  public List<Object> crossSellIds = null;

  @JsonProperty("parent_id")
  public Long parentId;

  @JsonProperty("purchase_note")
  public String purchaseNote;

  @JsonProperty("categories")
  public List<WpCategory> categories = null;

  @JsonProperty("tags")
  public List<Object> tags = null;

  @JsonProperty("images")
  public Set<WpImage> images = null;

  @JsonProperty("attributes")
  public List<Attribute> attributes = null;

  @JsonProperty("default_attributes")
  public List<DefaultAttribute> defaultAttributes = null;

  @JsonIgnore public List<ProductVariation> variations = null;

  @JsonProperty("variations")
  public List<Long> variationIds = null;

  @JsonProperty("grouped_products")
  public List<Object> groupedProducts = null;

  @JsonProperty("menu_order")
  public Integer menuOrder;

  @JsonProperty("meta_data")
  public List<MetaData> metaData = null;

  @JsonIgnore public ClWpItem clWpItem;

  public Product() {
    images = new LinkedHashSet<>();
  }

  public Product(ClWpItem clWpItem) {
    this();
    this.clWpItem = clWpItem;
  }
}
