package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApItemStockGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** A class which represents the crawler_info table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "item_stock")
@Data
public class ApItemStock extends ApItemStockGen {

  public static final String TableName = "item_stock";

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "item_detail_id", insertable = false, updatable = false)
  private ApItemCountryDetail itemCountryDetail;

  public ApItemStock(Long itemDetailId) {
    this.itemDetailId = itemDetailId;
    this.status = true;
  }
}
