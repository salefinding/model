package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApBrandGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/** A class which represents the brand table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "brand")
@Data
public class ApBrand extends ApBrandGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "brand", fetch = FetchType.LAZY)
  private List<ApItem> itemList;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "brand", fetch = FetchType.LAZY)
  private List<ApCategory> categoryList;
}
