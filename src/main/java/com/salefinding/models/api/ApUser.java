package com.salefinding.models.api;

import com.salefinding.models.api.gen.ApUserGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/** A class which represents the user table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "user")
@Data
public class ApUser extends ApUserGen {}
