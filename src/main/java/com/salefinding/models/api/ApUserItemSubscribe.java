package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApUserItemSubscribeGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** A class which represents the user_item_subscribe table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "user_item_subscribe")
@Data
public class ApUserItemSubscribe extends ApUserItemSubscribeGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "user_id", insertable = false, updatable = false)
  private ApUser user;

  public ApUserItemSubscribe() {
    this.status = true;
  }
}
