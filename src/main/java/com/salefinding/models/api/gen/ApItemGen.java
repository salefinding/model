package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the item table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApItemGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "brand_id", nullable = false)
  protected Long brandId;

  public static final String BrandIdColumn = "brandId";

  @Column(name = "category_id", nullable = false)
  protected Long categoryId;

  public static final String CategoryIdColumn = "categoryId";

  @Column(name = "code", nullable = false, length = 200)
  protected String code;

  public static final String CodeColumn = "code";

  @Column(name = "name", nullable = false, length = 200)
  protected String name;

  public static final String NameColumn = "name";

  @Column(name = "uuid", length = 32)
  protected String uuid;

  public static final String UuidColumn = "uuid";

  @Column(name = "max_sale_rate", nullable = false)
  protected Integer maxSaleRate;

  public static final String MaxSaleRateColumn = "maxSaleRate";

  @Column(name = "available", nullable = false)
  protected Boolean available;

  public static final String AvailableColumn = "available";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
