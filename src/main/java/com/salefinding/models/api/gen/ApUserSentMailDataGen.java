package com.salefinding.models.api.gen;

import com.salefinding.models.api.ApItem;
import io.ebean.Model;
import io.ebean.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.List;

/** A class which represents the user_item_subcribe table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApUserSentMailDataGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "user_id", nullable = false)
  @Index(unique = true)
  protected Long userId;

  public static final String UserIdColumn = "userId";

  @Column(name = "item_list", nullable = false)
  @DbJson(storage = DbJsonType.BLOB)
  protected List<ApItem> itemList;

  public static final String ItemListColumn = "itemList";

  @Column(name = "last_sent_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp lastSentAt;

  public static final String LastSentAtColumn = "lastSentAt";

  @Column(name = "created_at")
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at")
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
