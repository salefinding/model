package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the order table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApOrderGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "user_id", nullable = false)
  protected Long userId;

  public static final String UserIdColumn = "userId";

  @Column(name = "user_address_id", nullable = false)
  protected Long userAddressId;

  public static final String UserAddressIdColumn = "userAddressId";

  @Column(name = "order_detail", length = 65535)
  protected String orderDetail;

  public static final String OrderDetailColumn = "orderDetail";

  @Column(name = "total_amount")
  protected java.math.BigDecimal totalAmount;

  public static final String TotalAmountColumn = "totalAmount";

  @Column(name = "status", nullable = false)
  protected Integer status;

  public static final String StatusColumn = "status";

  @Column(name = "payment_status", nullable = false)
  protected Integer paymentStatus;

  public static final String PaymentStatusColumn = "paymentStatus";

  @Column(name = "uuid", length = 32)
  protected String uuid;

  public static final String UuidColumn = "uuid";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
