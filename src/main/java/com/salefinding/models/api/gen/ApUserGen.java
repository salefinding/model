package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.Index;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the user table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApUserGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "email", nullable = false, length = 200)
  @Index(unique = true)
  protected String email;

  public static final String EmailColumn = "email";

  @Column(name = "password", nullable = false, length = 200)
  protected String password;

  public static final String PasswordColumn = "password";

  @Column(name = "name", length = 200)
  protected String name;

  public static final String NameColumn = "name";

  @Column(name = "date_of_birth")
  protected java.sql.Date dateOfBirth;

  public static final String DateOfBirthColumn = "dateOfBirth";

  @Column(name = "total_spent_money")
  protected java.math.BigDecimal totalSpentMoney;

  public static final String TotalSpentMoneyColumn = "totalSpentMoney";

  @Column(name = "is_blocked", nullable = false)
  protected Boolean isBlocked;

  public static final String IsBlockedColumn = "isBlocked";

  @Column(name = "is_active", nullable = false)
  protected Boolean isActive;

  public static final String IsActiveColumn = "isActive";

  @Column(name = "uuid", length = 32)
  protected String uuid;

  public static final String UuidColumn = "uuid";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
