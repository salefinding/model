package com.salefinding.models.api.gen;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the category table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApCategoryGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "brand_id", nullable = false)
  protected Long brandId;

  public static final String BrandIdColumn = "brandId";

  @Column(name = "cat1", length = 200)
  protected String cat1;

  public static final String Cat1Column = "cat1";

  @Column(name = "cat2", length = 200)
  protected String cat2;

  public static final String Cat2Column = "cat2";

  @Column(name = "cat3", length = 200)
  protected String cat3;

  public static final String Cat3Column = "cat3";

  @Column(name = "cat4", length = 200)
  protected String cat4;

  public static final String Cat4Column = "cat4";

  @Column(name = "cat5", length = 200)
  protected String cat5;

  public static final String Cat5Column = "cat5";

  @JsonIgnore
  @Column(name = "available", nullable = false)
  protected Boolean available;

  public static final String AvailableColumn = "available";

  @JsonIgnore
  @Column(name = "uuid", nullable = false, length = 32)
  protected String uuid;

  public static final String UuidColumn = "uuid";

  @JsonIgnore
  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @JsonIgnore
  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
