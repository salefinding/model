package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the user_item_subscribe table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApUserItemSubscribeGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "user_id", nullable = false)
  protected Long userId;

  public static final String UserIdColumn = "userId";

  @Column(name = "entity_type", nullable = false, length = 50)
  protected String entityType;

  public static final String EntityTypeColumn = "entityType";

  @Column(name = "entity_id", nullable = false)
  protected Long entityId;

  public static final String EntityIdColumn = "entityId";

  @Column(name = "status", nullable = false)
  protected Boolean status;

  public static final String StatusColumn = "status";

  @Column(name = "created_at")
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at")
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
