package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the user_address table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApUserAddressGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "user_id", nullable = false)
  protected Long userId;

  public static final String UserIdColumn = "userId";

  @Column(name = "address", length = 500)
  protected String address;

  public static final String AddressColumn = "address";

  @Column(name = "phone")
  protected java.sql.Date phone;

  public static final String PhoneColumn = "phone";

  @Column(name = "country", length = 45)
  protected String country;

  public static final String CountryColumn = "country";

  @Column(name = "is_default", nullable = false)
  protected Boolean isDefault;

  public static final String IsDefaultColumn = "isDefault";

  @Column(name = "uuid", length = 32)
  protected String uuid;

  public static final String UuidColumn = "uuid";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
