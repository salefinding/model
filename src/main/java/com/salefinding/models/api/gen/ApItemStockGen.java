package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the item_stock table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApItemStockGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "item_detail_id", nullable = false)
  protected Long itemDetailId;

  public static final String ItemDetailIdColumn = "itemDetailId";

  @Column(name = "color_code", length = 100)
  protected String colorCode;

  public static final String ColorCodeColumn = "colorCode";

  @Column(name = "size_code", length = 100)
  protected String sizeCode;

  public static final String SizeCodeColumn = "sizeCode";

  @Column(name = "stock", nullable = false)
  protected Integer stock;

  public static final String StockColumn = "stock";

  @Column(name = "status", nullable = false)
  protected Boolean status;

  public static final String StatusColumn = "status";

  @Column(name = "uuid", nullable = false, length = 32)
  protected String uuid;

  public static final String UuidColumn = "uuid";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
