package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the item_country_detail table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApItemCountryDetailGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "item_id", nullable = false)
  protected Long itemId;

  public static final String ItemIdColumn = "itemId";

  @Column(name = "brand_id", nullable = false)
  protected Long brandId;

  public static final String BrandIdColumn = "brandId";

  @Column(name = "country_id", nullable = false)
  protected Long countryId;

  public static final String CountryIdColumn = "countryId";

  @Column(name = "category_id", nullable = false)
  protected Long categoryId;

  public static final String CategoryIdColumn = "categoryId";

  @Column(name = "code", nullable = false, length = 200)
  protected String code;

  public static final String CodeColumn = "code";

  @Column(name = "name", nullable = false, length = 200)
  protected String name;

  public static final String NameColumn = "name";

  @Column(name = "image", nullable = false, length = 65535)
  protected String image;

  public static final String ImageColumn = "image";

  @Column(name = "old_price")
  protected java.math.BigDecimal oldPrice;

  public static final String OldPriceColumn = "oldPrice";

  @Column(name = "price", nullable = false)
  protected java.math.BigDecimal price;

  public static final String PriceColumn = "price";

  @Column(name = "special_price")
  protected java.math.BigDecimal specialPrice;

  public static final String SpecialPriceColumn = "specialPrice";

  @Column(name = "sale_rate", nullable = false)
  protected Integer saleRate;

  public static final String SaleRateColumn = "saleRate";

  @Column(name = "url", nullable = false, length = 65535)
  protected String url;

  public static final String UrlColumn = "url";

  @Column(name = "available", nullable = false)
  protected Boolean available;

  public static final String AvailableColumn = "available";

  @Column(name = "uuid", nullable = false, length = 32)
  protected String uuid;

  public static final String UuidColumn = "uuid";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
