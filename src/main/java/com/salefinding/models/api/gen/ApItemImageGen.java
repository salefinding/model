package com.salefinding.models.api.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the item_image table in the salefinding_api Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ApItemImageGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "item_detail_id", nullable = false)
  protected Long itemDetailId;

  public static final String ItemDetailIdColumn = "itemDetailId";

  @Column(name = "thumb_url", nullable = false, length = 65535)
  protected String thumbUrl;

  public static final String ThumbUrlColumn = "thumbUrl";

  @Column(name = "url", nullable = false, length = 65535)
  protected String url;

  public static final String UrlColumn = "url";

  @Column(name = "description", length = 65535)
  protected String description;

  public static final String DescriptionColumn = "description";

  @Column(name = "uuid", length = 32)
  protected String uuid;

  public static final String UuidColumn = "uuid";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
