package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApCategoryGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** A class which represents the category table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "category")
@Data
public class ApCategory extends ApCategoryGen {

  public static final String TableName = "category";

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "brand_id", insertable = false, updatable = false)
  private ApBrand brand;

  public ApCategory() {
    this.available = true;
  }

  public ApCategory(Long brandId, String cat1, String cat2, String cat3, String cat4, String cat5) {
    this();
    this.brandId = brandId;
    this.cat1 = cat1;
    this.cat2 = cat2;
    this.cat3 = cat3;
    this.cat4 = cat4;
    this.cat5 = cat5;
  }

  public String getCatByIndex(int index) {
    String result = null;
    switch (index) {
      case 1:
        result = cat1;
        break;
      case 2:
        result = cat2;
        break;
      case 3:
        result = cat3;
        break;
      case 4:
        result = cat4;
        break;
      case 5:
        result = cat5;
        break;
    }
    return result;
  }
}
