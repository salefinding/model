package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApItemImageGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** A class which represents the crawler_info table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "item_image")
@Data
public class ApItemImage extends ApItemImageGen {

  public static final String TableName = "item_image";

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "item_detail_id", insertable = false, updatable = false)
  private ApItemCountryDetail itemCountryDetail;

  public ApItemImage(Long itemDetailId) {
    this.itemDetailId = itemDetailId;
  }
}
