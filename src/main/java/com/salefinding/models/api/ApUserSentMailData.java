package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApUserSentMailDataGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.List;

/** A class which represents the user_item_subscribe table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "user_sent_mail_data")
@Data
public class ApUserSentMailData extends ApUserSentMailDataGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "user_id", insertable = false, updatable = false)
  private ApUser user;

  public ApUserSentMailData() {}

  public ApUserSentMailData(Long userId, List<ApItem> itemList, Timestamp lastSentAt) {
    this.userId = userId;
    this.itemList = itemList;
    this.lastSentAt = lastSentAt;
  }
}
