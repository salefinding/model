package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApCountryGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/** A class which represents the country table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "country")
@Data
public class ApCountry extends ApCountryGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToOne
  @JoinColumn(name = "currency_id", insertable = false, updatable = false)
  private ApCurrency currency;
}
