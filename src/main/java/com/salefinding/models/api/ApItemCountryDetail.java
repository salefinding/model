package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApItemCountryDetailGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

/** A class which represents the item table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "item_country_detail")
@Data
public class ApItemCountryDetail extends ApItemCountryDetailGen {

  public static final String TableName = "item_country_detail";

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "item_id", insertable = false, updatable = false)
  private ApItem item;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "country_id", insertable = false, updatable = false)
  private ApCountry country;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "category_id", insertable = false, updatable = false)
  private ApCategory category;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "itemCountryDetail", fetch = FetchType.LAZY)
  private List<ApItemStock> itemStockList;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "itemCountryDetail", fetch = FetchType.LAZY)
  private List<ApItemImage> itemImageList;

  @Transient private String countryName;

  public ApItemCountryDetail(Long itemId, Long brandId, Long countryId) {
    this.saleRate = 0;
    this.available = true;
    this.itemId = itemId;
    this.brandId = brandId;
    this.countryId = countryId;
  }
}
