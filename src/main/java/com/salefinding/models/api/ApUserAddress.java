package com.salefinding.models.api;

import com.salefinding.models.api.gen.ApUserAddressGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/** A class which represents the user_address table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "user_address")
@Data
public class ApUserAddress extends ApUserAddressGen {}
