package com.salefinding.models.api;

import com.salefinding.models.api.gen.ApOrderGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/** A class which represents the order table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "order")
@Data
public class ApOrder extends ApOrderGen {}
