package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApItemGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

/** A class which represents the item table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "item")
@Data
public class ApItem extends ApItemGen {

  public static final String TableName = "item";

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "brand_id", insertable = false, updatable = false)
  private ApBrand brand;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "category_id", insertable = false, updatable = false)
  private ApCategory category;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "item", fetch = FetchType.EAGER)
  private List<ApItemCountryDetail> itemDetailList;

  @Transient private String brandName;

  public ApItem(Long brandId) {
    this.available = true;
    this.maxSaleRate = 0;
    this.brandId = brandId;
  }
}
