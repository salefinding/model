package com.salefinding.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.api.gen.ApCurrencyGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/** A class which represents the currency table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "currency")
@Data
public class ApCurrency extends ApCurrencyGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToOne(mappedBy = "currency")
  private ApCountry country;
}
