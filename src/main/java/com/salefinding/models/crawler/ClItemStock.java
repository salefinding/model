package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClItemStockGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

/** A class which represents the item_stock table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "item_stock")
@Data
public class ClItemStock extends ClItemStockGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "item_detail_id", insertable = false, updatable = false)
  private ClItemCountryDetail itemCountryDetail;

  public ClItemStock(
      Long itemDetailId,
      String colorCode,
      String sizeCode,
      Integer stock,
      BigDecimal oldPrice,
      BigDecimal price,
      BigDecimal specialPrice) {
    this.status = true;
    this.itemDetailId = itemDetailId;
    this.colorCode = colorCode;
    this.sizeCode = sizeCode;
    this.stock = stock;
    this.oldPrice = oldPrice;
    this.price = price;
    this.specialPrice = specialPrice;
  }
}
