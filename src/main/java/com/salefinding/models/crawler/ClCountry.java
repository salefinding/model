package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClCountryGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

/** A class which represents the country table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "country")
@Data
public class ClCountry extends ClCountryGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "country")
  private List<ClBrandCountry> brandCountryList;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToOne
  @JoinColumn(name = "currency_id", insertable = false, updatable = false)
  private ClCurrency currency;
}
