package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClItemGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/** A class which represents the item table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "item")
@Data
public class ClItem extends ClItemGen {

  public static final String TableName = "item";

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "brand_id", insertable = false, updatable = false)
  private ClBrand brand;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "category_id", insertable = false, updatable = false)
  private ClCategory category;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "item", fetch = FetchType.EAGER)
  private List<ClItemCountryDetail> itemCountryDetailList;

  public ClItem(Long brandId, Integer subBrand) {
    super();
    this.brandId = brandId;
    this.subBrand = subBrand;
    this.available = false;
    this.maxSaleRate = 0;
    this.itemCountryDetailList = new ArrayList<>();
  }

  public ClItem(Long brandId, Integer subBrand, String code) {
    this(brandId, subBrand);
    this.code = code;
  }
}
