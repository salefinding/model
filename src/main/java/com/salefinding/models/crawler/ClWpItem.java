package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClWpItemGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

/** A class which represents the item table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "wp_item")
@Data
public class ClWpItem extends ClWpItemGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToOne
  @JoinColumn(name = "item_id", insertable = false, updatable = false)
  private ClItem clItem;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "wp_item_image")
  private List<ClWpImage> clWpImages;

  public ClWpItem() {
    this.available = true;
    this.removedInWp = false;
  }

  public ClWpItem(Long itemId) {
    this();
    this.itemId = itemId;
  }

  public ClWpItem(ClItem clItem) {
    this();
    this.clItem = clItem;

    if (clItem != null) {
      this.itemId = clItem.getId();
      this.brandId = clItem.getBrandId();
      this.available = clItem.getAvailable();
    }
  }
}
