package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClCategoryGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** A class which represents the crawler_info table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "category")
@Data
public class ClCategory extends ClCategoryGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "brand_id", insertable = false, updatable = false)
  private ClBrand brand;

  public ClCategory() {
    super();
  }

  public ClCategory(
      Long brandId,
      Integer subBrand,
      String cat1,
      String cat2,
      String cat3,
      String cat4,
      String cat5) {
    this();
    this.brandId = brandId;
    this.subBrand = subBrand;
    this.cat1 = cat1;
    this.cat2 = cat2;
    this.cat3 = cat3;
    this.cat4 = cat4;
    this.cat5 = cat5;
  }

  public String getCatByIndex(int index) {
    String result = null;
    switch (index) {
      case 1:
        result = cat1;
        break;
      case 2:
        result = cat2;
        break;
      case 3:
        result = cat3;
        break;
      case 4:
        result = cat4;
        break;
      case 5:
        result = cat5;
        break;
    }
    return result;
  }
}
