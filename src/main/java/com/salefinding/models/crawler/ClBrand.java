package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClBrandGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/** A class which represents the brand table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "brand")
@Data
public class ClBrand extends ClBrandGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "brand")
  private List<ClBrandCountry> clBrandCountries;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "brand", fetch = FetchType.LAZY)
  private List<ClItem> itemList;
}
