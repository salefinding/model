package com.salefinding.models.crawler.gen;

import com.salefinding.pojo.wp.WpCategory;
import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.DbJson;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the item table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ClWpCategoryGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "brand_id", nullable = false)
  protected Long brandId;

  public static final String BrandIdColumn = "brandId";

  @Column(name = "category_id")
  protected Long categoryId;

  public static final String CategoryIdColumn = "categoryId";

  @Column(name = "wp_category_id")
  protected Long wpCategoryId;

  public static final String WpCategoryIdColumn = "wpCategoryId";

  @Column(name = "wp_parent_category_id")
  protected Long wpParentCategoryId;

  public static final String WpParentCategoryIdColumn = "wpParentCategoryId";

  @Column(name = "wp_category", nullable = false)
  @DbJson
  protected WpCategory wpCategory;

  public static final String WpCategoryColumn = "wpCategory";

  @Column(name = "available", nullable = false)
  protected Boolean available;

  public static final String AvailableColumn = "available";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
