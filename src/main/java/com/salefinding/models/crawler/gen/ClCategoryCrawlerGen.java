package com.salefinding.models.crawler.gen;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/** A class which represents the category_crawler table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class ClCategoryCrawlerGen extends AbstractClCrawlableBaseEntity {

  @Column(name = "brand_country_id", nullable = false)
  protected Long brandCountryId;

  public static final String BrandCountryIdColumn = "brandCountryId";

  @Column(name = "country_id", nullable = false)
  protected Long countryId;

  public static final String CountryIdColumn = "countryId";

  @Column(name = "category_id")
  protected Long categoryId;

  public static final String CategoryIdColumn = "categoryId";

  @Column(name = "cat1", length = 200)
  protected String cat1;

  public static final String Cat1Column = "cat1";

  @Column(name = "cat2", length = 200)
  protected String cat2;

  public static final String Cat2Column = "cat2";

  @Column(name = "cat3", length = 200)
  protected String cat3;

  public static final String Cat3Column = "cat3";

  @Column(name = "cat4", length = 200)
  protected String cat4;

  public static final String Cat4Column = "cat4";

  @Column(name = "cat5", length = 200)
  protected String cat5;

  public static final String Cat5Column = "cat5";

  @Column(name = "url", length = 65535)
  protected String url;

  public static final String UrlColumn = "url";
}
