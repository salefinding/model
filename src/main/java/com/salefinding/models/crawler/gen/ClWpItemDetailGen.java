package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the item table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ClWpItemDetailGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "brand_id", nullable = false)
  protected Long brandId;

  public static final String BrandIdColumn = "brandId";

  @Column(name = "item_detail_id", nullable = false)
  protected Long itemDetailId;

  public static final String ItemDetailIdColumn = "itemDetailId";

  @Column(name = "wp_product_variation_id", nullable = false)
  protected Long wpProductVariationId;

  public static final String WpProductVariationIdColumn = "wpProductVariationId";

  @Column(name = "wp_item_id", nullable = false)
  protected Long wpItemId;

  public static final String WpItemIdColumn = "wpItemId";

  @Column(name = "price")
  protected java.math.BigDecimal price;

  public static final String PriceColumn = "price";

  @Column(name = "available", nullable = false)
  protected Boolean available;

  public static final String AvailableColumn = "available";

  @Column(name = "removed_in_wp", nullable = false)
  protected Boolean removedInWp;

  public static final String RemovedInWpColumn = "removedInWp";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
