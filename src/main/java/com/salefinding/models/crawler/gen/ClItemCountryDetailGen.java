package com.salefinding.models.crawler.gen;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/** A class which represents the item_country_detail table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class ClItemCountryDetailGen extends AbstractClCrawlableBaseEntity {

  @Column(name = "item_id", nullable = false)
  protected Long itemId;

  public static final String ItemIdColumn = "itemId";

  @Column(name = "country_id", nullable = false)
  protected Long countryId;

  public static final String CountryIdColumn = "countryId";

  @Column(name = "brand_country_id", nullable = false)
  protected Long brandCountryId;

  public static final String BrandCountryIdColumn = "brandCountryId";

  @Column(name = "category_crawler_id", nullable = false)
  protected Long categoryCrawlerId;

  public static final String CategoryCrawlerIdColumn = "categoryCrawlerId";

  @Column(name = "code", nullable = false, length = 200)
  protected String code;

  public static final String CodeColumn = "code";

  @Column(name = "name", nullable = false, length = 200)
  protected String name;

  public static final String NameColumn = "name";

  @Column(name = "image", nullable = false, length = 65535)
  protected String image;

  public static final String ImageColumn = "image";

  @Column(name = "old_price")
  protected java.math.BigDecimal oldPrice;

  public static final String OldPriceColumn = "oldPrice";

  @Column(name = "price", nullable = false)
  protected java.math.BigDecimal price;

  public static final String PriceColumn = "price";

  @Column(name = "special_price")
  protected java.math.BigDecimal specialPrice;

  public static final String SpecialPriceColumn = "specialPrice";

  @Column(name = "sale_rate", nullable = false)
  protected Integer saleRate;

  public static final String SaleRateColumn = "saleRate";

  @Column(name = "url", nullable = false, length = 65535)
  protected String url;

  public static final String UrlColumn = "url";

  @Column(name = "parent_url", length = 65535)
  protected String parentUrl;

  public static final String ParentUrlColumn = "parentUrl";
}
