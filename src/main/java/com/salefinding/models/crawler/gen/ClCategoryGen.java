package com.salefinding.models.crawler.gen;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/** A class which represents the category table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class ClCategoryGen extends AbstractClCrawlableBaseEntity {

  @Column(name = "cat1", length = 200)
  protected String cat1;

  public static final String Cat1Column = "cat1";

  @Column(name = "cat2", length = 200)
  protected String cat2;

  public static final String Cat2Column = "cat2";

  @Column(name = "cat3", length = 200)
  protected String cat3;

  public static final String Cat3Column = "cat3";

  @Column(name = "cat4", length = 200)
  protected String cat4;

  public static final String Cat4Column = "cat4";

  @Column(name = "cat5", length = 200)
  protected String cat5;

  public static final String Cat5Column = "cat5";

  @Column(name = "shipping_fee")
  protected String shippingFee;

  public static final String ShippingFeeColumn = "shippingFee";
}
