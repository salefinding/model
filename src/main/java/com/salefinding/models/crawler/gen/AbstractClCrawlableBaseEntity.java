package com.salefinding.models.crawler.gen;

import com.salefinding.models.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class AbstractClCrawlableBaseEntity extends BaseModel {

  @Column(name = "brand_id", nullable = false)
  protected Long brandId;

  public static final String BrandIdColumn = "brandId";

  @Column(name = "sub_brand", nullable = false)
  protected Integer subBrand;

  public static final String SubBrandColumn = "subBrand";

  @Column(name = "available", nullable = false)
  protected Boolean available;

  public static final String AvailableColumn = "available";

  @Column(name = "crawled", nullable = false)
  protected Boolean crawled;

  public static final String CrawledColumn = "crawled";

  public AbstractClCrawlableBaseEntity() {
    this.available = true;
    this.crawled = true;
  }
}
