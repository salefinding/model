package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the brand_country table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ClBrandCountryGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "brand_id", nullable = false)
  protected Long brandId;

  public static final String BrandIdColumn = "brandId";

  @Column(name = "sub_brand", nullable = false)
  protected Integer subBrand;

  public static final String SubBrandColumn = "subBrand";

  @Column(name = "country_id", nullable = false)
  protected Long countryId;

  public static final String CountryIdColumn = "countryId";

  @Column(name = "group_id", nullable = false)
  protected Integer groupId;

  public static final String GroupIdColumn = "groupId";

  @Column(name = "crawler_info_id", nullable = false)
  protected Long crawlerInfoId;

  public static final String CrawlerInfoIdColumn = "crawlerInfoId";

  @Column(name = "launcher_class", nullable = false, length = 65535)
  protected String launcherClass;

  public static final String LauncherClass = "launcherClass";

  @Column(name = "fetcher_class", nullable = false, length = 65535)
  protected String fetcherClass;

  public static final String FetcherClass = "fetcherClass";

  @Column(name = "crawler_class", nullable = false, length = 65535)
  protected String crawlerClass;

  public static final String CrawlerClassColumn = "crawlerClass";

  @Column(name = "parser_class", nullable = false, length = 65535)
  protected String parserClass;

  public static final String ParserClassColumn = "parserClass";

  @Column(name = "category_parser_class", nullable = false, length = 65535)
  protected String categoryParserClass;

  public static final String CategoryParserClassColumn = "categoryParserClass";

  @Column(name = "crawler_factory_class", nullable = false, length = 65535)
  protected String crawlerFactoryClass;

  public static final String CrawlerFactoryClassColumn = "crawlerFactoryClass";

  @Column(name = "start_url", nullable = false, length = 65535)
  protected String startUrl;

  public static final String StartUrlColumn = "startUrl";

  @Column(name = "wait_for_element", nullable = false, length = 65535)
  protected String waitForElement;

  public static final String WaitForElementColumn = "waitForElement";

  @Column(name = "check_stock_api_uri", length = 65535)
  protected String checkStockApiUri;

  public static final String CheckStockApiUriColumn = "checkStockApiUri";

  @Column(name = "filter", nullable = false, length = 65535)
  protected String filter;

  public static final String FilterColumn = "filter";

  @Column(name = "repeat", nullable = false)
  protected Boolean repeat;

  public static final String RepeatColumn = "repeat";

  @Column(name = "politeness_delay", nullable = false)
  protected Integer politenessDelay;

  public static final String PolitenessDelayColumn = "politenessDelay";

  @Column(name = "number_of_thread", nullable = false)
  protected Integer numberOfThread;

  public static final String NumberOfThreadColumn = "numberOfThread";

  @Column(name = "last_crawling_item_count", nullable = false)
  protected Integer lastCrawlingItemCount;

  public static final String LastCrawlingItemCountColumn = "lastCrawlingItemCount";

  @Column(name = "last_crawling_time", nullable = false)
  protected Long lastCrawlingTime;

  public static final String LastCrawlingTimeColumn = "lastCrawlingTime";

  @Column(name = "last_crawling_at")
  protected java.sql.Timestamp lastCrawlingAt;

  public static final String LastCrawlingAtColumn = "lastCrawlingAt";

  @Column(name = "order", nullable = false)
  protected Integer order;

  public static final String OrderColumn = "order";

  @Column(name = "running", nullable = false)
  protected Integer running;

  public static final String RunningColumn = "running";

  @Column(name = "enable", nullable = false)
  protected Boolean enable;

  public static final String EnableColumn = "enable";

  @Column(name = "uuid", length = 32)
  protected String uuid;

  public static final String UuidColumn = "uuid";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
