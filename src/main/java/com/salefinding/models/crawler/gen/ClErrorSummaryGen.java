package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the error_summary table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ClErrorSummaryGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "run_uid")
  protected String runUid;

  public static final String RunUidColumn = "runUid";

  @Column(name = "message", nullable = false)
  protected String message;

  public static final String MessageColumn = "message";

  @Column(name = "stack_trace")
  protected String stackTrace;

  public static final String StackTraceColumn = "stackTrace";

  @Column(name = "data")
  protected String data;

  public static final String DataColumn = "data";

  @Column(name = "status", nullable = false)
  protected Integer status;

  public static final String StatusColumn = "status";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
