package com.salefinding.models.crawler.gen;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/** A class which represents the key_value table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "key_value")
@Data
public class ClKeyValue extends ClKeyValueGen {}
