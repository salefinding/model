package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the brand_country table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ClCrawlingDataGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "brand_country_id", nullable = false)
  protected Long brandCountryId;

  public static final String BrandCountryIdColumn = "brandCountryId";

  @Column(name = "run_uid")
  protected String runUid;

  public static final String RunUidColumn = "runUid";

  @Column(name = "url", length = 65535)
  protected String url;

  public static final String UrlColumn = "url";

  @Column(name = "should_visit", nullable = false)
  protected Boolean shouldVisit;

  public static final String ShouldVisitColumn = "shouldVisit";

  @Column(name = "visited", nullable = false)
  protected Boolean visited;

  public static final String VisitedColumn = "visited";

  @Column(name = "saved_item", nullable = false)
  protected Boolean savedItem;

  public static final String SavedItemColumn = "savedItem";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
