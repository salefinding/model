package com.salefinding.models.crawler.gen;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/** A class which represents the item table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class ClItemGen extends AbstractClCrawlableBaseEntity {

  @Column(name = "category_id", nullable = false)
  protected Long categoryId;

  public static final String CategoryIdColumn = "categoryId";

  @Column(name = "code", nullable = false, length = 200)
  protected String code;

  public static final String CodeColumn = "code";

  @Column(name = "name", nullable = false, length = 200)
  protected String name;

  public static final String NameColumn = "name";

  @Column(name = "max_sale_rate", nullable = false)
  protected Integer maxSaleRate;

  public static final String MaxSaleRateColumn = "maxSaleRate";
}
