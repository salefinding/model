package com.salefinding.models.crawler.gen;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/** A class which represents the brand_country table in the crawler Database. */
@MappedSuperclass
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ClCrawlingStatisticsGen extends Model {

  @Id
  @Column(name = "id")
  @EqualsAndHashCode.Include
  protected Long id;

  public static final String IdColumn = "id";

  @Column(name = "brand_country_id", nullable = false)
  protected Long brandCountryId;

  public static final String BrandCountryIdColumn = "brandCountryId";

  @Column(name = "politeness_delay", nullable = false)
  protected Integer politenessDelay;

  public static final String PolitenessDelayColumn = "politenessDelay";

  @Column(name = "number_of_thread", nullable = false)
  protected Integer numberOfThread;

  public static final String NumberOfThreadColumn = "numberOfThread";

  @Column(name = "crawling_item_count", nullable = false)
  protected Integer crawlingItemCount;

  public static final String CrawlingItemCountColumn = "crawlingItemCount";

  @Column(name = "crawling_time", nullable = false)
  protected Long crawlingTime;

  public static final String CrawlingTimeColumn = "crawlingTime";

  @Column(name = "crawling_at")
  protected java.sql.Timestamp crawlingAt;

  public static final String CrawlingAtColumn = "crawlingAt";

  @Column(name = "created_at", nullable = false)
  @CreatedTimestamp
  protected java.sql.Timestamp createdAt;

  public static final String CreatedAtColumn = "createdAt";

  @Column(name = "updated_at", nullable = false)
  @UpdatedTimestamp
  protected java.sql.Timestamp updatedAt;

  public static final String UpdatedAtColumn = "updatedAt";
}
