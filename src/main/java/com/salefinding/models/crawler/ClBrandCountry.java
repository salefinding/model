package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClBrandCountryGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Set;

/** A class which represents the brand_country table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "brand_country")
@Data
public class ClBrandCountry extends ClBrandCountryGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "brand_id", insertable = false, updatable = false)
  private ClBrand brand;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "country_id", insertable = false, updatable = false)
  private ClCountry country;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "crawler_info_id", insertable = false, updatable = false)
  private ClCrawlerInfo crawlerInfo;

  @Transient private Set<String> possibleItemUrls;

  public synchronized int increaseLastCrawlingItemCount(int count) {
    lastCrawlingItemCount += count;
    return lastCrawlingItemCount;
  }
}
