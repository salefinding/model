package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClItemImageGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** A class which represents the item_image table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "item_image")
@Data
public class ClItemImage extends ClItemImageGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "item_detail_id", insertable = false, updatable = false)
  private ClItemCountryDetail itemCountryDetail;

  public ClItemImage(Long itemDetailId, String thumbUrl, String url, String description) {
    this.itemDetailId = itemDetailId;
    this.thumbUrl = thumbUrl;
    this.url = url;
    this.description = description;
    if (url != null && !url.contains("data:image") && url.lastIndexOf('/') > 0) {
      name = url.substring(url.lastIndexOf('/') + 1);
    }
  }

  @Override
  public String getName() {
    if (name == null && url != null && !url.contains("data:image") && url.lastIndexOf('/') > 0) {
      name = url.substring(url.lastIndexOf('/') + 1);
    }

    return name;
  }
}
