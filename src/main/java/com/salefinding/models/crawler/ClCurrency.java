package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClCurrencyGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/** A class which represents the currency table in the salefinding_api Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "currency")
@Data
public class ClCurrency extends ClCurrencyGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToOne(mappedBy = "currency")
  private ClCountry country;
}
