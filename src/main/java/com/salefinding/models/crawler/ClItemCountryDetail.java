package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClItemCountryDetailGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/** A class which represents the item table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "item_country_detail")
@Data
public class ClItemCountryDetail extends ClItemCountryDetailGen {

  public static final String TableName = "item_country_detail";

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "item_id", insertable = false, updatable = false)
  private ClItem item;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "country_id", insertable = false, updatable = false)
  private ClCountry country;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "category_crawler_id", insertable = false, updatable = false)
  private ClCategoryCrawler categoryCrawler;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "itemCountryDetail", fetch = FetchType.EAGER)
  private List<ClItemStock> itemStockList;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "itemCountryDetail", fetch = FetchType.EAGER)
  private List<ClItemImage> itemImageList;

  public ClItemCountryDetail(
      Long itemId, Long brandCountryId, Long brandId, Integer subBrand, Long countryId) {
    super();
    this.saleRate = 0;
    this.available = false;
    this.itemId = itemId;
    this.brandCountryId = brandCountryId;
    this.brandId = brandId;
    this.subBrand = subBrand;
    this.countryId = countryId;
    this.itemStockList = new ArrayList<>();
    this.itemImageList = new ArrayList<>();
  }
}
