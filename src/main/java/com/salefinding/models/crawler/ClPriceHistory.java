package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClPriceHistoryGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** A class which represents the price_history table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "price_history")
@Data
public class ClPriceHistory extends ClPriceHistoryGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "item_detail_id", insertable = false, updatable = false)
  private ClItemCountryDetail itemCountryDetail;
}
