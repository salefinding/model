package com.salefinding.models.crawler;

import com.salefinding.models.crawler.gen.ClWpImageGen;
import com.salefinding.pojo.wp.WpImage;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "wp_image")
@Data
public class ClWpImage extends ClWpImageGen {

  public ClWpImage(WpImage image) {
    setImage(image);
  }

  public void setImage(WpImage image) {
    this.image = image;
    if (image != null) {
      this.name = image.name;
      this.url = image.src;
    }
  }
}
