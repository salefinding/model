package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClWpItemDetailGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/** A class which represents the item_image table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "wp_item_detail")
@Data
public class ClWpItemDetail extends ClWpItemDetailGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToOne
  @JoinColumn(name = "item_detail_id", insertable = false, updatable = false)
  private ClItemCountryDetail clItemCountryDetail;

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "wp_item_id", insertable = false, updatable = false)
  private ClWpItem clWpItem;

  public ClWpItemDetail() {
    this.available = true;
    this.removedInWp = false;
  }

  public ClWpItemDetail(ClItemCountryDetail clItemCountryDetail) {
    this();
    this.clItemCountryDetail = clItemCountryDetail;
    if (clItemCountryDetail != null) {
      this.itemDetailId = clItemCountryDetail.getId();
      this.brandId = clItemCountryDetail.getBrandId();
      this.available = clItemCountryDetail.getAvailable();
    }
  }
}
