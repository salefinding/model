package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClCrawlerInfoGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/** A class which represents the crawler_info table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "crawler_info")
@Data
public class ClCrawlerInfo extends ClCrawlerInfoGen {

  @EqualsAndHashCode.Exclude
  @JsonIgnore
  @OneToMany(mappedBy = "country", fetch = FetchType.LAZY)
  private List<ClBrandCountry> brandCountryList;
}
