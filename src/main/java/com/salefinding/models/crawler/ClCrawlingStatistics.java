package com.salefinding.models.crawler;

import com.salefinding.models.crawler.gen.ClCrawlingStatisticsGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/** A class which represents the crawling_statistics table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "crawling_statistics")
@Data
public class ClCrawlingStatistics extends ClCrawlingStatisticsGen {

  public ClCrawlingStatistics(ClBrandCountry brandCountry) {
    this.brandCountryId = brandCountry.getId();
    this.politenessDelay = brandCountry.getPolitenessDelay();
    this.crawlingAt = brandCountry.getLastCrawlingAt();
    this.crawlingItemCount = brandCountry.getLastCrawlingItemCount();
    this.crawlingTime = brandCountry.getLastCrawlingTime();
    this.numberOfThread = brandCountry.getNumberOfThread();
  }

  public ClCrawlingStatistics() {}
}
