package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClCategoryCrawlerGen;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

/** A class which represents the category table in the crawler Database. */
@Entity
@Table(name = "category_crawler")
@Data
public class ClCategoryCrawler extends ClCategoryCrawlerGen {

  public static final String TableName = "category_crawler";

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "brand_id", insertable = false, updatable = false)
  private ClBrand brand;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "country_id", insertable = false, updatable = false)
  private ClCountry country;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "category_id", insertable = false, updatable = false)
  private ClCategory category;

  public ClCategoryCrawler() {
    super();
  }

  public ClCategoryCrawler(ClBrandCountry brandCountry) {
    this();
    this.brandCountryId = brandCountry.getId();
    this.brandId = brandCountry.getBrandId();
    this.subBrand = brandCountry.getSubBrand();
    this.countryId = brandCountry.getCountryId();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClCategoryCrawler that = (ClCategoryCrawler) o;
    return Objects.equals(brandId, that.brandId)
        && Objects.equals(countryId, that.countryId)
        && Objects.equals(cat1, that.cat1)
        && Objects.equals(cat2, that.cat2)
        && Objects.equals(cat3, that.cat3)
        && Objects.equals(cat4, that.cat4)
        && Objects.equals(cat5, that.cat5)
        && Objects.equals(url, that.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(brandId, countryId, cat1, cat2, cat3, cat4, cat5, url);
  }
}
