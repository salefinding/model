package com.salefinding.models.crawler;

import com.salefinding.enums.ErrorSummaryStatus;
import com.salefinding.models.crawler.gen.ClErrorSummaryGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/** A class which represents the error_summary table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "error_summary")
@Data
public class ClErrorSummary extends ClErrorSummaryGen {

  public ClErrorSummary() {
    this.status = ErrorSummaryStatus.NEW.getValue();
  }
}
