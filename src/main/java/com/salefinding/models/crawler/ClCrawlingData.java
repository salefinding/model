package com.salefinding.models.crawler;

import com.salefinding.models.crawler.gen.ClCrawlingDataGen;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/** A class which represents the crawling_data table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "crawling_data")
@Data
public class ClCrawlingData extends ClCrawlingDataGen {

  public ClCrawlingData() {}

  public ClCrawlingData(
      Long brandCountryId,
      String runUid,
      String url,
      Boolean shouldVisit,
      Boolean visited,
      Boolean savedItem) {
    this.brandCountryId = brandCountryId;
    this.runUid = runUid;
    this.url = url;
    this.shouldVisit = shouldVisit;
    this.visited = visited;
    this.savedItem = savedItem;
  }
}
