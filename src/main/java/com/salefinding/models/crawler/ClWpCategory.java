package com.salefinding.models.crawler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salefinding.models.crawler.gen.ClWpCategoryGen;
import com.salefinding.pojo.wp.WpCategory;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/** A class which represents the item table in the crawler Database. */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "wp_category")
@Data
public class ClWpCategory extends ClWpCategoryGen {

  @JsonIgnore
  @OneToOne
  @JoinColumn(name = "category_id", insertable = false, updatable = false)
  private ClCategory clCategory;

  public ClWpCategory() {}

  public ClWpCategory(ClCategory clCategory) {
    this();
    this.clCategory = clCategory;
    this.available = false;
    if (clCategory != null) {
      this.brandId = clCategory.getBrandId();
      this.categoryId = clCategory.getId();
      this.available = clCategory.getAvailable();
    }
  }

  public ClWpCategory(ClCategory clCategory, WpCategory wpCategory) {
    this(clCategory);
    this.wpCategoryId = wpCategory.id;
    this.wpParentCategoryId = wpCategory.parent;
    this.wpCategory = wpCategory;
  }
}
