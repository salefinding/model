package com.salefinding.enums;

public enum BrandCountryRunning {
  STOP(0),
  WAIT_TO_RUN(1),
  RUNNING(2);

  private int value;

  BrandCountryRunning(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
