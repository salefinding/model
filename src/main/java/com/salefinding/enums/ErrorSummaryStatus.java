package com.salefinding.enums;

public enum ErrorSummaryStatus {
  NEW(0),
  SENT(1);

  private int value;

  ErrorSummaryStatus(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
