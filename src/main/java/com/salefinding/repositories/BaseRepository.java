package com.salefinding.repositories;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import io.ebean.Finder;

import java.util.Collection;
import java.util.List;

public abstract class BaseRepository<I, T> {

  protected EbeanServer server;

  protected Finder<I, T> find;

  public BaseRepository(String dbName, Class<T> objectType) {
    server = Ebean.getServer(dbName);
    find = new Finder<>(objectType, dbName);
  }

  public T getById(I id) {
    return find.byId(id);
  }

  public List<T> getByIds(Collection<I> ids) {
    return find.query().where().in("id", ids).findList();
  }

  public List<T> getAll() {
    return find.all();
  }

  public void save(Object o) {
    server.save(o);
  }

  public void saveAll(Collection<Object> collection) {
    server.saveAll(collection);
  }

  public void update(Object o) {
    server.update(o);
  }

  public void delete(Object o) {
    server.delete(o);
  }

  public EbeanServer getServer() {
    return server;
  }
}
