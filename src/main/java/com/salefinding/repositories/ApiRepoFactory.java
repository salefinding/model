package com.salefinding.repositories;

import com.salefinding.repositories.api.*;

public class ApiRepoFactory {

  private static ApiRepoFactory factory;

  private static String dbName;

  private ApCommonRepository commonRepository;

  private ApBrandRepository brandRepository;

  private ApCategoryRepository categoryRepository;

  private ApItemRepository itemRepository;

  private ApUserRepository userRepository;

  private ApUserSentMailDataRepository sentMailDataRepository;

  private ApiRepoFactory() {}

  public static ApiRepoFactory getInstance(String dbInfo) {
    if (ApiRepoFactory.dbName != null && !ApiRepoFactory.dbName.equals(dbInfo)) {
      if (factory != null) {
        factory = null;
      }
    }
    ApiRepoFactory.dbName = dbInfo;
    if (factory == null) {
      synchronized (ApiRepoFactory.class) {
        if (factory == null) {
          factory = new ApiRepoFactory();
        }
      }
    }
    return factory;
  }

  public ApBrandRepository getBrandRepository() {
    if (brandRepository == null) {
      synchronized (ApiRepoFactory.class) {
        if (brandRepository == null) {
          brandRepository = new ApBrandRepository(dbName);
        }
      }
    }
    return brandRepository;
  }

  public ApItemRepository getItemRepository() {
    if (itemRepository == null) {
      synchronized (ApiRepoFactory.class) {
        if (itemRepository == null) {
          itemRepository = new ApItemRepository(dbName);
        }
      }
    }
    return itemRepository;
  }

  public ApCategoryRepository getCategoryRepository() {
    if (categoryRepository == null) {
      synchronized (ApiRepoFactory.class) {
        if (categoryRepository == null) {
          categoryRepository = new ApCategoryRepository(dbName);
        }
      }
    }
    return categoryRepository;
  }

  public ApCommonRepository getCommonRepository() {
    if (commonRepository == null) {
      synchronized (ApiRepoFactory.class) {
        if (commonRepository == null) {
          commonRepository = new ApCommonRepository(dbName);
        }
      }
    }
    return commonRepository;
  }

  public ApUserRepository getUserRepository() {
    if (userRepository == null) {
      synchronized (ApUserRepository.class) {
        if (userRepository == null) {
          userRepository = new ApUserRepository(dbName);
        }
      }
    }
    return userRepository;
  }

  public ApUserSentMailDataRepository getSentMailDataRepository() {
    if (sentMailDataRepository == null) {
      synchronized (ApUserSentMailDataRepository.class) {
        if (sentMailDataRepository == null) {
          sentMailDataRepository = new ApUserSentMailDataRepository(dbName);
        }
      }
    }
    return sentMailDataRepository;
  }
}
