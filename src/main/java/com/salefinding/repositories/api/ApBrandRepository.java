package com.salefinding.repositories.api;

import com.salefinding.models.api.ApBrand;
import com.salefinding.repositories.BaseRepository;

public class ApBrandRepository extends BaseRepository<Long, ApBrand> {

  public ApBrandRepository(String dbName) {
    super(dbName, ApBrand.class);
  }

  public ApBrand getByName(String name) {
    return find.query().where().eq(ApBrand.NameColumn, name).findOne();
  }
}
