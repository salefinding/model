package com.salefinding.repositories.api;

import com.salefinding.models.api.ApItem;
import com.salefinding.models.api.ApItemCountryDetail;
import com.salefinding.repositories.BaseRepository;
import io.ebean.Expr;
import io.ebean.SqlUpdate;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ApItemRepository extends BaseRepository<Long, ApItem> {

  public ApItemRepository(String dbName) {
    super(dbName, ApItem.class);
  }

  public ApItem findByItem(ApItem clItem) {
    return find.query()
        .where()
        .eq(ApItem.BrandIdColumn, clItem.getBrandId())
        .eq(ApItem.CodeColumn, clItem.getCode())
        .findOne();
  }

  public ApItem findByItem(Long brandId, String code) {
    return find.query()
        .where()
        .eq(ApItem.BrandIdColumn, brandId)
        .eq(ApItem.CodeColumn, code)
        .findOne();
  }

  public ApItemCountryDetail findItemDetailByUuid(String uuid) {
    return server.find(ApItemCountryDetail.class).where().eq(ApItem.UuidColumn, uuid).findOne();
  }

  public ApItem findItemByUuid(String uuid) {
    return find.query().where().eq(ApItem.UuidColumn, uuid).findOne();
  }

  public Collection<ApItem> searchByNameOrCode(String input) {
    return find.query()
        .where()
        .or(Expr.icontains(ApItem.NameColumn, input), Expr.icontains(ApItem.CodeColumn, input))
        .findList();
  }

  public List<ApItem> getByCatId(Long catId) {
    return getByCatIds(Collections.singletonList(catId));
  }

  public List<ApItem> getByCatIds(Collection<Long> catId) {
    return find.query()
        .fetch("itemDetailList")
        .where()
        .in("itemDetailList.categoryId", catId)
        .eq(ApItem.AvailableColumn, true)
        .gt(ApItem.MaxSaleRateColumn, 0)
        .orderBy()
        .desc(ApItem.MaxSaleRateColumn)
        .findList();
  }

  public List<ApItem> getTopSaleRate(int number) {
    return find.query()
        .fetch("itemDetailList")
        .where()
        .eq(ApItem.AvailableColumn, true)
        .gt(ApItem.MaxSaleRateColumn, 0)
        .orderBy()
        .desc(ApItem.MaxSaleRateColumn)
        .setMaxRows(number)
        .findList();
  }

  public int setUnavailableForAllItems(Long brandId) {
    SqlUpdate sqlUpdate;
    if (brandId == null) {
      sqlUpdate = server.createSqlUpdate("update item set available = 0 where id > 0");
    } else {
      sqlUpdate = server.createSqlUpdate("update item set available = 0 where brand_id = :brandId");
      sqlUpdate.setParameter("brandId", brandId);
    }
    return server.execute(sqlUpdate);
  }

  public int setUnavailableForAllItemDetails(Long brandId) {
    SqlUpdate sqlUpdate;
    if (brandId == null) {
      sqlUpdate =
          server.createSqlUpdate("update item_country_detail set available = 0 where id > 0");
    } else {
      sqlUpdate =
          server.createSqlUpdate(
              "update item_country_detail set available = 0 where brand_id = :brandId");
      sqlUpdate.setParameter("brandId", brandId);
    }
    return server.execute(sqlUpdate);
  }

  public int deleteAllStocksOfItem(Long itemDetailId) {
    SqlUpdate sqlUpdate =
        server.createSqlUpdate("delete from item_stock where item_detail_id = :itemDetailId");
    sqlUpdate.setParameter("itemDetailId", itemDetailId);
    return server.execute(sqlUpdate);
  }

  public int deleteAllImagesOfItem(Long itemDetailId) {
    SqlUpdate sqlUpdate =
        server.createSqlUpdate("delete from item_image where item_detail_id = :itemDetailId");
    sqlUpdate.setParameter("itemDetailId", itemDetailId);
    return server.execute(sqlUpdate);
  }
}
