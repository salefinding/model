package com.salefinding.repositories.api;

import com.salefinding.models.api.ApUser;
import com.salefinding.repositories.BaseRepository;

public class ApUserRepository extends BaseRepository<Long, ApUser> {

  public ApUserRepository(String dbName) {
    super(dbName, ApUser.class);
  }

  public ApUser getUserByEmail(String email) {
    return server.find(ApUser.class).where().eq(ApUser.EmailColumn, email).findOne();
  }
}
