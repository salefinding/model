package com.salefinding.repositories.api;

import com.salefinding.models.api.ApCountry;
import com.salefinding.models.api.ApUser;
import com.salefinding.models.api.ApUserItemSubscribe;
import com.salefinding.repositories.BaseRepository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ApCommonRepository extends BaseRepository<Long, Object> {

  public ApCommonRepository(String dbName) {
    super(dbName, Object.class);
  }

  public List<ApCountry> getAllCountries() {
    return server.find(ApCountry.class).fetch("currency").findList();
  }

  public Map<ApUser, List<ApUserItemSubscribe>> getAllValidUserItemSubscribes() {
    return server
        .find(ApUserItemSubscribe.class)
        .fetch("user")
        .where()
        .eq(ApUserItemSubscribe.StatusColumn, true)
        .findList()
        .stream()
        .collect(Collectors.groupingBy(ApUserItemSubscribe::getUser));
  }
}
