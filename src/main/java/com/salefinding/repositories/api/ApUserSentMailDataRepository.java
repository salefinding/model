package com.salefinding.repositories.api;

import com.salefinding.models.api.ApUserSentMailData;
import com.salefinding.repositories.BaseRepository;

public class ApUserSentMailDataRepository extends BaseRepository<Long, ApUserSentMailData> {

  public ApUserSentMailDataRepository(String dbName) {
    super(dbName, ApUserSentMailData.class);
  }

  public ApUserSentMailData getByUserId(Long userId) {
    return find.query().where().eq(ApUserSentMailData.UserIdColumn, userId).findOne();
  }
}
