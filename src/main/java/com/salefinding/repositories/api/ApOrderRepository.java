package com.salefinding.repositories.api;

import com.salefinding.models.api.ApOrder;
import com.salefinding.repositories.BaseRepository;

public class ApOrderRepository extends BaseRepository<Long, ApOrder> {

  public ApOrderRepository(String dbName) {
    super(dbName, ApOrder.class);
  }
}
