package com.salefinding.repositories.api;

import com.salefinding.models.api.ApCategory;
import com.salefinding.repositories.BaseRepository;
import io.ebean.ExpressionList;
import io.ebean.SqlUpdate;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class ApCategoryRepository extends BaseRepository<Long, ApCategory> {

  public ApCategoryRepository(String dbName) {
    super(dbName, ApCategory.class);
  }

  public List<ApCategory> getByBrandId(Long brandId) {
    return find.query()
        .where()
        .eq(ApCategory.BrandIdColumn, brandId)
        .eq(ApCategory.AvailableColumn, true)
        .findList();
  }

  public ApCategory findByUuid(String uuid) {
    return find.query().where().eq(ApCategory.UuidColumn, uuid).findOne();
  }

  public List<ApCategory> getCatsByBrand(Long brandId) {
    return find.query()
        .setDistinct(true)
        .select("cat1, cat2, cat3, cat4, cat5")
        .where()
        .eq(ApCategory.BrandIdColumn, brandId)
        .eq(ApCategory.AvailableColumn, true)
        .findList();
  }

  public List<ApCategory> getByCats(
      Long brandId, String cat1, String cat2, String cat3, String cat4, String cat5) {
    ExpressionList<ApCategory> expressionList =
        find.query().where().eq(ApCategory.BrandIdColumn, brandId);

    if (StringUtils.isNotBlank(cat1)) {
      expressionList.eq(ApCategory.Cat1Column, cat1);
    }

    if (StringUtils.isNotBlank(cat2)) {
      expressionList.eq(ApCategory.Cat2Column, cat2);
    }

    if (StringUtils.isNotBlank(cat3)) {
      expressionList.eq(ApCategory.Cat3Column, cat3);
    }

    if (StringUtils.isNotBlank(cat4)) {
      expressionList.eq(ApCategory.Cat4Column, cat4);
    }

    if (StringUtils.isNotBlank(cat5)) {
      expressionList.eq(ApCategory.Cat5Column, cat5);
    }

    return expressionList.findList();
  }

  public ApCategory findByCats(
      Long brandId, String cat1, String cat2, String cat3, String cat4, String cat5) {
    ExpressionList<ApCategory> expressionList =
        find.query().where().eq(ApCategory.BrandIdColumn, brandId);

    if (StringUtils.isNotBlank(cat1)) {
      expressionList.eq(ApCategory.Cat1Column, cat1);
    }

    if (StringUtils.isNotBlank(cat2)) {
      expressionList.eq(ApCategory.Cat2Column, cat2);
    }

    if (StringUtils.isNotBlank(cat3)) {
      expressionList.eq(ApCategory.Cat3Column, cat3);
    }

    if (StringUtils.isNotBlank(cat4)) {
      expressionList.eq(ApCategory.Cat4Column, cat4);
    }

    if (StringUtils.isNotBlank(cat5)) {
      expressionList.eq(ApCategory.Cat5Column, cat5);
    }

    return expressionList.findOne();
  }

  public void saveCategory(ApCategory clCategory) {
    ApCategory dbApCategory =
        findByCats(
            clCategory.getBrandId(),
            clCategory.getCat1(),
            clCategory.getCat2(),
            clCategory.getCat3(),
            clCategory.getCat4(),
            clCategory.getCat5());

    if (dbApCategory != null) {
      clCategory.setId(dbApCategory.getId());
      update(clCategory);
    } else {
      save(clCategory);
    }
  }

  public int setUnAvailableForAll(Long brandId) {
    SqlUpdate sqlUpdate;
    if (brandId == null) {
      sqlUpdate = server.createSqlUpdate("update category set available = 0 where id > 0");
    } else {
      sqlUpdate =
          server.createSqlUpdate("update category set available = 0 where brand_id = :brandId");
      sqlUpdate.setParameter("brandId", brandId);
    }
    return server.execute(sqlUpdate);
  }
}
