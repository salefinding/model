package com.salefinding.repositories.crawler;

import com.salefinding.models.crawler.ClWpCategory;
import com.salefinding.models.crawler.ClWpImage;
import com.salefinding.models.crawler.ClWpItem;
import com.salefinding.models.crawler.ClWpItemDetail;
import com.salefinding.repositories.BaseRepository;

import java.util.Collection;
import java.util.List;

public class ClWpRepository extends BaseRepository<Long, ClWpItem> {

  public ClWpRepository(String dbName) {
    super(dbName, ClWpItem.class);
  }

  public List<ClWpItem> getWpItems(Collection<Long> itemIds) {
    return server.find(ClWpItem.class).where().in(ClWpItem.ItemIdColumn, itemIds).findList();
  }

  public ClWpItem getWpItemByItemId(long itemId) {
    return server.find(ClWpItem.class).where().eq(ClWpItem.ItemIdColumn, itemId).findOne();
  }

  public List<ClWpItemDetail> getWpItemDetails(Collection<Long> itemDetailIds) {
    return server
        .find(ClWpItemDetail.class)
        .where()
        .in(ClWpItemDetail.ItemDetailIdColumn, itemDetailIds)
        .findList();
  }

  public List<ClWpItemDetail> getWpItemDetails(Long wpItemId) {
    return server
        .find(ClWpItemDetail.class)
        .where()
        .eq(ClWpItemDetail.WpItemIdColumn, wpItemId)
        .findList();
  }

  public List<ClWpItemDetail> getWpItemDetails(
      long brandId, int batchSize, long startId, boolean available) {
    return server
        .find(ClWpItemDetail.class)
        .fetch("clWpItem")
        .where()
        .gt(ClWpItemDetail.IdColumn, startId)
        .eq(ClWpItemDetail.BrandIdColumn, brandId)
        .eq(ClWpItemDetail.AvailableColumn, available)
        .eq(ClWpItemDetail.RemovedInWpColumn, false)
        .setMaxRows(batchSize)
        .order()
        .asc(ClWpItemDetail.IdColumn)
        .findList();
  }

  public List<ClWpCategory> getWpCategories() {
    return server.find(ClWpCategory.class).findList();
  }

  public List<ClWpCategory> getWpCategories(List<Long> categoryIds) {
    return server
        .find(ClWpCategory.class)
        .where()
        .in(ClWpCategory.CategoryIdColumn, categoryIds)
        .findList();
  }

  public List<ClWpItem> getWpItems(int batchSize, long startId) {
    return server
        .find(ClWpItem.class)
        .fetch("clItem")
        .where()
        .gt(ClWpItem.IdColumn, startId)
        .setMaxRows(batchSize)
        .order()
        .asc(ClWpItem.IdColumn)
        .findList();
  }

  public List<ClWpItemDetail> getWpItemDetails(int batchSize, long startId) {
    return server
        .find(ClWpItemDetail.class)
        .fetch("clItemCountryDetail")
        .where()
        .gt(ClWpItemDetail.IdColumn, startId)
        .setMaxRows(batchSize)
        .order()
        .asc(ClWpItemDetail.IdColumn)
        .findList();
  }

  public List<ClWpItem> getWpItems(long brandId, int batchSize, long startId, boolean available) {
    return server
        .find(ClWpItem.class)
        .where()
        .gt(ClWpItem.IdColumn, startId)
        .eq(ClWpItem.AvailableColumn, available)
        .eq(ClWpItem.RemovedInWpColumn, false)
        .eq(ClWpItem.BrandIdColumn, brandId)
        .setMaxRows(batchSize)
        .order()
        .asc(ClWpItem.IdColumn)
        .findList();
  }

  public int setClWpCategoriesStatus(Long brandId, boolean status) {
    if (brandId != null) {
      return server
          .createUpdate(
              ClWpCategory.class,
              "update ClWpCategory set available = :available where brandId = :brandId")
          .set("available", status)
          .set("brandId", brandId)
          .execute();
    } else {
      return server
          .createUpdate(
              ClWpCategory.class, "update ClWpCategory set available = :available where id > 0")
          .set("available", status)
          .execute();
    }
  }

  public int setClWpItemDetailsStatus(Long brandId, boolean status) {
    if (brandId != null) {
      return server
          .createUpdate(
              ClWpItemDetail.class,
              "update ClWpItemDetail set available = :available where brandId = :brandId")
          .set("available", status)
          .set("brandId", brandId)
          .execute();
    } else {
      return server
          .createUpdate(
              ClWpItemDetail.class, "update ClWpItemDetail set available = :available where id > 0")
          .set("available", status)
          .execute();
    }
  }

  public int setClWpItemsStatus(Long brandId, boolean status) {
    if (brandId != null) {
      return server
          .createUpdate(
              ClWpItem.class, "update ClWpItem set available = :available where brandId = :brandId")
          .set("available", status)
          .set("brandId", brandId)
          .execute();
    } else {
      return server
          .createUpdate(ClWpItem.class, "update ClWpItem set available = :available where id > 0")
          .set("available", status)
          .execute();
    }
  }

  public ClWpImage getWpImageByName(String name) {
    return server.find(ClWpImage.class).where().eq(ClWpImage.NameColumn, name).findOne();
  }
}
