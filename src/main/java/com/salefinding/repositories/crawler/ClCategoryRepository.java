package com.salefinding.repositories.crawler;

import com.salefinding.models.crawler.ClCategory;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.BaseRepository;
import io.ebean.ExpressionList;
import io.ebean.SqlUpdate;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class ClCategoryRepository extends BaseRepository<Long, ClCategory> {

  public ClCategoryRepository(String dbName) {
    super(dbName, ClCategory.class);
  }

  public List<ClCategoryCrawler> getAllCatCrawler() {
    return server.find(ClCategoryCrawler.class).findList();
  }

  public List<ClCategoryCrawler> getCatCrawlers(Long brandCountryId) {
    return server
        .find(ClCategoryCrawler.class)
        .where()
        .eq(ClCategoryCrawler.BrandCountryIdColumn, brandCountryId)
        .findList();
  }

  public List<ClCategory> getCatsByBrand(Long brandId) {
    return find.query()
        .setDistinct(true)
        .select("cat1, cat2, cat3, cat4, cat5")
        .where()
        .eq(ClCategory.BrandIdColumn, brandId)
        .findList();
  }

  public List<ClCategory> getAllAvailableCats(Integer batchSize, Long startId) {
    return find.query()
        .where()
        .gt(ClCategory.IdColumn, startId)
        .eq(ClCategory.AvailableColumn, true)
        .setMaxRows(batchSize)
        .order()
        .asc(ClCategory.IdColumn)
        .findList();
  }

  public List<ClCategory> getAllAvailableCats(Integer batchSize, Long startId, Long brandId) {
    return find.query()
        .where()
        .gt(ClCategory.IdColumn, startId)
        .eq(ClCategory.BrandIdColumn, brandId)
        .eq(ClCategory.AvailableColumn, true)
        .setMaxRows(batchSize)
        .order()
        .asc(ClCategory.IdColumn)
        .findList();
  }

  public List<ClCategory> getAllAvailableCats() {
    return find.query()
        .where()
        .eq(ClCategory.AvailableColumn, true)
        .order()
        .asc(ClCategory.IdColumn)
        .findList();
  }

  public List<ClCategory> getAllAvailableCats(Long brandId) {
    return find.query()
        .where()
        .eq(ClCategory.BrandIdColumn, brandId)
        .eq(ClCategory.AvailableColumn, true)
        .order()
        .asc(ClCategory.IdColumn)
        .findList();
  }

  public List<ClCategory> getAllCats(Integer batchSize, Long startId) {
    return find.query()
        .where()
        .gt(ClCategory.IdColumn, startId)
        .setMaxRows(batchSize)
        .order()
        .asc(ClCategory.IdColumn)
        .findList();
  }

  public List<ClCategory> getAllCats(Integer batchSize, Long startId, Long brandId) {
    return find.query()
        .where()
        .gt(ClCategory.IdColumn, startId)
        .eq(ClCategory.BrandIdColumn, brandId)
        .setMaxRows(batchSize)
        .order()
        .asc(ClCategory.IdColumn)
        .findList();
  }

  public ClCategory findByCats(
      Long brandId, String cat1, String cat2, String cat3, String cat4, String cat5) {
    ExpressionList<ClCategory> expressionList =
        find.query().where().eq(ClCategory.BrandIdColumn, brandId);

    if (StringUtils.isNotBlank(cat1)) {
      expressionList.eq(ClCategory.Cat1Column, cat1);
    }

    if (StringUtils.isNotBlank(cat2)) {
      expressionList.eq(ClCategory.Cat2Column, cat2);
    }

    if (StringUtils.isNotBlank(cat3)) {
      expressionList.eq(ClCategory.Cat3Column, cat3);
    }

    if (StringUtils.isNotBlank(cat4)) {
      expressionList.eq(ClCategory.Cat4Column, cat4);
    }

    if (StringUtils.isNotBlank(cat5)) {
      expressionList.eq(ClCategory.Cat5Column, cat5);
    }

    return expressionList.findOne();
  }

  public ClCategoryCrawler getCategoryCrawler(ClCategoryCrawler category) {
    ExpressionList<ClCategoryCrawler> expressionList =
        server
            .createQuery(ClCategoryCrawler.class)
            .where()
            .eq(ClCategoryCrawler.BrandIdColumn, category.getBrandId())
            .eq(ClCategoryCrawler.CountryIdColumn, category.getCountryId());
    if (StringUtils.isNotBlank(category.getCat1())) {
      expressionList.eq(ClCategoryCrawler.Cat1Column, category.getCat1());
    }

    if (StringUtils.isNotBlank(category.getCat2())) {
      expressionList.eq(ClCategoryCrawler.Cat2Column, category.getCat2());
    }

    if (StringUtils.isNotBlank(category.getCat3())) {
      expressionList.eq(ClCategoryCrawler.Cat3Column, category.getCat3());
    }

    if (StringUtils.isNotBlank(category.getCat4())) {
      expressionList.eq(ClCategoryCrawler.Cat4Column, category.getCat4());
    }

    if (StringUtils.isNotBlank(category.getCat5())) {
      expressionList.eq(ClCategoryCrawler.Cat5Column, category.getCat5());
    }

    if (StringUtils.isNotBlank(category.getUrl())) {
      expressionList.eq(ClCategoryCrawler.UrlColumn, category.getUrl());
    }

    return expressionList.findOne();
  }

  public void saveCategoryCrawler(ClCategoryCrawler category) {
    if (category.getId() == null) {
      ClCategoryCrawler dbCategory = getCategoryCrawler(category);
      if (dbCategory != null) {
        category.setId(dbCategory.getId());
        update(category);
      }
    }

    save(category);
  }

  public synchronized int setAllCategoryCrawlersAvailableStatus(
      Long brandCountryId, boolean status) {
    SqlUpdate sqlUpdate =
        server.createSqlUpdate(
            "update category_crawler set available = :available where brand_country_id = :brandCountryId");
    sqlUpdate.setParameter("available", status);
    sqlUpdate.setParameter("brandCountryId", brandCountryId);

    return server.execute(sqlUpdate);
  }

  public synchronized int setAllCategoriesAvailableStatus(
      Long brandId, Integer subBrand, boolean status) {
    String query = "update category set available = :available where brand_id = :brandId";
    if (subBrand != null) {
      query += " and sub_brand = :subBrand";
    }

    SqlUpdate sqlUpdate = server.createSqlUpdate(query);
    sqlUpdate.setParameter("available", status);
    sqlUpdate.setParameter("brandId", brandId);
    if (subBrand != null) {
      sqlUpdate.setParameter("subBrand", subBrand);
    }

    return server.execute(sqlUpdate);
  }

  public synchronized int setCategoryCrawlersCrawled(
      Long brandCountryId, boolean available, boolean crawled) {
    SqlUpdate sqlUpdate =
        server.createSqlUpdate(
            "update category_crawler set available = :available, crawled = :crawled where brand_country_id = :brandCountryId");
    sqlUpdate.setParameter("crawled", crawled);
    sqlUpdate.setParameter("available", available);
    sqlUpdate.setParameter("brandCountryId", brandCountryId);

    return server.execute(sqlUpdate);
  }

  public synchronized int setCategoriesStatus(
      Long brandId, boolean available, boolean crawled) {
    SqlUpdate sqlUpdate =
        server.createSqlUpdate(
            "update category set available = :available, crawled = :crawled where brand_id = :brandId");
    sqlUpdate.setParameter("crawled", crawled);
    sqlUpdate.setParameter("available", available);
    sqlUpdate.setParameter("brandId", brandId);

    return server.execute(sqlUpdate);
  }

  public synchronized int setAllCategoryCrawlersStatus(boolean available, boolean crawled) {
    return server.execute(
        server
            .createSqlUpdate(
                "update category_crawler set available = :available, crawled = :crawled where id > 0")
            .setParameter("available", available)
            .setParameter("crawled", crawled));
  }

  public synchronized int setAllCategoriesStatus(boolean available, boolean crawled) {
    return server.execute(
        server
            .createSqlUpdate(
                "update category set available = :available, crawled = :crawled where id > 0")
            .setParameter("available", available)
            .setParameter("crawled", crawled));
  }

  public synchronized void saveCategory(ClCategory clCategory) {
    if (clCategory.getId() == null) {
      // try to map current category with existing category
      List<ClCategory> categories = getCatsByBrand(clCategory.getBrandId());
      for (ClCategory cat : categories) {
        clCategory.setCat1(getCat(clCategory.getCat1(), cat.getCat1()));
        clCategory.setCat2(getCat(clCategory.getCat2(), cat.getCat2()));
        clCategory.setCat3(getCat(clCategory.getCat3(), cat.getCat3()));
        clCategory.setCat4(getCat(clCategory.getCat4(), cat.getCat4()));
        clCategory.setCat5(getCat(clCategory.getCat5(), cat.getCat5()));
      }

      ClCategory dbClCategory =
          findByCats(
              clCategory.getBrandId(),
              clCategory.getCat1(),
              clCategory.getCat2(),
              clCategory.getCat3(),
              clCategory.getCat4(),
              clCategory.getCat5());

      if (dbClCategory != null) {
        clCategory.setId(dbClCategory.getId());
        update(clCategory);
      }
    }

    save(clCategory);
  }

  private String getCat(String currentCat, String dbCat) {
    if (StringUtils.isNotBlank(currentCat)
        && StringUtils.isNotBlank(dbCat)
        && (currentCat.contains(" ") || dbCat.contains(" "))) {
      String baseUpper = currentCat.toUpperCase();
      String compareUpper = dbCat.toUpperCase();
      if (baseUpper.contains(compareUpper) || compareUpper.contains(baseUpper)) {
        return dbCat;
      }
    }
    return currentCat;
  }
}
