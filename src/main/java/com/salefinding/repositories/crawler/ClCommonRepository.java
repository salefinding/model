package com.salefinding.repositories.crawler;

import com.salefinding.models.crawler.ClBrand;
import com.salefinding.models.crawler.ClCountry;
import com.salefinding.models.crawler.gen.ClKeyValue;
import com.salefinding.repositories.BaseRepository;

import java.util.List;

public class ClCommonRepository extends BaseRepository<Long, Object> {

  public ClCommonRepository(String dbName) {
    super(dbName, Object.class);
  }

  public List<ClCountry> getAllCountries() {
    return server.find(ClCountry.class).fetch("currency").findList();
  }

  public List<ClKeyValue> getAllKeyValues() {
    return server.find(ClKeyValue.class).findList();
  }

  public ClBrand getBrandById(long id) {
    return server.find(ClBrand.class).where().eq(ClBrand.IdColumn, id).findOne();
  }
}
