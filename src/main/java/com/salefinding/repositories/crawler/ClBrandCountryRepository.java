package com.salefinding.repositories.crawler;

import com.salefinding.enums.BrandCountryRunning;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.repositories.BaseRepository;

import java.util.List;

public class ClBrandCountryRepository extends BaseRepository<Long, ClBrandCountry> {

  public ClBrandCountryRepository(String dbName) {
    super(dbName, ClBrandCountry.class);
  }

  public List<ClBrandCountry> getAllEnabled() {
    return find.query().where().eq(ClBrandCountry.EnableColumn, true).findList();
  }

  public int getNumberSameBrandRunning(long brandId) {
    List<ClBrandCountry> list =
        find.query()
            .where()
            .eq(ClBrandCountry.BrandIdColumn, brandId)
            .ne(ClBrandCountry.RunningColumn, BrandCountryRunning.STOP.getValue())
            .findList();
    return list.size();
  }
}
