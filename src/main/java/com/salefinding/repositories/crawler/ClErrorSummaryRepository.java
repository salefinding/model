package com.salefinding.repositories.crawler;

import com.salefinding.enums.ErrorSummaryStatus;
import com.salefinding.models.crawler.ClErrorSummary;
import com.salefinding.repositories.BaseRepository;
import com.salefinding.utils.SFObjectMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.List;

public class ClErrorSummaryRepository extends BaseRepository<Long, ClErrorSummary> {

  public ClErrorSummaryRepository(String dbName) {
    super(dbName, ClErrorSummary.class);
  }

  public void saveError(String runUid, Throwable throwable) {
    saveError(runUid, ExceptionUtils.getMessage(throwable), throwable, (Object[]) null);
  }

  public void saveError(String runUid, Throwable throwable, Object... data) {
    saveError(runUid, ExceptionUtils.getMessage(throwable), throwable, data);
  }

  public void saveError(String runUid, String message, Throwable throwable) {
    saveError(runUid, message, throwable, (Object[]) null);
  }

  public void saveError(String runUid, String message, Throwable throwable, Object... data) {
    ClErrorSummary errorSummary = new ClErrorSummary();
    errorSummary.setRunUid(runUid);
    errorSummary.setMessage(message);
    if (throwable != null) {
      errorSummary.setStackTrace(ExceptionUtils.getStackTrace(throwable));
    }
    if (data != null) {
      errorSummary.setData(SFObjectMapper.toJsonNode(data, true).toString());
    }

    save(errorSummary);
  }

  public List<ClErrorSummary> getNewErrors() {
    return find.query()
        .where()
        .eq(ClErrorSummary.StatusColumn, ErrorSummaryStatus.NEW.getValue())
        .findList();
  }
}
