package com.salefinding.repositories.crawler;

import com.salefinding.models.crawler.ClCrawlingStatistics;
import com.salefinding.repositories.BaseRepository;

public class ClCrawlingStatisticsRepository extends BaseRepository<Long, ClCrawlingStatistics> {

  public ClCrawlingStatisticsRepository(String dbName) {
    super(dbName, ClCrawlingStatistics.class);
  }
}
