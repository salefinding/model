package com.salefinding.repositories.crawler;

import com.salefinding.models.crawler.ClCrawlingData;
import com.salefinding.repositories.BaseRepository;

public class ClCrawlingDataRepository extends BaseRepository<Long, ClCrawlingData> {

  public ClCrawlingDataRepository(String dbName) {
    super(dbName, ClCrawlingData.class);
  }

  public void updateVisited(Long brandCountryId, String runUid, String url) {
    saveOrUpdate(brandCountryId, runUid, url, true, true, false);
  }

  public void updateSavedItem(Long brandCountryId, String runUid, String url) {
    saveOrUpdate(brandCountryId, runUid, url, true, true, true);
  }

  public void saveOrUpdate(
      Long brandCountryId,
      String runUid,
      String url,
      Boolean shouldVisit,
      Boolean visited,
      Boolean savedItem) {
    ClCrawlingData crawlingData =
        find.query()
            .where()
            .eq(ClCrawlingData.BrandCountryIdColumn, brandCountryId)
            .eq(ClCrawlingData.RunUidColumn, runUid)
            .eq(ClCrawlingData.UrlColumn, url)
            .findOne();
    if (crawlingData == null) {
      crawlingData =
          new ClCrawlingData(brandCountryId, runUid, url, shouldVisit, visited, savedItem);
    } else {
      crawlingData.setVisited(true);
      crawlingData.setSavedItem(true);
    }

    server.save(crawlingData);
  }
}
