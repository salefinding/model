package com.salefinding.repositories.crawler;

import com.salefinding.models.crawler.ClItem;
import com.salefinding.models.crawler.ClItemCountryDetail;
import com.salefinding.repositories.BaseRepository;
import io.ebean.SqlUpdate;

import java.util.List;

public class ClItemRepository extends BaseRepository<Long, ClItem> {

  public ClItemRepository(String dbName) {
    super(dbName, ClItem.class);
  }

  public ClItem find(Long brandId, String code) {
    return find.query()
        .where()
        .eq(ClItem.BrandIdColumn, brandId)
        .eq(ClItem.CodeColumn, code)
        .findOne();
  }

  public ClItemCountryDetail getItemDetail(Long brandCountryId, String code, String url) {
    return server
        .find(ClItemCountryDetail.class)
        .where()
        .eq(ClItemCountryDetail.BrandCountryIdColumn, brandCountryId)
        .eq(ClItemCountryDetail.CodeColumn, code)
        .eq(ClItemCountryDetail.UrlColumn, url)
        .findOne();
  }

  public List<ClItemCountryDetail> getAllItemDetails(int batchSize, long startId) {
    return server
        .find(ClItemCountryDetail.class)
        .where()
        .gt(ClItemCountryDetail.IdColumn, startId)
        .eq(ClItemCountryDetail.AvailableColumn, true)
        .setMaxRows(batchSize)
        .order()
        .asc(ClItemCountryDetail.IdColumn)
        .findList();
  }

  public List<ClItem> getAllItems(int batchSize, long startId) {
    return find.query()
        .where()
        .gt(ClItem.IdColumn, startId)
        .eq(ClItem.AvailableColumn, true)
        .setMaxRows(batchSize)
        .order()
        .asc(ClItem.IdColumn)
        .findList();
  }

  public List<ClItem> getAllItems(int batchSize, long startId, Long brandId) {
    return find.query()
        .where()
        .gt(ClItem.IdColumn, startId)
        .eq(ClItem.BrandIdColumn, brandId)
        .eq(ClItem.AvailableColumn, true)
        .setMaxRows(batchSize)
        .order()
        .asc(ClItem.IdColumn)
        .findList();
  }

  public List<ClItem> getAllSaleItems(int batchSize, long startId) {
    return find.query()
        .where()
        .gt(ClItem.IdColumn, startId)
        .gt(ClItem.MaxSaleRateColumn, 0)
        .eq(ClItem.AvailableColumn, true)
        .setMaxRows(batchSize)
        .order()
        .asc(ClItem.IdColumn)
        .findList();
  }

  public List<ClItem> getAllSaleItems(int batchSize, long startId, Long brandId) {
    return find.query()
        .where()
        .gt(ClItem.IdColumn, startId)
        .eq(ClItem.BrandIdColumn, brandId)
        .eq(ClItem.AvailableColumn, true)
        .gt(ClItem.MaxSaleRateColumn, 0)
        .setMaxRows(batchSize)
        .order()
        .asc(ClItem.IdColumn)
        .findList();
  }

  public synchronized int setAllItemsStatus(
      Long brandId, Integer subBrand, boolean available, boolean crawled) {
    String query =
        "update item set available = :available, crawled = :crawled where brand_id = :brandId";
    if (subBrand != null) {
      query += " and sub_brand = :subBrand";
    }

    SqlUpdate sqlUpdate = server.createSqlUpdate(query);
    sqlUpdate.setParameter("available", available);
    sqlUpdate.setParameter("crawled", crawled);
    sqlUpdate.setParameter("brandId", brandId);
    if (subBrand != null) {
      sqlUpdate.setParameter("subBrand", subBrand);
    }

    return server.execute(sqlUpdate);
  }

  public synchronized int setAllItemDetailsStatus(
      Long brandCountryId, boolean available, boolean crawled) {
    SqlUpdate sqlUpdate =
        server.createSqlUpdate(
            "update item_country_detail set available = :available, crawled = :crawled where brand_country_id = :brandCountryId");
    sqlUpdate.setParameter("available", available);
    sqlUpdate.setParameter("crawled", crawled);
    sqlUpdate.setParameter("brandCountryId", brandCountryId);

    return server.execute(sqlUpdate);
  }

  public synchronized int setAllItemDetailsStatus(boolean status, boolean crawled) {
    return server.execute(
        server
            .createSqlUpdate(
                "update item_country_detail set available = :available, crawled = :crawled where id > 0")
            .setParameter("available", status)
            .setParameter("crawled", crawled));
  }

  public synchronized int setAllItemsStatus(boolean status, boolean crawled) {
    return server.execute(
        server
            .createSqlUpdate(
                "update item set max_sale_rate = 0, available = :available, crawled = :crawled where id > 0")
            .setParameter("available", status)
            .setParameter("crawled", crawled));
  }

  public synchronized int deleteAllStocksOfItem(Long itemDetailId) {
    SqlUpdate sqlUpdate =
        server.createSqlUpdate("delete from item_stock where item_detail_id = :itemDetailId");
    sqlUpdate.setParameter("itemDetailId", itemDetailId);
    return server.execute(sqlUpdate);
  }

  public synchronized int deleteAllImagesOfItem(Long itemDetailId) {
    SqlUpdate sqlUpdate =
        server.createSqlUpdate("delete from item_image where item_detail_id = :itemDetailId");
    sqlUpdate.setParameter("itemDetailId", itemDetailId);
    return server.execute(sqlUpdate);
  }
}
