package com.salefinding.repositories;

import com.salefinding.repositories.crawler.*;

public class CrawlerRepoFactory {

  private static CrawlerRepoFactory factory;

  private static String dbName;

  private ClCategoryRepository categoryRepo;

  private ClItemRepository itemRepo;

  private ClBrandCountryRepository brandCountryRepo;

  private ClErrorSummaryRepository errorSummaryRepo;

  private ClCrawlingStatisticsRepository crawlingStatisticsRepo;

  private ClCrawlingDataRepository crawlingDataRepo;

  private ClWpRepository wpRepository;

  private ClCommonRepository commonRepository;

  private CrawlerRepoFactory() {}

  public static CrawlerRepoFactory getInstance(String dbName) {
    if (CrawlerRepoFactory.dbName != null && !CrawlerRepoFactory.dbName.equals(dbName)) {
      if (factory != null) {
        factory = null;
      }
    }
    CrawlerRepoFactory.dbName = dbName;
    if (factory == null) {
      synchronized (CrawlerRepoFactory.class) {
        if (factory == null) {
          factory = new CrawlerRepoFactory();
        }
      }
    }
    return factory;
  }

  public ClItemRepository getItemRepo() {
    if (itemRepo == null) {
      synchronized (CrawlerRepoFactory.class) {
        if (itemRepo == null) {
          itemRepo = new ClItemRepository(dbName);
        }
      }
    }
    return itemRepo;
  }

  public ClCategoryRepository getCategoryRepo() {
    if (categoryRepo == null) {
      synchronized (CrawlerRepoFactory.class) {
        if (categoryRepo == null) {
          categoryRepo = new ClCategoryRepository(dbName);
        }
      }
    }
    return categoryRepo;
  }

  public ClBrandCountryRepository getBrandCountryRepo() {
    if (brandCountryRepo == null) {
      synchronized (CrawlerRepoFactory.class) {
        if (brandCountryRepo == null) {
          brandCountryRepo = new ClBrandCountryRepository(dbName);
        }
      }
    }
    return brandCountryRepo;
  }

  public ClErrorSummaryRepository getErrorSummaryRepo() {
    if (errorSummaryRepo == null) {
      synchronized (CrawlerRepoFactory.class) {
        if (errorSummaryRepo == null) {
          errorSummaryRepo = new ClErrorSummaryRepository(dbName);
        }
      }
    }
    return errorSummaryRepo;
  }

  public ClCrawlingStatisticsRepository getCrawlingStatisticsRepo() {
    if (crawlingStatisticsRepo == null) {
      synchronized (ClCrawlingStatisticsRepository.class) {
        if (crawlingStatisticsRepo == null) {
          crawlingStatisticsRepo = new ClCrawlingStatisticsRepository(dbName);
        }
      }
    }
    return crawlingStatisticsRepo;
  }

  public ClCrawlingDataRepository getCrawlingDataRepo() {
    if (crawlingDataRepo == null) {
      synchronized (ClCrawlingDataRepository.class) {
        if (crawlingDataRepo == null) {
          crawlingDataRepo = new ClCrawlingDataRepository(dbName);
        }
      }
    }
    return crawlingDataRepo;
  }

  public ClWpRepository getWpRepository() {
    if (wpRepository == null) {
      synchronized (ClWpRepository.class) {
        if (wpRepository == null) {
          wpRepository = new ClWpRepository(dbName);
        }
      }
    }
    return wpRepository;
  }

  public ClCommonRepository getCommonRepository() {
    if (commonRepository == null) {
      synchronized (ClCommonRepository.class) {
        if (commonRepository == null) {
          commonRepository = new ClCommonRepository(dbName);
        }
      }
    }
    return commonRepository;
  }
}
